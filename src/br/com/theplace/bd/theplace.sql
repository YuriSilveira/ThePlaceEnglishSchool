DROP DATABASE IF EXISTS theplace;

CREATE DATABASE theplace CHARACTER SET utf8 COLLATE utf8_general_ci;

USE theplace;

-- 1
CREATE TABLE usuario (
  id INT NOT NULL AUTO_INCREMENT,
  nome varchar(50) NOT NULL,
  email varchar(50) NOT NULL,
  senha varchar(50) NOT NULL,
  hashTemporaria varchar(92) DEFAULT NULL,
  status int NOT NULL,
  PRIMARY KEY(id)
);
-- 2
CREATE TABLE curso (
  id INT NOT NULL AUTO_INCREMENT,
  nome varchar(50) NOT NULL,
  quantLivros int NOT NULL,
  status int NOT NULL,
  PRIMARY KEY(id)
);
-- 3
CREATE TABLE estado (
  id INT NOT NULL AUTO_INCREMENT,
  nome varchar(50) NOT NULL,
  PRIMARY KEY(id)
);
-- 4
CREATE TABLE cidade (
  id INT NOT NULL AUTO_INCREMENT,
  nome varchar(50) NOT NULL,
  idEstado int,

  CONSTRAINT FK_cidade_estado
  FOREIGN KEY (idEstado)
  REFERENCES estado(id),

  PRIMARY KEY(id)
);
-- 5
CREATE TABLE endereco (
  id INT NOT NULL AUTO_INCREMENT,
  logradouro varchar(50) NOT NULL,
  numero int NOT NULL,
  complemento varchar(20) NOT NULL,
  cep varchar(11) NOT NULL,
  bairro varchar(25) NOT NULL,
  idCidade int,

  CONSTRAINT FK_endereco_cidade
  FOREIGN KEY (idCidade)
  REFERENCES cidade(id),

  PRIMARY KEY(id)
) ;
-- 6
CREATE TABLE professor (
  id int AUTO_INCREMENT,
  nome varchar(50) NOT NULL,
  dataNascimento date NOT NULL,
  rg varchar(25) NOT NULL,
  cpf varchar(25) NOT NULL,
  salario int NOT NULL,
  status int NOT NULL,
  idEndereco int,

  CONSTRAINT FK_professor_endereco
  FOREIGN KEY (idEndereco)
  REFERENCES endereco(id),

PRIMARY KEY(id)
);
-- 7
CREATE TABLE responsavel (
  id INT NOT NULL AUTO_INCREMENT,
  nome varchar(50) NOT NULL,
  dataNascimento date NOT NULL,
  rg varchar(25) NOT NULL,
  cpf varchar(25) NOT NULL,
  email varchar(50) NOT NULL,
  imagem varchar(35) NOT NULL,
  telefoneA varchar(25) NOT NULL,
  telefoneB varchar(25),
  telefoneC varchar(25),
  idEndereco int,

  CONSTRAINT FK_responsavel_endereco
  FOREIGN KEY (idEndereco)
  REFERENCES endereco(id),

  PRIMARY KEY(id)
);
-- 8
CREATE TABLE livro (
  id INT NOT NULL AUTO_INCREMENT,
  nome varchar(50) NOT NULL,
  idCurso int,
  status int NOT NULL,

  CONSTRAINT FK_livro_curso
  FOREIGN KEY (idCurso)
  REFERENCES curso(id),

  PRIMARY KEY(id)
);
-- 9
CREATE TABLE matricula (
  id int AUTO_INCREMENT,
  tipoPagamento varchar(50),
  idLivro int,

  CONSTRAINT FK_matricula_livro
  FOREIGN KEY (idLivro)
  REFERENCES livro(id),

  PRIMARY KEY(id)
);
-- 10
CREATE TABLE parcela (
  id INT NOT NULL AUTO_INCREMENT,
  numParcela int NOT NULL,
  totalParcela int NOT NULL,
  valorParcela double NOT NULL,
  dataVencimento date NOT NULL,
  status varchar(50) DEFAULT 'Aguardando Pagamento',
  idMatricula int,

  CONSTRAINT FK_parcela_matricula
  FOREIGN KEY (idMatricula)
  REFERENCES matricula(id),

  PRIMARY KEY(id)
);
-- 11
CREATE TABLE turma (
  id int AUTO_INCREMENT,
  dataInicial date NOT NULL,
  dataFinal date NOT NULL,
  status int NOT NULL,
PRIMARY KEY(id)
);
-- 12
CREATE TABLE aluno (
  id int AUTO_INCREMENT,
  nome varchar(50) NOT NULL,
  dataNascimento date NOT NULL,
  rg varchar(25) NOT NULL,
  cpf varchar(25) NOT NULL,
  email varchar(50),
  telefone varchar(50),
  idEndereco int,
  idResponsavel int,
  idMatricula int,
  idTurma int,
  status int NOT NULL,

  CONSTRAINT FK_aluno_endereco
  FOREIGN KEY (idEndereco)
  REFERENCES endereco(id),

  CONSTRAINT FK_aluno_responsavel
  FOREIGN KEY (idResponsavel)
  REFERENCES responsavel(id),

  CONSTRAINT FK_aluno_matricula
  FOREIGN KEY (idMatricula)
  REFERENCES matricula(id),

  CONSTRAINT FK_aluno_turma
  FOREIGN KEY (idTurma)
  REFERENCES turma(id),

  PRIMARY KEY(id)
);
-- 13
CREATE TABLE horario (
  id int AUTO_INCREMENT,
  diaSemana varchar(3) NOT NULL,
  horaInicial varchar(10) NOT NULL,
  horaFinal varchar(10) NOT NULL,
  idTurma int,
  status int NOT NULL,

  CONSTRAINT FK_horario_turma
  FOREIGN KEY (idTurma)
  REFERENCES turma(id),
  
  PRIMARY KEY(id)
);
-- 14
CREATE TABLE mensalidade (
  id int AUTO_INCREMENT,
  valor int NOT NULL,
  numParcela int NOT NULL,
  idAluno int NOT NULL,

  CONSTRAINT FK_mensalidade_aluno
  FOREIGN KEY (idAluno)
  REFERENCES aluno(id),
  
  PRIMARY KEY(id)
);

INSERT INTO usuario (nome, email, senha, status) VALUES 
("Caio Francisco Lopes", "caio@gmail.com",(MD5("Caio@123")),2),
("Administrador", "admin@gmail.com",(MD5("Admin@123")),2),
("Funcionario", "funcionario@gmail.com",(MD5("Funcionario@123")),1),
("Jeferson Luiz dos Santos Silva", "jefinho_silva@hotmail.com",(MD5("Jeferson@123")),1),
("Roberto Weslley", "roberto@hotmail.com",(MD5("Roberto@123")),1),
("Daiana Karine Rolim Caula", "daiana@bol.com.br",(MD5("Daiana@123")),1),
("Juliana Silva Sousa", "juliana@gmail.com",(MD5("Juliana@123")),1),
("Gilberto Alves da Cruz", "gilberto@outlook.com",(MD5("Gilberto@123")),1),
("Italo Aguiar Freire", "italo@outlook.com",(MD5("Italo@123")),0),
("Maria Renata Silveira", "maria@outlook.com",(MD5("Maria@123")),0),
("Paloma de Oliveira Gomes", "paloma@hotmail.com",(MD5("Paloma@123")),0),
("Everton Alencar Moura", "everton@hotmail.com",(MD5("Everton@123")),0),
("Diego Kedson dos Santos", "diego@hotmail.com",(MD5("Diego@123")),0),
("Irene Barbosa Gondim", "irene@gmail.com",(MD5("Irene@123")),0),
("Ana Neyla Martins da Mota", "ana@gmail.com",(MD5("Ana@123")),0),
("Allyson Bezerra de Oliveira", "allyson@outlook.com",(MD5("Allyson@123")),0),
("Emmanuel Alves de Melo", "emmanuelalves@outlook.com",(MD5("Emmanuel@123")),0),
("Marcelo Silva Costa", "marcelinho@gmail.com",(MD5("Marcelo@123")),1),
("Joaquim da Silva", "joaquim@hotmail.com",(MD5("Joaquim@123")),1),
("Vinicius Lima Angelo", "vinicius@gmail.com",(MD5("Vinicius@123")),1),
("Yuri Goncalves da Silveira", "yuriinternacional86@gmail.com",(MD5("Yuri@123")),0);

INSERT INTO curso (nome, quantLivros, status) VALUES 
("TEENS SERIES", 0, 1),
("ADULTS SERIES", 0, 1),
("KEEP UP SERIES", 0, 1);

INSERT INTO estado (nome) VALUES 
("RS");

INSERT INTO cidade (nome, idEstado) VALUES 
("Pelotas", 1);

INSERT INTO endereco (logradouro, numero, complemento, cep, bairro, idCidade) VALUES 
("Rua Anita Garibaldi", 601, "", "96083500","Areal", 1),
("Rua Andrade Neves", 1414, "202", "96020080", "Centro", 1),
("Rua Bernardo José de Souza", 121, "", "96040230","Areal", 1),
("Rua Alcides Maya", 4290, "303", "96040220","Fragata", 1),
("Rua Alcindo Guanabara", 88, "", "96065210","Três Vendas", 1),
("Rua Doutor José Brusque", 2070, "101", "96077480","Centro", 1),
("Rua Doutor José Corrêa", 155, "", "96065780","Três Vendas", 1),
("Rua Doutor José Gurvitz", 7878, "101", "96045540","Fragata", 1),
("Rua Doutor José Machado Mendonça", 2783, "", "96040270","Fragata", 1),
("Rua Doutor Juvenal Miller", 1918, "", "96080510","Areal", 1),
("Rua Doutor Manoel Cipriano de Moraes", 8080, "", "96040274","Fragata", 1),
("Alameda dos Ligustros", 5710, "303", "96083455","São Gonçalo", 1),
("Rua Duque Estrada", 873, "", "96060280","Três Vendas", 1),
("Rua Dois (Navegantes II)", 218, "", "96076150","São Gonçalo", 1),
("Rua Félix Xavier da Cunha", 98, "", "96010000","Centro", 1),
("Rua José Vieira Pimenta", 12, "", "96030060","Fragata", 1),
("Rua Lavras do Sul", 982, "", "96095160","Laranjal", 1),
("Rua Irmão Gabino Gerardo", 25, "202", "96040120","Fragata", 1);

INSERT INTO professor (nome, dataNascimento, rg, cpf, salario, status, idEndereco) VALUES 
("Heloisa Camila Alves", "1988-07-11", "495600829", "915.955.990-02", "2800", 1, 1),
("Carlos Eduardo Pietro Caio Freitas", "1988-07-11", "447058034", "383.866.520-12", "3500", 1, 2),
("Iago Gabriel Monteiro", "1988-07-11", "499405778", "399.145.650-85", "3000", 1, 2),
("Melissa Sara Nicole Cardoso", "1988-07-11", "357435849", "086.309.580-14", "2800", 1, 2),
("Antonella Pietra dos Santos", "1988-07-11", "436094332", "194.131.410-45", "2500", 1, 1);

INSERT INTO responsavel (nome, dataNascimento, rg, cpf, email, imagem, telefoneA, telefoneB, telefoneC, idEndereco) VALUES 
("Thales Julio Bernardo da Silva", "1990-07-17", "479787669", "028.359.740-24", "thales@hotmail.com", "", "(53) 991270514", NULL, NULL, 5),
("Juan Renan Diego Martins", "1990-07-17", "483159633", "807.453.520-73", "juan@gmail.com", "", "(53) 991270514", NULL, NULL, 8),
("Augusto Alexandre Thales Mendes", "2002-10-02", "376648077", "716.999.600-62", "augusto@outlook.com", "", "(53) 991178580", NULL, NULL, 12),
("Paulo Luiz Moura", "2002-10-02", "369586141", "313.033.000-36", "paulo@gmail.com", "", "(53) 984657571", NULL, NULL, 15),
("Lorenzo Samuel Marcos Vinicius Almeida", "2002-10-02", "206547316", "856.538.170-66", "lorenzo@hotmail.com", "", "(53) 981461593", NULL, NULL, 18),
("Luiz de Paula", "2002-10-02", "283821036", "469.152.290-51", "luiz@hotmail.com", "", "(53) 991511389", NULL, NULL, 16),
("Danilo Marcelo Castro", "1990-07-17", "353653433", "934.583.740-00", "danilo@hotmail.com", "", "(53) 991270514", NULL, NULL, 3),
("Henrique Luan Levi de Paula", "1990-07-17", "29.951.664-7", "965.352.450-04", "henrique@gmail.com", "", "(53) 991270514", NULL, NULL, 4),
("Augusto Vitor Francisco Barros", "1990-07-17", "418575757", "711.620.290-05", "augusto@hotmail.com", "", "(53) 991270514", NULL, NULL, 9),
("Vitor Thiago Ribeiro", "1990-07-17", "507073514", "900.611.190-24", "vitor@bol.com.br", "", "(53) 991270514", NULL, NULL, 11),
("Manuela Alice Moura", "1990-07-17", "269206395", "539.836.350-67", "manuela@bol.com.br", "", "(53) 991270514", NULL, NULL, 6),
("Nicolas Hugo Barbosa", "1990-07-17", "115707049", "613.461.290-10", "nicolas@hotmail.com", "", "(53) 991270514", NULL, NULL, 10),
("Luan Vitor de Paula", "1990-07-17", "497457362", "079.279.160-68", "luan@hotmail.com", "", "(53) 991270514", NULL, NULL, 7),
("Ryan Iago Igor Carvalho", "1990-07-17", "204643016", "517.229.850-97", "ryan@gmail.com", "", "(53) 991270514", NULL, NULL, 13),
("Caroline Laura Freitas", "1990-07-17", "143139952", "142.683.230-31", "caroline@hotmail.com", "", "(53) 991270514", NULL, NULL, 14),
("Leonardo Barros", "2002-10-02", "170649647", "317.889.640-63", "leonardo@gmail.com", "", "(53) 81676226", NULL, NULL, 17),
("Henry Vinicius Lucca Ribeiro", "2002-10-02", "453951375", "554.868.970-30", "henry@hotmail.com", "", "(53) 32724401", NULL, NULL, 10),
("Breno Diego Luiz Monteiro", "2002-10-02", "239450851", "118.481.620-49", "breno@hotmail.com", "", "(53) 32595788", NULL, NULL, 11),
("Leonardo Davi Pedro Mendes", "2002-10-02", "136031018", "435.380.110-44", "leonardo@gmail.com", "", "(53) 32747187", NULL, NULL, 18),
("Samuel Guilherme Mendes", "2002-10-02", "171754852", "905.580.690-02", "samuel@gmail.com", "", "(53) 991522964", NULL, NULL, 5),
("Renan Kevin Lucas Mendes", "2002-10-02", "444116461", "611.656.350-38", "renan@gmail.com", "", "(53) 991814441", NULL, NULL, 3),
("Renato Igor Lorenzo Oliveira", "2002-10-02", "399254092", "811.021.120-89", "renato@gmail.com", "", "(53) 991980842", NULL, NULL, 7),
("Filipe Enzo Campos", "2002-10-02", "259562385", "733.404.270-03", "filipe@hotmail.com", "", "(53) 32819943", NULL, NULL, 9),
("Ryan Daniel Vinicius Freitas", "2002-10-02", "500352628", "770.283.100-60", "ryan_vini@gmail.com", "", "(53) 32493728", NULL, NULL, 15),
("Juan Kevin da Silva", "2002-10-02", "331684226", "571.109.130-15", "juan@outlook.com", "", "(53) 991862770", NULL, NULL, 4),
("Giovanni Elias Benjamin Monteiro", "2002-10-02", "234018288", "523.109.810-87", "giovanni@hotmail.com", "", "(53) 32884990", NULL, NULL, 6),
("Marcela Marina Heloisa Ribeiro", "2002-10-02", "456673593", "062.671.490-72", "marcela@hotmail.com", "", "(53) 84530379", NULL, NULL, 17),
("Sophia Marcela Castro", "2002-10-02", "344068134", "475.352.470-16", "sophia@gmail.com", "", "(53) 84967284", NULL, NULL, 8),
("Luna Laura Raquel Cardoso", "2002-10-02", "420079701", "42.007.970-1", "luna@outlook.com", "", "(53) 84728540", NULL, NULL, 14),
("Bruna Alice Araujo", "2002-10-02", "348767389", "80582709032", "bruna@outlook.com", "", "(53) 32955606", NULL, NULL, 12);

INSERT INTO livro (nome, idCurso, status) VALUES 
("START", 1, 1),
("OPEN", 1, 1),
("INSIDE", 1, 1),
("TAKE OFF", 1, 1),
("CONQUEST", 1, 1),
("SHOW & TELL", 1, 1),
("FREEDOM", 1, 1),
("START", 2, 1),
("VISION", 2, 1),
("CONTROL", 2, 1),
("SPEED UP", 2, 1),
("USA", 3, 1),
("PHONETIC", 3, 1),
("DESTINY", 3, 1);

INSERT INTO matricula (tipoPagamento, idLivro) VALUES 
("Boleto", 5),
("Boleto", 2),
("Boleto", 1),
("Boleto", 6),
("Boleto", 8),
("Boleto", 11),
("Boleto", 14),
("Boleto", 10),
("Boleto", 10),
("Boleto", 12),
("Boleto", 13),
("Boleto", 2),
("Boleto", 2),
("Boleto", 3),
("Boleto", 4),
("Boleto", 4),
("Boleto", 1),
("Boleto", 1),
("Boleto", 14),
("Boleto", 2),
("Boleto", 2),
("Boleto", 3),
("Boleto", 5),
("Boleto", 5),
("Boleto", 4),
("Boleto", 7),
("Boleto", 7),
("Boleto", 7),
("Boleto", 8),
("Boleto", 8),
("Boleto", 8),
("Boleto", 7),
("Boleto", 7),
("Boleto", 9),
("Boleto", 9);

INSERT INTO turma (dataInicial, dataFinal, status) VALUES 
("2018-03-02", "2018-12-05", 1),
("2018-02-22", "2018-11-15", 1),
("2018-03-10", "2018-12-14", 1),
("2018-06-20", "2019-11-20", 1),
("2018-07-09", "2019-03-11", 1),
("2017-02-11", "2017-11-20", 0),
("2017-03-03", "2017-09-24", 0);

INSERT INTO horario (diaSemana, horaInicial, horaFinal, idTurma, status) VALUES 
("Seg", "13:30", "15:30", 1, 1),
("Qua", "14:00", "16:00", 2, 1),
("Sab", "08:00", "10:00", 3, 1),
("Ter", "08:00", "10:00", 4, 1),
("Qui", "07:30", "09:30", 5, 1),
("Sex", "08:00", "10:00", 6, 0),
("Sex", "10:00", "12:00", 7, 0);

INSERT INTO aluno (nome, dataNascimento, rg, cpf, email, telefone, idEndereco, idResponsavel, idMatricula, idTurma, status) VALUES 
("Henrique Guilherme Cardoso", "2002-08-20", "231974401", "306.812.920-67", "henrique@hotmail.com", "(53) 32886367", 1, 1, 1, 1, 1),
("Arthur Felipe Luiz Monteiro", "2002-10-02", "320918452", "320.991.490-70", "arthur@gmail.com", "(53) 991470532", 2, 2, 2, 1, 1),
("Augusto Alexandre Thales Mendes", "2002-10-02", "376648077", "716.999.600-62", "augusto@outlook.com", "(53) 991178580", 3, 3, 3, 1, 1),
("Paulo Luiz Moura", "2002-10-02", "369586141", "313.033.000-36", "paulo@gmail.com", "(53) 984657571", 4, 4, 4, 1, 1),
("Lorenzo Samuel Marcos Vinicius Almeida", "2002-10-02", "206547316", "856.538.170-66", "lorenzo@hotmail.com", "(53) 981461593", 5, 5, 5, 1, 1),
("Luiz de Paula", "2002-10-02", "283821036", "469.152.290-51", "luiz@hotmail.com", "(53) 991511389", 6, 6, 6, 1, 1),
("Thales Luiz Ribeiro", "2002-10-02", "143254698", "539.221.130-52", "thales@outlook.com", "(53) 32014303", 7, 7, 7, 2, 1),
("Marcos Vinicius Carlos Eduardo Lima", "2002-10-02", "329560256", "841.622.380-73", "marcos@outlook.com", "(53) 32106609", 8, 8, 8, 2, 1),
("Nathan Antonio Castro", "2002-10-02", "167186711", "306.800.250-81", "nathan@gmail.com", "(53) 32469495", 9, 9, 9, 2, 1),
("Iago Julio Vitor Rodrigues", "2002-10-02", "288704447", "428.358.900-41", "iago@hotmail.com", "(53) 991771829", 10, 10, 10, 2, 1),
("Benjamin Davi Fernandes", "2002-10-02", "121040057", "128.801.330-25", "benjamin@hotmail.com", "(53) 984628961", 11, 11, 11, 2, 1),
("Rodrigo Juan de Paula", "2002-10-02", "251390299", "760.998.850-21", "rodrigo@gmail.com", "(53) 32276945", 12, 12, 12, 2, 1),
("Bryan Carlos Eduardo Breno Campos", "2002-10-02", "347597579", "921.519.590-40", "bryan@bol.com.br", "(53) 991248529", 13, 13, 13, 3, 1),
("Filipe Nicolas Paulo Campos", "2002-10-02", "212779746", "668.368.400-68", "filipe@hotmail.com", "(53) 991511927", 14, 14, 14, 3, 1),
("Erick Enzo Carvalho", "2002-10-02", "229325841", "724.091.940-44", "erick@hotmail.com", "(53) 84643891", 15, 15, 15, 3, 1),
("Leonardo Barros", "2002-10-02", "170649647", "317.889.640-63", "leonardo@gmail.com", "(53) 81676226", 16, 16, 16, 3, 1),
("Henry Vinicius Lucca Ribeiro", "2002-10-02", "453951375", "554.868.970-30", "henry@hotmail.com", "(53) 32724401", 2, 17, 17, 4, 1),
("Breno Diego Luiz Monteiro", "2002-10-02", "239450851", "118.481.620-49", "breno@hotmail.com", "(53) 32595788", 2, 18, 18, 4, 1),
("Leonardo Davi Pedro Mendes", "2002-10-02", "136031018", "435.380.110-44", "leonardo@gmail.com", "(53) 32747187", 2, 19, 19, 4, 1),
("Samuel Guilherme Mendes", "2002-10-02", "171754852", "905.580.690-02", "samuel@gmail.com", "(53) 991522964", 2, 20, 20, 4, 1),
("Renan Kevin Lucas Mendes", "2002-10-02", "444116461", "611.656.350-38", "renan@gmail.com", "(53) 991814441", 2, 21, 21, 4, 1),
("Renato Igor Lorenzo Oliveira", "2002-10-02", "399254092", "811.021.120-89", "renato@gmail.com", "(53) 991980842", 2, 22, 22, 5, 1),
("Filipe Enzo Campos", "2002-10-02", "259562385", "733.404.270-03", "filipe@hotmail.com", "(53) 32819943", 2, 23, 23, 5, 1),
("Ryan Daniel Vinicius Freitas", "2002-10-02", "500352628", "770.283.100-60", "ryan_vini@gmail.com", "(53) 32493728", 2, 24, 24, 5, 1),
("Juan Kevin da Silva", "2002-10-02", "331684226", "571.109.130-15", "juan@outlook.com", "(53) 991862770", 2, 25, 25, 5, 1),
("Giovanni Elias Benjamin Monteiro", "2002-10-02", "234018288", "523.109.810-87", "giovanni@hotmail.com", "(53) 32884990", 2, 26, 26, 6, 0),
("Marcela Marina Heloisa Ribeiro", "2002-10-02", "456673593", "062.671.490-72", "marcela@hotmail.com", "(53) 84530379", 2, 27, 27, 6, 0),
("Sophia Marcela Castro", "2002-10-02", "344068134", "475.352.470-16", "sophia@gmail.com", "(53) 84967284", 2, 28, 28, 6, 0),
("Luna Laura Raquel Cardoso", "2002-10-02", "420079701", "42.007.970-1", "luna@outlook.com", "(53) 84728540", 2, 29, 29, 7, 0),
("Bruna Alice Araujo", "2002-10-02", "348767389", "80582709032", "bruna@outlook.com", "(53) 32955606", 2, 30, 30, 7, 0);

INSERT INTO parcela (numParcela, totalParcela, valorParcela, dataVencimento, status, idMatricula) VALUES 
-- matricula 01
('1', '6', '120', '2017-12-05', 'Aguardando Pagamento', 1),
('2', '6', '120', '2017-12-05', 'Aguardando Pagamento', 1),
('3', '6', '120', '2017-12-05', 'Aguardando Pagamento', 1),
('4', '6', '120', '2017-12-05', 'Aguardando Pagamento', 1),
('5', '6', '120', '2017-12-05', 'Aguardando Pagamento', 1),
('6', '6', '120', '2017-12-05', 'Aguardando Pagamento', 1),
-- matricula 02
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 2),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 2),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 2),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 2),
-- matricula 03
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 3),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 3),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 3),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 3),
-- matricula 04
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 4),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 4),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 4),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 4),
-- matricula 05
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 5),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 5),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 5),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 5),
-- matricula 06
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 6),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 6),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 6),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 6),
-- matricula 07
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 7),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 7),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 7),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 7),
-- matricula 08
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 8),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 8),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 8),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 8),
-- matricula 09
('1', '3', '200', '2017-12-05', 'Aguardando Pagamento', 9),
('2', '3', '200', '2017-12-05', 'Aguardando Pagamento', 9),
('3', '3', '200', '2017-12-05', 'Aguardando Pagamento', 9),
-- matricula 10
('1', '2', '300', '2017-12-05', 'Aguardando Pagamento', 10),
('2', '2', '300', '2017-12-05', 'Aguardando Pagamento', 10),
-- matricula 11
('1', '2', '300', '2017-12-05', 'Aguardando Pagamento', 11),
('2', '2', '300', '2017-12-05', 'Aguardando Pagamento', 11),
-- matricula 12
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 12),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 12),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 12),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 12),
-- matricula 13
('1', '2', '300', '2017-12-05', 'Aguardando Pagamento', 13),
('2', '2', '300', '2017-12-05', 'Aguardando Pagamento', 13),
-- matricula 14
('1', '1', '600', '2017-12-05', 'Aguardando Pagamento', 14),
-- matricula 15
('1', '1', '600', '2017-12-05', 'Aguardando Pagamento', 15),
-- matricula 16
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 16),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 16),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 16),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 16),
-- matricula 17
('1', '1', '600', '2017-12-05', 'Aguardando Pagamento', 17),
-- matricula 18
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 18),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 18),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 18),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 18),
-- matricula 19
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 19),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 19),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 19),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 19),
-- matricula 20
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 20),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 20),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 20),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 20),
-- matricula 21
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 21),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 21),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 21),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 21),
-- matricula 22
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 22),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 22),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 22),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 22),
-- matricula 23
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 23),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 23),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 23),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 23),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 23),
-- matricula 24
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 24),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 24),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 24),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 24),
-- matricula 25
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 25),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 25),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 25),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 25),
-- matricula 26
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 26),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 26),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 26),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 26),
-- matricula 27
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 27),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 27),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 27),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 27),
-- matricula 28
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 28),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 28),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 28),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 28),
-- matricula 29
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 29),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 29),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 29),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 29),
-- matricula 30
('1', '4', '180', '2017-12-05', 'Aguardando Pagamento', 30),
('2', '4', '180', '2017-12-05', 'Aguardando Pagamento', 30),
('3', '4', '180', '2017-12-05', 'Aguardando Pagamento', 30),
('4', '4', '180', '2017-12-05', 'Aguardando Pagamento', 30);
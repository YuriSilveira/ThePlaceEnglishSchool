package br.com.theplace.dao;

import br.com.theplace.dal.ModuloConexao;
import java.net.URL;
import java.sql.Connection;
import java.util.HashMap;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

public class RelatoriosDAO {

    private final String caminho;

    public RelatoriosDAO() {
        URL urlDiretorioLocal = this.getClass().getResource("/br/com/theplace/relatorios/");
        caminho = urlDiretorioLocal.getPath();
    }

    public JasperPrint gerarRelatAlunos() {
        JasperPrint relatorio = null;
        try {
            Connection conexao;
            conexao = ModuloConexao.conector();
            relatorio = JasperFillManager.fillReport(caminho + "relatAlunos.jasper", new HashMap(), conexao);
            //fechando a conexão
            conexao.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao gerar relatório.", "Erro", 0);
        }
        return relatorio;
    }

    public JasperPrint gerarRelatAlunosInativos() {
        JasperPrint relatorio = null;
        try {
            Connection conexao;
            conexao = ModuloConexao.conector();
            relatorio = JasperFillManager.fillReport(caminho + "relatAlunosInativos.jasper", new HashMap(), conexao);
            //fechando a conexão
            conexao.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao gerar relatório.", "Erro", 0);
        }
        return relatorio;
    }

    public JasperPrint gerarRelatCarnes(int idMatricula) {
        JasperPrint relatorio = null;
        try {
            Connection conexao;
            conexao = ModuloConexao.conector();
            HashMap hashMap = new HashMap();
            hashMap.put("idMatricula", idMatricula);
            relatorio = JasperFillManager.fillReport(caminho + "relatCarne.jasper", hashMap, conexao);
            //fechando a conexão
            conexao.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao gerar relatório.", "Erro", 0);
        }
        return relatorio;
    }
}

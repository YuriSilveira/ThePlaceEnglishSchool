package br.com.theplace.dao;

import br.com.theplace.dal.ModuloConexao;
import br.com.theplace.model.Professor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class ProfessoresDAO {

    private int retornoID;

    public int insert(Professor professor) {
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "INSERT INTO professor (nome, dataNascimento, rg, cpf, salario, status, idEndereco) VALUES (?,?,?,?,?,?,?);";
            pst = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pst.setString(1, professor.getNome());
            pst.setString(2, professor.getDataNascimento());
            pst.setString(3, professor.getRg());
            pst.setString(4, professor.getCpf());
            pst.setInt(5, professor.getSalario());
            pst.setInt(6, professor.getStatus());
            pst.setInt(7, professor.getIdEndereco());
            pst.executeUpdate();
            rs = pst.getGeneratedKeys();
            rs.next();
            retornoID = rs.getInt(1);

            //fechando a conexão
            conexao.close();
            
            JOptionPane.showMessageDialog(null, "Professor cadastrado com sucesso!", "Sucesso", 1);
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao cadastrar endereço.", "Erro", 0);
        }
        return retornoID;
    }

}

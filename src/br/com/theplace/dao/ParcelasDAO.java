package br.com.theplace.dao;

import br.com.theplace.dal.ModuloConexao;
import br.com.theplace.model.Parcela;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class ParcelasDAO {

    private int retornoID;

    public int insert(Parcela parcela) {
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "INSERT INTO parcela (numParcela, totalParcela, "
                    + "valorParcela, dataVencimento, idMatricula) "
                    + "VALUES (?,?,?,?,?);";
            pst = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pst.setInt(1, parcela.getNumParcela());
            pst.setInt(2, parcela.getTotalParcela());
            pst.setDouble(3, parcela.getValorParcela());
            pst.setString(4, parcela.getDataVencimento());
            pst.setInt(5, parcela.getIdMatricula());
            pst.executeUpdate();
            rs = pst.getGeneratedKeys();
            rs.next();
            retornoID = rs.getInt(1);

            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao cadastrar parcela.", "Erro", 0);
        }
        return retornoID;
    }

    public ArrayList<Parcela> readParcelaAluno(int id) {
        ArrayList<Parcela> parcelas = new ArrayList();
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "SELECT parcela.numParcela, parcela.totalParcela, "
                    + "parcela.valorParcela, parcela.dataVencimento, "
                    + "parcela.status FROM parcela "
                    + "WHERE parcela.idMatricula = " + id;
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                Parcela parcela = new Parcela();
                parcela.setNumParcela(rs.getInt("numParcela"));
                parcela.setTotalParcela(rs.getInt("totalParcela"));
                parcela.setValorParcela(rs.getInt("valorParcela"));
                parcela.setDataVencimento(rs.getString("dataVencimento"));
                parcela.setStatus(rs.getString("status"));
                parcelas.add(parcela);
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao listar parcelas de aluno.", "Erro", 0);
        }
        return parcelas;
    }

    public int[] returnIDArray(int idMatricula) {
        int[] id = null;
        int i = 0;
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "SELECT id FROM parcela WHERE idMatricula ='" + idMatricula + "'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                id[i] = rs.getInt("id");
                i++;
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao retornar o ID.", "Erro", 0);
        }
        return id;
    }

    public void update(Parcela parcela, int id) {
        try {
            Connection conexao;
            PreparedStatement pst;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "update parcela set numParcela=?, totalParcela=?, "
                    + "valorParcela=?, dataVencimento=?, "
                    + "idMatricula=? where id=" + id;
            pst = conexao.prepareStatement(sql);
            pst.setInt(1, parcela.getNumParcela());
            pst.setInt(2, parcela.getTotalParcela());
            pst.setDouble(3, parcela.getValorParcela());
            pst.setString(4, parcela.getDataVencimento());
            pst.setInt(5, parcela.getIdMatricula());
            pst.executeUpdate();
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao editar parcelas.", "Erro", 0);
        }
    }

    public void updateStatus(String status, int idMatricula) {
        try {
            Connection conexao;
            PreparedStatement pst;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "update parcela set status=? where idMatricula=" + idMatricula;
            pst = conexao.prepareStatement(sql);
            pst.setString(1, status);
            pst.executeUpdate();
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao atualizae status das parcelas.", "Erro", 0);
        }
    }

}

package br.com.theplace.dao;

import br.com.theplace.dal.ModuloConexao;
import br.com.theplace.model.Aluno;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class AlunosDAO {

    private int retornoID;

    public int insert(Aluno aluno) {
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "INSERT INTO aluno (nome, dataNascimento, rg, cpf, "
                    + "email, telefone, idEndereco, idResponsavel, idMatricula, "
                    + "idTurma, status) VALUES (?,?,?,?,?,?,?,?,?,?,?);";
            pst = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pst.setString(1, aluno.getNome());
            pst.setString(2, aluno.getDataNascimento());
            pst.setString(3, aluno.getRg());
            pst.setString(4, aluno.getCpf());
            pst.setString(5, aluno.getEmail());
            pst.setString(6, aluno.getTelefone());
            pst.setInt(7, aluno.getIdEndereco());
            pst.setInt(8, aluno.getIdResponsavel());
            pst.setInt(9, aluno.getIdMatricula());
            pst.setInt(10, aluno.getIdTurma());
            pst.setInt(11, aluno.getStatus());
            pst.executeUpdate();
            rs = pst.getGeneratedKeys();
            rs.next();
            retornoID = rs.getInt(1);

            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao cadastrar aluno.", "Erro", 0);
        }
        return retornoID;
    }

    public void delete(String itemSelecionado) {
        try {
            Connection conexao;
            PreparedStatement pst;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "UPDATE aluno SET status = 0 WHERE cpf='" + itemSelecionado + "'";
            pst = conexao.prepareStatement(sql);
            pst.executeUpdate();

            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao excluir aluno(s).", "Erro", 0);
        }
    }

    public ArrayList<Aluno> read() {
        ArrayList<Aluno> alunos = new ArrayList();
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "SELECT aluno.nome, aluno.dataNascimento, aluno.cpf, "
                    + "aluno.email, aluno.telefone, aluno.idResponsavel, "
                    + "aluno.idTurma, aluno.status FROM aluno;";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                Aluno aluno = new Aluno();
                aluno.setNome(rs.getString("nome"));
                aluno.setDataNascimento(rs.getString("dataNascimento"));
                aluno.setCpf(rs.getString("cpf"));
                aluno.setEmail(rs.getString("email"));
                aluno.setTelefone(rs.getString("telefone"));
                aluno.setIdResponsavel(rs.getInt("idResponsavel"));
                aluno.setIdTurma(rs.getInt("idTurma"));
                aluno.setStatus(rs.getInt("status"));
                alunos.add(aluno);
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao listar os alunos.", "Erro", 0);
        }
        return alunos;
    }

    public ArrayList<Aluno> readUpdate(String itemSelecionado) {
        ArrayList<Aluno> alunos = new ArrayList();
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sqlSelect = "SELECT aluno.nome, aluno.dataNascimento, "
                    + "aluno.rg, aluno.cpf, aluno.email, aluno.telefone, "
                    + "aluno.idEndereco, aluno.idResponsavel, aluno.idMatricula, "
                    + "aluno.idTurma, aluno.status FROM aluno "
                    + "WHERE aluno.id = '" + itemSelecionado + "'";
            pst = conexao.prepareStatement(sqlSelect);
            rs = pst.executeQuery();

            while (rs.next()) {
                Aluno aluno = new Aluno();
                aluno.setNome(rs.getString("nome"));
                aluno.setDataNascimento(rs.getString("dataNascimento"));
                aluno.setRg(rs.getString("rg"));
                aluno.setCpf(rs.getString("cpf"));
                aluno.setEmail(rs.getString("email"));
                aluno.setTelefone(rs.getString("telefone"));
                aluno.setIdEndereco(rs.getInt("idEndereco"));
                aluno.setIdResponsavel(rs.getInt("idResponsavel"));
                aluno.setIdMatricula(rs.getInt("idMatricula"));
                aluno.setIdTurma(rs.getInt("idTurma"));
                aluno.setStatus(rs.getInt("status"));
                alunos.add(aluno);
            }
            //fechando a conexão
            conexao.close();

        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao solicitar edição de alunos.", "Erro", 0);
        }
        return alunos;
    }

    public void update(Aluno aluno, int id) {
        try {
            Connection conexao;
            PreparedStatement pst;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "update aluno set aluno.nome=?, aluno.dataNascimento=?, "
                    + "aluno.rg=?, aluno.cpf=?, aluno.email=?, aluno.telefone=?, "
                    + "aluno.idEndereco=?, aluno.idResponsavel=?, aluno.idMatricula=?, "
                    + "aluno.idTurma=? where id=" + id;
            pst = conexao.prepareStatement(sql);
            pst.setString(1, aluno.getNome());
            pst.setString(2, aluno.getDataNascimento());
            pst.setString(3, aluno.getRg());
            pst.setString(4, aluno.getCpf());
            pst.setString(5, aluno.getEmail());
            pst.setString(6, aluno.getTelefone());
            pst.setInt(7, aluno.getIdEndereco());
            pst.setInt(8, aluno.getIdResponsavel());
            pst.setInt(9, aluno.getIdMatricula());
            pst.setInt(10, aluno.getIdTurma());
            pst.executeUpdate();
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao editar aluno.", "Erro", 0);
        }
    }

    public ArrayList<Aluno> search(String dadoRecebido) {
        ArrayList<Aluno> alunos = new ArrayList();
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            int numItensEncontrados = 0;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "SELECT aluno.nome, aluno.dataNascimento, aluno.cpf, "
                    + "aluno.email, aluno.telefone, aluno.idResponsavel, "
                    + "aluno.idTurma, aluno.status FROM aluno "
                    + "WHERE (aluno.nome LIKE '%" + dadoRecebido + "%' "
                    + "OR aluno.cpf LIKE '%" + dadoRecebido + "%')";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                if (rs.getInt("status") == 1) {
                    Aluno aluno = new Aluno();
                    aluno.setNome(rs.getString("nome"));
                    aluno.setDataNascimento(rs.getString("dataNascimento"));
                    aluno.setCpf(rs.getString("cpf"));
                    aluno.setEmail(rs.getString("email"));
                    aluno.setTelefone(rs.getString("telefone"));
                    aluno.setIdTurma(rs.getInt("idTurma"));
                    aluno.setIdResponsavel(rs.getInt("idResponsavel"));
                    aluno.setStatus(rs.getInt("status"));
                    alunos.add(aluno);
                    numItensEncontrados++;
                }
            }

            //mensagem se encontrou algo na pesquisa ou não
            if (numItensEncontrados == 0) {
                JOptionPane.showMessageDialog(null, "Nenhum item foi encontrado na pesquisa por " + dadoRecebido, "Aviso", 1);
            } else {
                JOptionPane.showMessageDialog(null, "Encontrado " + numItensEncontrados + " item(ns) na pesquisa por " + dadoRecebido, "Sucesso", 1);
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao realizar a pesquisa.", "Erro", 0);
        }
        return alunos;
    }

    public int returnID(String dadoRecebido) {
        int id = 0;
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select id from aluno where cpf='" + dadoRecebido + "'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                id = rs.getInt("id");
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao retornar o ID.", "Erro", 0);
        }
        return id;
    }

    public int returnIDMatricula(String dadoRecebido) {
        int id = 0;
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select idMatricula from aluno where cpf='" + dadoRecebido + "'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                id = rs.getInt("idMatricula");
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao retornar o ID da matrícula.", "Erro", 0);
        }
        return id;
    }

    public int returnIDResponsavel(String dadoRecebido) {
        int id = 0;
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select idResponsavel from aluno where cpf='" + dadoRecebido + "'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                id = rs.getInt("idResponsavel");
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao retornar o ID do Responsável.", "Erro", 0);
        }
        return id;
    }

}

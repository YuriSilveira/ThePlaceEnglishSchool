package br.com.theplace.dao;

import br.com.theplace.dal.ModuloConexao;
import br.com.theplace.model.Curso;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class CursosDAO {

    public void insert(Curso curso) {
        Connection conexao;
        PreparedStatement pst;

        //realizando a conexão estipulada na classe ModuloConexao
        conexao = ModuloConexao.conector();
        try {
            String sql = "INSERT INTO curso (nome, quantLivros, status) VALUES (?,?,?);";
            pst = conexao.prepareStatement(sql);
            pst.setString(1, curso.getNome());
            pst.setInt(2, curso.getQuantLivros());
            pst.setInt(3, curso.getStatus());

            pst.executeUpdate();

            //fechando a conexão
            conexao.close();
            JOptionPane.showMessageDialog(null, "Curso cadastrado com sucesso!", "Sucesso", 1);
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao cadastrar curso.", "Erro", 0);
        }
    }

    public void update(Curso curso, String itemSelecionado) {
        try {
            Connection conexao;
            PreparedStatement pstSelect, pst;
            ResultSet rsSelect;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sqlSelect = "select id, nome from curso where nome='" + itemSelecionado + "'";
            pstSelect = conexao.prepareStatement(sqlSelect);
            rsSelect = pstSelect.executeQuery();

            //se existir um id do modulo informado
            if (rsSelect.next()) {
                String sql = "update curso set nome = ? where id = " + rsSelect.getString(1);
                pst = conexao.prepareStatement(sql);
                pst.setString(1, curso.getNome());

                pst.executeUpdate();

                //fechando a conexão
                conexao.close();
                JOptionPane.showMessageDialog(null, "Curso editado com sucesso!", "Sucesso", 1);
            }
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao editar curso.", "Erro", 0);
        }
    }

    public void delete(String itemSelecionado) {
        try {
            Connection conexao;
            PreparedStatement pst;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            int idCurso = returnID(itemSelecionado);

            String sql = "UPDATE curso SET status = 0 WHERE id='" + idCurso + "'";
            pst = conexao.prepareStatement(sql);
            pst.executeUpdate();

            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao excluir curso(s).", "Erro", 0);
        }
    }

    public ArrayList<Curso> read() {
        ArrayList<Curso> cursos = new ArrayList();
        try {
            Connection conexao;
            PreparedStatement pstUpdate, pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            //Atualizar tabela módulo de acordo com quantidade de livro por módulo
            String sqlUpdate = "UPDATE curso INNER JOIN "
                    + "(SELECT idCurso, COUNT(*) total FROM livro WHERE livro.status = 1 GROUP BY idCurso) "
                    + "livro ON curso.id = livro.idCurso "
                    + "SET curso.quantLivros = livro.total";
            pstUpdate = conexao.prepareStatement(sqlUpdate);
            pstUpdate.executeUpdate();
            String sql = "select nome, quantLivros, status from curso";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                Curso curso = new Curso();
                curso.setNome(rs.getString("nome"));
                curso.setQuantLivros(rs.getInt("quantLivros"));
                curso.setStatus(rs.getInt("status"));
                cursos.add(curso);
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao listas cursos.", "Erro", 0);
        }
        return cursos;
    }

    public ArrayList<Curso> readUpdate(String itemSelecionado) {
        ArrayList<Curso> cursos = new ArrayList();

        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sqlSelect = "select nome, status from curso where nome='" + itemSelecionado + "'";
            pst = conexao.prepareStatement(sqlSelect);
            rs = pst.executeQuery();

            while (rs.next()) {
                Curso curso = new Curso();
                curso.setNome(rs.getString("nome"));
                curso.setStatus(rs.getInt("status"));
                cursos.add(curso);
            }

            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao solicitar edição de cursos.", "Erro", 0);
        }
        return cursos;
    }

    public ArrayList<Curso> search(String dadoRecebido) {
        ArrayList<Curso> cursos = new ArrayList();
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            int numItensEncontrados = 0;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select nome, quantLivros, status from curso where nome like '%" + dadoRecebido + "%'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                if (rs.getInt("status") == 1) {
                    Curso curso = new Curso();
                    curso.setNome(rs.getString("nome"));
                    curso.setQuantLivros(rs.getInt("quantLivros"));
                    curso.setStatus(rs.getInt("status"));
                    cursos.add(curso);
                    numItensEncontrados++;
                }
            }

            //mensagem se encontrou algo na pesquisa ou não
            if (numItensEncontrados == 0) {
                JOptionPane.showMessageDialog(null, "Nenhum item foi encontrado na pesquisa por " + dadoRecebido, "Aviso", 1);
            } else {
                JOptionPane.showMessageDialog(null, "Encontrado " + numItensEncontrados + " item(ns) na pesquisa por " + dadoRecebido, "Sucesso", 1);
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao realizar a pesquisa.", "Erro", 0);
        }
        return cursos;
    }

    public String returnInfo(int id) {
        String nome = "";
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select nome from curso where id=" + id;
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                nome = rs.getString("nome");
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao retornar a informação.", "Erro", 0);
        }
        return nome;
    }

    public int returnID(String dadoRecebido) {
        int id = 0;
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select id from curso where nome='" + dadoRecebido + "'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                id = rs.getInt("id");
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao retornar o ID.", "Erro", 0);
        }
        return id;
    }
}

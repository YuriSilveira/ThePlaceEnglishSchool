package br.com.theplace.dao;

import br.com.theplace.dal.ModuloConexao;
import br.com.theplace.model.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class UsuariosDAO {

    public void insert(Usuario usuario) {
        Connection conexao;
        PreparedStatement pst;
        //realizando a conexão estipulada na classe ModuloConexao
        conexao = ModuloConexao.conector();
        try {
            String sql = "INSERT INTO usuario (nome, email, senha, status) VALUES (?,?,(MD5(?)),?);";

            pst = conexao.prepareStatement(sql);
            pst.setString(1, usuario.getNome());
            pst.setString(2, usuario.getEmail());
            pst.setString(3, usuario.getSenha());
            pst.setInt(4, usuario.getStatus());
            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Usuário cadastrado com sucesso!", "Sucesso", 1);
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao cadastrar usuário.", "Erro", 0);
        }
    }

    public void update(Usuario usuario, String itemSelecionado) {
        try {
            Connection conexao;
            PreparedStatement pstSelect, pst;
            ResultSet rsSelect;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sqlSelect = "select id, nome from usuario where nome='" + itemSelecionado + "'";
            pstSelect = conexao.prepareStatement(sqlSelect);
            rsSelect = pstSelect.executeQuery();

            //se existir um id do modulo informado
            if (rsSelect.next()) {
                String sql = "update usuario set nome=?, email=?, senha=(MD5(?)), status=? where id = " + rsSelect.getString(1);
                pst = conexao.prepareStatement(sql);
                pst.setString(1, usuario.getNome());
                pst.setString(2, usuario.getEmail());
                pst.setString(3, usuario.getSenha());
                if (usuario.getStatus() == 1) {
                    pst.setInt(4, 1);
                } else {
                    pst.setInt(4, 2);
                }
                pst.executeUpdate();

                //fechando a conexão
                conexao.close();
                JOptionPane.showMessageDialog(null, "Usuário editado com sucesso!", "Sucesso", 1);
            }
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao editar um usuário.", "Erro", 0);
        }
    }

    public void delete(String itemSelecionado) {
        try {
            Connection conexao;
            PreparedStatement pst;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            int idUsuario = returnID(itemSelecionado);
            String sql = "UPDATE usuario SET status = -1 WHERE id='" + idUsuario + "'";
            pst = conexao.prepareStatement(sql);
            pst.executeUpdate();

            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao excluir usuário(s).", "Erro", 0);
        }
    }

    public ArrayList<Usuario> read() {
        ArrayList<Usuario> usuarios = new ArrayList();
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "select nome, email, status from usuario";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                Usuario usuario = new Usuario();
                usuario.setNome(rs.getString("nome"));
                usuario.setEmail(rs.getString("email"));
                usuario.setStatus(rs.getInt("status"));
                usuarios.add(usuario);
            }

            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao carregar a tabela de usuários.", "Erro", 0);
        }
        return usuarios;
    }

    public ArrayList<Usuario> readUpdate(String itemSelecionado) {
        ArrayList<Usuario> usuarios = new ArrayList();

        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "select nome, email, status from usuario where nome='" + itemSelecionado + "'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                Usuario usuario = new Usuario();
                usuario.setNome(rs.getString("nome"));
                usuario.setEmail(rs.getString("email"));
                usuario.setStatus(rs.getInt("status"));
                usuarios.add(usuario);
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao solicitar edição de usuário.", "Erro", 0);
        }
        return usuarios;
    }

    public ArrayList<Usuario> search(String dadoRecebido) {
        ArrayList<Usuario> usuarios = new ArrayList();
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            int numItensEncontrados = 0;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select nome, email, status from usuario where nome like '%" + dadoRecebido + "%'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                if (rs.getInt("status") == 1 || rs.getInt("status") == 2) {
                    Usuario usuario = new Usuario();
                    usuario.setNome(rs.getString("nome"));
                    usuario.setEmail(rs.getString("email"));
                    usuario.setStatus(rs.getInt("status"));
                    usuarios.add(usuario);
                    numItensEncontrados++;
                }
            }

            //mensagem se encontrou algo na pesquisa ou não
            if (numItensEncontrados == 0) {
                JOptionPane.showMessageDialog(null, "Nenhum item foi encontrado na pesquisa por " + dadoRecebido, "Aviso", 1);
            } else {
                JOptionPane.showMessageDialog(null, "Encontrado " + numItensEncontrados + " item(ns) na pesquisa por " + dadoRecebido, "Sucesso", 1);
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao realizar a pesquisa.", "Erro", 0);
        }
        return usuarios;
    }

    public String returnInfo(int id) {
        String nome = "";
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select nome from usuario where id=" + id;
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                nome = rs.getString("nome");
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao retornar a informação.", "Erro", 0);
        }
        return nome;
    }

    public int returnID(String dadoRecebido) {
        int id = 0;
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select id from usuario where nome='" + dadoRecebido + "'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                id = rs.getInt("id");
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao retornar o ID.", "Erro", 0);
        }
        return id;
    }

    public int newPassword(int id, String hashTemporaria, String novaSenha) {
        int executouQuery = 0;
        try {
            Connection conexao;
            PreparedStatement pst;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "update usuario set hashTemporaria = ?, senha = (MD5(?)) where "
                    + "id = '" + id + "' "
                    + "and hashTemporaria = '" + hashTemporaria + "'";
            pst = conexao.prepareStatement(sql);
            pst.setString(1, "");
            pst.setString(2, novaSenha);
            executouQuery = pst.executeUpdate();

            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao alterar senha de usuário.", "Erro", 0);
        }
        return executouQuery;
    }

    public int findEmail(String emailUsuario) {
        int retornoID = 0;
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "select id from usuario where email='" + emailUsuario + "'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                retornoID = rs.getInt(1);
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao encontrar e-mail de recuperação de senha.", "Erro", 0);
        }
        return retornoID;
    }

    public Usuario login(Usuario usuario) {
        Usuario retorno = null;
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "select * from usuario where email=? and senha=(MD5(?))";
            pst = conexao.prepareStatement(sql);
            pst.setString(1, usuario.getEmail());
            pst.setString(2, usuario.getSenha());
            rs = pst.executeQuery();
            //se existir usuário e senha correspondente
            if (rs.next()) {
                String nomeUsuario = rs.getString(2);
                int status = rs.getInt(6);
                usuario.setNome(nomeUsuario);
                usuario.setStatus(status);
                retorno = usuario;
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao tentar logar no sistema.", "Erro", 0);
        }
        return retorno;
    }
}

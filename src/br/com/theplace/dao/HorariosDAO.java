package br.com.theplace.dao;

import br.com.theplace.dal.ModuloConexao;
import br.com.theplace.model.Horario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class HorariosDAO {

    private int retornoID;

    public int insert(Horario horario) {
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "INSERT INTO horario (diaSemana, horaInicial, horaFinal, idTurma, status) VALUES (?,?,?,?,?);";
            pst = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pst.setString(1, horario.getDiaSemana());
            pst.setString(2, horario.getHoraInicial());
            pst.setString(3, horario.getHoraFinal());
            pst.setInt(4, horario.getIdTurma());
            pst.setInt(5, horario.getStatus());
            pst.executeUpdate();
            rs = pst.getGeneratedKeys();
            rs.next();
            retornoID = rs.getInt(1);

            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao cadastrar horario.", "Erro", 0);
        }
        return retornoID;
    }

    public ArrayList<Horario> read(int id) {
        ArrayList<Horario> horarios = new ArrayList();
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "SELECT diaSemana, horaInicial, horaFinal, idTurma, status "
                    + "FROM horario WHERE idTurma= '" + id + "'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                Horario horario = new Horario();
                horario.setDiaSemana(rs.getString("diaSemana"));
                horario.setHoraInicial(rs.getString("horaInicial"));
                horario.setHoraFinal(rs.getString("horaFinal"));
                horario.setIdTurma(rs.getInt("idTurma"));
                horario.setStatus(rs.getInt("status"));
                horarios.add(horario);
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao buscar dados de horarios.", "Erro", 0);
        }
        return horarios;
    }

    public ArrayList<Horario> readComboBox() {
        ArrayList<Horario> horarios = new ArrayList();
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "SELECT diaSemana, horaInicial, horaFinal, status FROM horario";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                Horario horario = new Horario();
                horario.setDiaSemana(rs.getString("diaSemana"));
                horario.setHoraInicial(rs.getString("horaInicial"));
                horario.setHoraFinal(rs.getString("horaFinal"));
                horario.setStatus(rs.getInt("status"));
                horarios.add(horario);
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao buscar dados de horarios para combobox.", "Erro", 0);
        }
        return horarios;
    }

    public ArrayList<Horario> retornoInfo(int id) {
        ArrayList<Horario> horarios = new ArrayList();
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "SELECT diaSemana, horaInicial, horaFinal FROM horario WHERE id='" + id + "'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()) {
                Horario horario = new Horario();
                horario.setDiaSemana(rs.getString("diaSemana"));
                horario.setHoraInicial(rs.getString("horaInicial"));
                horario.setHoraFinal(rs.getString("horaFinal"));
                horarios.add(horario);
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao buscar dados de horarios.", "Erro", 0);
        }
        return horarios;
    }

}

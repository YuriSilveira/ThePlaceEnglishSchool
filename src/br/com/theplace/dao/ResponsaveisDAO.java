package br.com.theplace.dao;

import br.com.theplace.dal.ModuloConexao;
import br.com.theplace.model.Responsavel;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class ResponsaveisDAO {

    private int retornoID;

    public int insert(Responsavel responsavel) {
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "INSERT INTO responsavel (nome, dataNascimento, rg, "
                    + "cpf, email, imagem, telefoneA, telefoneB, telefoneC, "
                    + "idEndereco) VALUES (?,?,?,?,?,?,?,?,?,?);";
            pst = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pst.setString(1, responsavel.getNome());
            pst.setString(2, responsavel.getDataNascimento());
            pst.setString(3, responsavel.getRg());
            pst.setString(4, responsavel.getCpf());
            pst.setString(5, responsavel.getEmail());
            pst.setString(6, responsavel.getImagem());
            pst.setString(7, responsavel.getTelefoneA());
            pst.setString(8, responsavel.getTelefoneB());
            pst.setString(9, responsavel.getTelefoneC());
            pst.setInt(10, responsavel.getIdEndereco());
            pst.executeUpdate();
            rs = pst.getGeneratedKeys();
            rs.next();
            retornoID = rs.getInt(1);

            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao cadastrar responsável.", "Erro", 0);
        }
        return retornoID;
    }

    public ArrayList<Responsavel> readUpdate(String itemSelecionado) {
        ArrayList<Responsavel> responsaveis = new ArrayList();
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sqlSelect = "SELECT responsavel.nome, responsavel.dataNascimento, "
                    + "responsavel.rg, responsavel.cpf, responsavel.email, "
                    + "responsavel.telefoneA, responsavel.telefoneB, "
                    + "responsavel.telefoneC, responsavel.imagem, responsavel.idEndereco "
                    + "FROM responsavel WHERE responsavel.id = '" + itemSelecionado + "'";
            pst = conexao.prepareStatement(sqlSelect);
            rs = pst.executeQuery();

            while (rs.next()) {
                Responsavel responsavel = new Responsavel();
                responsavel.setNome(rs.getString("nome"));
                responsavel.setDataNascimento(rs.getString("dataNascimento"));
                responsavel.setRg(rs.getString("rg"));
                responsavel.setCpf(rs.getString("cpf"));
                responsavel.setEmail(rs.getString("email"));
                responsavel.setTelefoneA(rs.getString("telefoneA"));
                responsavel.setTelefoneB(rs.getString("telefoneB"));
                responsavel.setTelefoneC(rs.getString("telefoneC"));
                responsavel.setImagem(rs.getString("imagem"));
                responsavel.setIdEndereco(rs.getInt("idEndereco"));
                responsaveis.add(responsavel);
            }
            //fechando a conexão
            conexao.close();

        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao solicitar edição de responsáveis.", "Erro", 0);
        }
        return responsaveis;
    }

    public String returnInfo(int id) {
        String nome = "";
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select nome from responsavel where id=" + id;
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                nome = rs.getString("nome");
            }

            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao retornar a informação.", "Erro", 0);
        }
        return nome;
    }

    public String returnInfoTelefone(int id) {
        String telefoneA = "";
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select telefoneA from responsavel where id=" + id;
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                telefoneA = rs.getString("telefoneA");
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao retornar a informação.", "Erro", 0);
        }
        return telefoneA;
    }

    public int returnIDEndereco(int idResponsavel) {
        int id = 0;
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select idEndereco from responsavel where id='" + idResponsavel + "'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                id = rs.getInt("idEndereco");
            }

            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao retornar o ID.", "Erro", 0);
        }
        return id;
    }

    public void update(Responsavel responsavel, int id) {
        try {
            Connection conexao;
            PreparedStatement pst;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "update responsavel set nome=?, dataNascimento=?, rg=?, "
                    + "cpf=?, email=?, imagem=?, telefoneA=?, telefoneB=?, "
                    + "telefoneC=?, idEndereco=? where id=" + id;
            pst = conexao.prepareStatement(sql);
            pst.setString(1, responsavel.getNome());
            pst.setString(2, responsavel.getDataNascimento());
            pst.setString(3, responsavel.getRg());
            pst.setString(4, responsavel.getCpf());
            pst.setString(5, responsavel.getEmail());
            pst.setString(6, responsavel.getImagem());
            pst.setString(7, responsavel.getTelefoneA());
            pst.setString(8, responsavel.getTelefoneB());
            pst.setString(9, responsavel.getTelefoneC());
            pst.setInt(10, responsavel.getIdEndereco());
            pst.executeUpdate();
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao editar responsável.", "Erro", 0);
        }
    }

}

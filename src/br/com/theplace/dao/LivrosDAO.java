package br.com.theplace.dao;

import br.com.theplace.dal.ModuloConexao;
import br.com.theplace.model.Livro;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class LivrosDAO {

    public void insert(Livro livro) {
        try {
            Connection conexao;
            PreparedStatement pst;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "INSERT INTO livro (nome, idCurso, status) VALUES (?,?,?);";
            pst = conexao.prepareStatement(sql);
            pst.setString(1, livro.getNome());
            pst.setInt(2, livro.getIdCurso());
            pst.setInt(3, livro.getStatus());

            pst.executeUpdate();

            JOptionPane.showMessageDialog(null, "Livro cadastrado com sucesso!", "Sucesso", 1);
            //fechando a conexão
            conexao.close();

        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao cadastrar livro.", "Erro", 0);
        }
    }

    public void update(Livro livro, String itemSelecionado) {
        Connection conexao;
        PreparedStatement pstSelect, pst;
        ResultSet rsSelect;

        //realizando a conexão estipulada na classe ModuloConexao
        conexao = ModuloConexao.conector();
        try {
            String sqlSelect = "SELECT livro.id, livro.nome, curso.nome AS nomeCurso "
                    + "FROM livro INNER JOIN curso ON livro.idCurso = curso.id "
                    + "WHERE livro.nome ='" + itemSelecionado + "'";
            pstSelect = conexao.prepareStatement(sqlSelect);
            rsSelect = pstSelect.executeQuery();

            //se existir um id do modulo informado
            if (rsSelect.next()) {
                String sql = "update livro set nome = ?, idCurso = ? where id = " + rsSelect.getInt("id");
                pst = conexao.prepareStatement(sql);
                pst.setString(1, livro.getNome());
                pst.setInt(2, livro.getIdCurso());

                pst.executeUpdate();
                JOptionPane.showMessageDialog(null, "Livro editado com sucesso!", "Sucesso", 1);
            }
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao editar livro.", "Erro", 0);
        }
    }

    public void delete(String itemSelecionado) {
        Connection conexao;
        PreparedStatement pstSelect, pst;
        ResultSet rsSelect;
        int idLivro;
        int idCurso;

        //realizando a conexão estipulada na classe ModuloConexao
        conexao = ModuloConexao.conector();
        try {
            String sqlSelect = "SELECT livro.id, livro.idCurso "
                    + "FROM livro INNER JOIN curso ON livro.idCurso = curso.id "
                    + "WHERE livro.nome='" + itemSelecionado + "'";
            pstSelect = conexao.prepareStatement(sqlSelect);
            rsSelect = pstSelect.executeQuery();

            //se existir um id
            if (rsSelect.next()) {
                idLivro = rsSelect.getInt(1);
                idCurso = rsSelect.getInt(2);
                String sql = "UPDATE livro SET status = 0 WHERE id='" + idLivro + "' and idCurso='" + idCurso + "'";
                pst = conexao.prepareStatement(sql);
                pst.executeUpdate();
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao excluir livro(s).", "Erro", 0);
        }
    }

    public ArrayList<Livro> read() {
        ArrayList<Livro> livros = new ArrayList();
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "SELECT livro.nome, livro.idCurso, livro.status FROM livro";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                Livro livro = new Livro();
                livro.setNome(rs.getString("nome"));
                livro.setIdCurso(rs.getInt("idCurso"));
                livro.setStatus(rs.getInt("status"));
                livros.add(livro);
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao listar os livros.", "Erro", 0);
        }
        return livros;
    }

    public ArrayList<Livro> readUpdate(String itemSelecionado) {
        ArrayList<Livro> livros = new ArrayList();
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sqlSelect = "SELECT livro.nome, livro.idCurso, livro.status "
                    + "FROM livro WHERE livro.nome = '" + itemSelecionado + "'";
            pst = conexao.prepareStatement(sqlSelect);
            rs = pst.executeQuery();

            while (rs.next()) {
                Livro livro = new Livro();
                livro.setNome(rs.getString("nome"));
                livro.setIdCurso(rs.getInt("idCurso"));
                livro.setStatus(rs.getInt("status"));
                livros.add(livro);
            }
            //fechando a conexão
            conexao.close();

        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao solicitar edição de livros.", "Erro", 0);
        }
        return livros;
    }

    public ArrayList<Livro> search(String dadoRecebido) {
        ArrayList<Livro> livros = new ArrayList();
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            int numItensEncontrados = 0;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "SELECT nome, idCurso, status FROM livro WHERE (livro.nome LIKE '%" + dadoRecebido + "%')";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                if (rs.getInt("status") == 1) {
                    Livro livro = new Livro();
                    livro.setNome(rs.getString("nome"));
                    livro.setIdCurso(rs.getInt("idCurso"));
                    livro.setStatus(rs.getInt("status"));
                    livros.add(livro);
                    numItensEncontrados++;
                }
            }

            //mensagem se encontrou algo na pesquisa ou não
            if (numItensEncontrados == 0) {
                JOptionPane.showMessageDialog(null, "Nenhum item foi encontrado na pesquisa por " + dadoRecebido, "Aviso", 1);
            } else {
                JOptionPane.showMessageDialog(null, "Encontrado " + numItensEncontrados + " item(ns) na pesquisa por " + dadoRecebido, "Sucesso", 1);
            }
            //fechando a conexão
            conexao.close();

        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao realizar a pesquisa.", "Erro", 0);
        }
        return livros;
    }

    public String returnInfo(int id) {
        String nome = "";
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select nome from livro where id=" + id;
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                nome = rs.getString("nome");
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao retornar a informação.", "Erro", 0);
        }
        return nome;
    }

    public int returnID(String dadoRecebido) {
        int id = 0;
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select id from livro where nome='" + dadoRecebido + "'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                id = rs.getInt("id");
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao retornar o ID.", "Erro", 0);
        }
        return id;
    }

    public int returnIDCurso(int idLivro) {
        int id = 0;
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select idCurso from livro where id='" + idLivro + "'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                id = rs.getInt("idCurso");
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao retornar o ID.", "Erro", 0);
        }
        return id;
    }

}

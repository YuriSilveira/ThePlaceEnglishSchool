package br.com.theplace.dao;

import br.com.theplace.dal.ModuloConexao;
import br.com.theplace.model.Matricula;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class MatriculasDAO {

    private int retornoID;

    public int insert(Matricula matricula) {
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "INSERT INTO matricula (tipoPagamento, idLivro) VALUES (?,?);";
            pst = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pst.setString(1, matricula.getTipoPagamento());
            pst.setInt(2, matricula.getIdLivro());
            pst.executeUpdate();
            rs = pst.getGeneratedKeys();
            rs.next();
            retornoID = rs.getInt(1);

            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao cadastrar matricula.", "Erro", 0);
        }
        return retornoID;
    }

    public String returnInfo(int id) {
        String nome = "";
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select tipoPagamento from matricula where id=" + id;
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                nome = rs.getString("tipoPagamento");
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao retornar a informação.", "Erro", 0);
        }
        return nome;
    }

    public int returnIDLivro(int idMatricula) {
        int id = 0;
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select idLivro from matricula where id='" + idMatricula + "'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                id = rs.getInt("idLivro");
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao retornar o ID do livro.", "Erro", 0);
        }
        return id;
    }

    public void update(Matricula matricula, int id) {
        try {
            Connection conexao;
            PreparedStatement pst;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "update matricula set tipoPagamento=?, idLivro=? where id=" + id;
            pst = conexao.prepareStatement(sql);
            pst.setString(1, matricula.getTipoPagamento());
            pst.setInt(2, matricula.getIdLivro());
            pst.executeUpdate();
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao editar matrícula.", "Erro", 0);
        }
    }

}

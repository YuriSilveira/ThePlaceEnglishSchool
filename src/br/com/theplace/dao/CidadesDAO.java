package br.com.theplace.dao;

import br.com.theplace.dal.ModuloConexao;
import br.com.theplace.model.Cidade;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class CidadesDAO {

    private int retornoID;

    public int insert(Cidade cidade) {
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "INSERT INTO cidade (nome, idEstado) VALUES (?,?);";
            pst = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pst.setString(1, cidade.getNome());
            pst.setInt(2, cidade.getIdEstado());
            pst.executeUpdate();
            rs = pst.getGeneratedKeys();
            rs.next();
            retornoID = rs.getInt(1);

            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao cadastrar cidade.", "Erro", 0);
        }
        return retornoID;
    }

    public ArrayList<Cidade> readUpdate(String itemSelecionado) {
        ArrayList<Cidade> cidades = new ArrayList();
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sqlSelect = "SELECT cidade.nome, cidade.idEstado FROM cidade "
                    + "WHERE cidade.id = '" + itemSelecionado + "'";
            pst = conexao.prepareStatement(sqlSelect);
            rs = pst.executeQuery();

            while (rs.next()) {
                Cidade cidade = new Cidade();
                cidade.setNome(rs.getString("nome"));
                cidade.setIdEstado(rs.getInt("idEstado"));
                cidades.add(cidade);
            }
            //fechando a conexão
            conexao.close();

        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao solicitar edição de cidades.", "Erro", 0);
        }
        return cidades;
    }

    public int returnIDEstado(int idCidade) {
        int id = 0;
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select idEstado from cidade where id='" + idCidade + "'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                id = rs.getInt("idEstado");
            }

            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao retornar o ID do estado.", "Erro", 0);
        }
        return id;
    }

    public void update(Cidade cidade, int id) {
        try {
            Connection conexao;
            PreparedStatement pst;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "update cidade set nome=?, idEstado=? where id=" + id;
            pst = conexao.prepareStatement(sql);
            pst.setString(1, cidade.getNome());
            pst.setInt(2, cidade.getIdEstado());
            pst.executeUpdate();
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao editar cidade.", "Erro", 0);
        }
    }

    public String returnInfo(int id) {
        String nome = "";
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select nome from cidade where id=" + id;
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                nome = rs.getString("nome");
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao retornar a informação.", "Erro", 0);
        }
        return nome;
    }

}

package br.com.theplace.dao;

import br.com.theplace.dal.ModuloConexao;
import br.com.theplace.model.Endereco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class EnderecosDAO {

    private int retornoID;

    public int insert(Endereco endereco) {
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "INSERT INTO endereco (logradouro, numero, complemento, cep, bairro, idCidade) VALUES (?,?,?,?,?,?);";
            pst = conexao.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pst.setString(1, endereco.getLogradouro());
            pst.setInt(2, endereco.getNumero());
            pst.setString(3, endereco.getComplemento());
            pst.setString(4, endereco.getCep());
            pst.setString(5, endereco.getBairro());
            pst.setInt(6, endereco.getIdCidade());
            pst.executeUpdate();
            rs = pst.getGeneratedKeys();
            rs.next();
            retornoID = rs.getInt(1);

            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao cadastrar endereço.", "Erro", 0);
        }
        return retornoID;
    }

    public ArrayList<Endereco> readUpdate(String itemSelecionado) {
        ArrayList<Endereco> enderecos = new ArrayList();
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sqlSelect = "SELECT endereco.logradouro, endereco.numero, "
                    + "endereco.complemento, endereco.cep, endereco.bairro, "
                    + "endereco.idCidade FROM endereco "
                    + "WHERE endereco.id = '" + itemSelecionado + "'";
            pst = conexao.prepareStatement(sqlSelect);
            rs = pst.executeQuery();

            while (rs.next()) {
                Endereco endereco = new Endereco();
                endereco.setLogradouro(rs.getString("logradouro"));
                endereco.setNumero(rs.getInt("numero"));
                endereco.setComplemento(rs.getString("complemento"));
                endereco.setCep(rs.getString("cep"));
                endereco.setBairro(rs.getString("bairro"));
                endereco.setIdCidade(rs.getInt("idCidade"));
                enderecos.add(endereco);
            }
            //fechando a conexão
            conexao.close();

        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao solicitar edição de endereços.", "Erro", 0);
        }
        return enderecos;
    }

    public int returnIDCidade(int idEndereco) {
        int id = 0;
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select idCidade from endereco where id='" + idEndereco + "'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                id = rs.getInt("idCidade");
            }

            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao retornar o ID.", "Erro", 0);
        }
        return id;
    }

    public void update(Endereco endereco, int id) {
        try {
            Connection conexao;
            PreparedStatement pst;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "update endereco set logradouro=?, numero=?, "
                    + "complemento=?, cep=?, bairro=?, idCidade=? where id=" + id;
            pst = conexao.prepareStatement(sql);
            pst.setString(1, endereco.getLogradouro());
            pst.setInt(2, endereco.getNumero());
            pst.setString(3, endereco.getComplemento());
            pst.setString(4, endereco.getCep());
            pst.setString(5, endereco.getBairro());
            pst.setInt(6, endereco.getIdCidade());
            pst.executeUpdate();
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao editar endereço.", "Erro", 0);
        }
    }

}

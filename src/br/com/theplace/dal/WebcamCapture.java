package br.com.theplace.dal;

import com.github.sarxos.webcam.Webcam;
import java.awt.Dimension;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class WebcamCapture extends Thread {

    JLabel lblWebcam;
    Webcam webcam;

    public WebcamCapture(JLabel lblWebcam) {
        webcam = Webcam.getDefault();
        webcam.setViewSize(new Dimension(320, 240));
        this.lblWebcam = lblWebcam;
    }

    @Override
    public void run() {
        try {
            webcam.open();
            while (!Thread.currentThread().isInterrupted()) {
                lblWebcam.setVisible(true);
                Image image = webcam.getImage();
                lblWebcam.setIcon(new ImageIcon(image));
                Thread.sleep(50);

            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
            System.out.println("Erro");
        } finally {
            webcam.close();
            Webcam.shutdown();
            lblWebcam.setIcon(null);
            lblWebcam.setVisible(false);
        }
    }

    public Image tirarFotoWebcam(String urlDiretorioLocal) {
        Image imagem = null;
        try {
            File arquivo = new File(urlDiretorioLocal);
            ImageIO.write(webcam.getImage(), "JPG", arquivo);
            ImageIcon icon = new ImageIcon(arquivo.getAbsolutePath());
            Image image = icon.getImage();
            imagem = image.getScaledInstance(211, 225, java.awt.Image.SCALE_SMOOTH);
        } catch (IOException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao tirar foto.", "Erro", 0);
        }
        return imagem;
    }

}

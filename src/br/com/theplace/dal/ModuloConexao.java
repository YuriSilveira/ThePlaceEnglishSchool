package br.com.theplace.dal;

import java.sql.*;
import javax.swing.JOptionPane;

public class ModuloConexao {

    //método responsavel por estabelecer a conexão com o banco
    public static Connection conector() {
        java.sql.Connection conexao = null;
        //a linha abaixo "chama" o driver de conexão
        String driver = "com.mysql.jdbc.Driver";
        //armazenando informações referentes ao banco
        String url = "jdbc:mysql://sql134.main-hosting.eu:3306/u179127244_place";
        String user = "u179127244_thesg";
        String password = "hf4l06Cj4eaA";
        //dados localhost
//        String url = "jdbc:mysql://localhost:3306/theplace";
//        String user = "root";
//        String password = "teste";
        //estabelecendo a conexão com o banco
        try {
            Class.forName(driver);
            conexao = DriverManager.getConnection(url, user, password);
            return conexao;
        } catch (ClassNotFoundException | SQLException e) {
            //a linha abaixo serve de apoio para esclarecer o erro de conexão
            JOptionPane.showMessageDialog(null, e);
            return null;
        }
    }
}

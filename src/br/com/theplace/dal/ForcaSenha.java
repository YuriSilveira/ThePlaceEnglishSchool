package br.com.theplace.dal;

import java.awt.Color;
import javax.swing.JCheckBox;
import javax.swing.JLabel;

public class ForcaSenha extends Thread {

    JCheckBox jCBTamanho;
    JCheckBox jCBCaracter;
    JCheckBox jCBMaiuscula;
    JLabel lblNivelSenha;
    String senhaInformada;
    boolean resultadoInfo = false;

    public ForcaSenha(JCheckBox jCBTamanho, JCheckBox jCBCaracter, JCheckBox jCBMaiuscula, JLabel lblNivelSenha) {
        this.jCBTamanho = jCBTamanho;
        this.jCBCaracter = jCBCaracter;
        this.jCBMaiuscula = jCBMaiuscula;
        this.lblNivelSenha = lblNivelSenha;
    }

    public void setSenhaInformada(String senhaInformada) {
        this.senhaInformada = senhaInformada;
    }

    public boolean isResultadoInfo() {
        return resultadoInfo;
    }

    private void verificarTamanho(String senha) {
        try {
            if (senha.length() >= 7) {
                jCBTamanho.setEnabled(true);
                jCBTamanho.setSelected(true);
            } else {
                jCBTamanho.setEnabled(false);
                jCBTamanho.setSelected(false);
            }
            Thread.sleep(1);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    private void verificarCaracter(String senha) {
        try {
            boolean resultado = false;
            char[] caracter = senha.toCharArray();
            for (int i = 0; i <= caracter.length - 1; i++) {
                if (caracter[i] == '@' || caracter[i] == '!'
                        || caracter[i] == '_' || caracter[i] == '&'
                        || caracter[i] == '[' || caracter[i] == ']'
                        || caracter[i] == '=' || caracter[i] == '-'
                        || caracter[i] == '|' || caracter[i] == '$') {
                    resultado = true;
                }
            }
            if (resultado == true) {
                jCBCaracter.setEnabled(true);
                jCBCaracter.setSelected(true);
            } else {
                jCBCaracter.setEnabled(false);
                jCBCaracter.setSelected(false);
            }
            Thread.sleep(1);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    private void verificarMaiuscula(String senha) {
        try {
            boolean resultado = false;
            char[] caracter = senha.toCharArray();
            char[] caracteresUpperCase = new char[]{'A', 'B', 'C', 'D', 'E', 'F',
                'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
                'T', 'U', 'V', 'X', 'W', 'Y', 'Z', 'Á', 'É', 'Í', 'Ó', 'Ú',
                'À', 'È', 'Ì', 'Ò', 'Ù', 'Ã', 'Õ', 'Â', 'Ê', 'Î', 'Ô', 'Û'};
            for (int i = 0; i <= caracter.length - 1; i++) {
                for (int j = 0; j < caracteresUpperCase.length - 1; j++) {
                    if (caracter[i] == caracteresUpperCase[j]) {
                        resultado = true;
                    }
                }

            }
            if (resultado == true) {
                jCBMaiuscula.setEnabled(true);
                jCBMaiuscula.setSelected(true);
            } else {
                jCBMaiuscula.setEnabled(false);
                jCBMaiuscula.setSelected(false);
            }
            Thread.sleep(1);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    private void infoAviso() {
        try {
            if (jCBTamanho.isSelected() && jCBCaracter.isSelected() && jCBMaiuscula.isSelected()) {
                lblNivelSenha.setForeground(Color.GREEN);
                resultadoInfo = true;
                lblNivelSenha.setText("Senha FORTE");
            } else if (jCBTamanho.isSelected() && jCBCaracter.isSelected()
                    || jCBTamanho.isSelected() && jCBMaiuscula.isSelected()
                    || jCBCaracter.isSelected() && jCBMaiuscula.isSelected()) {
                lblNivelSenha.setForeground(Color.WHITE);
                resultadoInfo = true;
                lblNivelSenha.setText("Senha BOA");
            } else if (jCBTamanho.isSelected() || jCBCaracter.isSelected()
                    || jCBMaiuscula.isSelected()) {
                lblNivelSenha.setForeground(Color.ORANGE);
                resultadoInfo = false;
                lblNivelSenha.setText("Senha FRACA");
            } else {
                lblNivelSenha.setForeground(Color.RED);
                resultadoInfo = false;
                lblNivelSenha.setText("Senha MUITO FRACA");
            }
            Thread.sleep(1);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            while (senhaInformada != "") {
                verificarTamanho(senhaInformada);
                verificarCaracter(senhaInformada);
                verificarMaiuscula(senhaInformada);
                infoAviso();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}

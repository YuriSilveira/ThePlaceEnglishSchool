package br.com.theplace.dal;

import br.com.theplace.model.Parcela;
import br.com.uol.pagseguro.domain.Address;
import br.com.uol.pagseguro.domain.Sender;
import br.com.uol.pagseguro.domain.Shipping;
import br.com.uol.pagseguro.domain.Item;

public interface PagSeguroInterface {

    String criarPagamento();

    //dados do pagador...
    Sender getSender();

    //dados do frete...
    Shipping getShipping();

    //dados do endereço para o frete
    Address getAddress(int id);

    //dados do produto
    Item getItem(Parcela p);

}

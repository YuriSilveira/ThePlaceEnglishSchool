package br.com.theplace.telas;

import br.com.theplace.dal.ModuloConexao;
import br.com.theplace.dao.RelatoriosDAO;
import br.com.theplace.telasInternas.*;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import net.sf.jasperreports.view.JasperViewer;

public class ThePlace extends javax.swing.JFrame {

    public boolean exibirNotificacao = true;
    public String nomeUsuario;

    public ThePlace() {
        initComponents();
        setIcon();
        this.setExtendedState(ThePlace.MAXIMIZED_BOTH);
        dataAtualizada();
        numInativos();
    }

    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/br/com/theplace/icones/logo.png")));
    }

    //método que centraliza um JInternalFrame (recebido por parâmetro);
    public static void centralizaInternalFrame(JInternalFrame frame) {
        Dimension desktopSize = jDesktopPaneAreaDeTrabalho.getSize();
        Dimension jInternalFrameSize = frame.getSize();
        frame.setLocation((desktopSize.width - jInternalFrameSize.width) / 2,
                (desktopSize.height - jInternalFrameSize.height) / 2);
    }

    /**
     * método que pega a Data Atual e exibe na label
     */
    private void dataAtualizada() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate localDate = LocalDate.now();
        lblData.setText(dtf.format(localDate));
    }

    /**
     * método exibe número de usuários inativos no sistema e interação com
     * thread para um efeito de visualização
     */
    private void numInativos() {
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;
            int contagemInativos = 0;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();

            String sql = "select status from usuario";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            //realizando a contagem
            while (rs.next()) {
                if ("0".equals(rs.getString(1))) {
                    contagemInativos++;
                }
            }
            //fechando a conexão
            conexao.close();

            //se tiver inativos exibe-os, caso contrario não exibe
            if (contagemInativos != 0 && exibirNotificacao == true) {
                lblNumInativos.setVisible(true);
                lblInativo.setVisible(true);
                //setando valor na label de notificação
                lblNumInativos.setText(String.valueOf(contagemInativos));
            } else {
                lblNumInativos.setVisible(false);
                lblInativo.setVisible(false);
            }
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao efetuar contagem de usuários inativos", "Erro", 0);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jFrame1 = new javax.swing.JFrame();
        jPainelAreaDeTrabalho = new javax.swing.JPanel();
        jDesktopPaneAreaDeTrabalho = new javax.swing.JDesktopPane();
        jPainelMenuOpcoes = new javax.swing.JPanel();
        btnEfetuarMatricula = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        lblNomeUsuario = new javax.swing.JLabel();
        lblData = new javax.swing.JLabel();
        btnEfetuarPagamento = new javax.swing.JButton();
        btnGerenUsuarios = new javax.swing.JButton();
        lblInativo = new javax.swing.JLabel();
        lblNumInativos = new javax.swing.JLabel();
        btnGraficos = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu2 = new javax.swing.JMenu();
        jMenuGerenAlunos = new javax.swing.JMenuItem();
        jMenuGerenModulos = new javax.swing.JMenuItem();
        jMenuGerenLivros = new javax.swing.JMenuItem();
        jMenuGerenProfessores = new javax.swing.JMenuItem();
        jMenuGerenTurmas = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        jMenuGeraRelatAlunos = new javax.swing.JMenuItem();
        jMenuGeraRelatAlunoInativo = new javax.swing.JMenuItem();

        javax.swing.GroupLayout jFrame1Layout = new javax.swing.GroupLayout(jFrame1.getContentPane());
        jFrame1.getContentPane().setLayout(jFrame1Layout);
        jFrame1Layout.setHorizontalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jFrame1Layout.setVerticalGroup(
            jFrame1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("The Place English School");
        setResizable(false);

        jDesktopPaneAreaDeTrabalho.addContainerListener(new java.awt.event.ContainerAdapter() {
            public void componentRemoved(java.awt.event.ContainerEvent evt) {
                jDesktopPaneAreaDeTrabalhoComponentRemoved(evt);
            }
        });

        javax.swing.GroupLayout jDesktopPaneAreaDeTrabalhoLayout = new javax.swing.GroupLayout(jDesktopPaneAreaDeTrabalho);
        jDesktopPaneAreaDeTrabalho.setLayout(jDesktopPaneAreaDeTrabalhoLayout);
        jDesktopPaneAreaDeTrabalhoLayout.setHorizontalGroup(
            jDesktopPaneAreaDeTrabalhoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1000, Short.MAX_VALUE)
        );
        jDesktopPaneAreaDeTrabalhoLayout.setVerticalGroup(
            jDesktopPaneAreaDeTrabalhoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 449, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPainelAreaDeTrabalhoLayout = new javax.swing.GroupLayout(jPainelAreaDeTrabalho);
        jPainelAreaDeTrabalho.setLayout(jPainelAreaDeTrabalhoLayout);
        jPainelAreaDeTrabalhoLayout.setHorizontalGroup(
            jPainelAreaDeTrabalhoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPaneAreaDeTrabalho)
        );
        jPainelAreaDeTrabalhoLayout.setVerticalGroup(
            jPainelAreaDeTrabalhoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPaneAreaDeTrabalho)
        );

        btnEfetuarMatricula.setText("Efetuar Matrícula");
        btnEfetuarMatricula.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnEfetuarMatricula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEfetuarMatriculaActionPerformed(evt);
            }
        });

        jPanel1.setForeground(new java.awt.Color(204, 204, 204));

        lblNomeUsuario.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblNomeUsuario.setForeground(new java.awt.Color(255, 0, 0));
        lblNomeUsuario.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblNomeUsuario.setText("Nome:");

        lblData.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblData.setForeground(new java.awt.Color(255, 0, 0));
        lblData.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblData.setText("Data");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblNomeUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, 491, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lblData, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblNomeUsuario)
                    .addComponent(lblData))
                .addContainerGap())
        );

        btnEfetuarPagamento.setText("Efetuar Pagamento");
        btnEfetuarPagamento.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnEfetuarPagamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEfetuarPagamentoActionPerformed(evt);
            }
        });

        btnGerenUsuarios.setText("Gerenciar Usuários");
        btnGerenUsuarios.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnGerenUsuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGerenUsuariosActionPerformed(evt);
            }
        });

        lblInativo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblInativo.setForeground(new java.awt.Color(255, 0, 0));
        lblInativo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblInativo.setText("Inativo(s)");

        lblNumInativos.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblNumInativos.setForeground(new java.awt.Color(255, 0, 0));
        lblNumInativos.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblNumInativos.setText("N");

        btnGraficos.setText("Gráficos");
        btnGraficos.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnGraficos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGraficosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPainelMenuOpcoesLayout = new javax.swing.GroupLayout(jPainelMenuOpcoes);
        jPainelMenuOpcoes.setLayout(jPainelMenuOpcoesLayout);
        jPainelMenuOpcoesLayout.setHorizontalGroup(
            jPainelMenuOpcoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPainelMenuOpcoesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnEfetuarMatricula, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPainelMenuOpcoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPainelMenuOpcoesLayout.createSequentialGroup()
                        .addGap(0, 189, Short.MAX_VALUE)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPainelMenuOpcoesLayout.createSequentialGroup()
                        .addComponent(btnEfetuarPagamento, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnGerenUsuarios, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnGraficos, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblNumInativos, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblInativo)
                        .addGap(56, 56, 56))))
        );
        jPainelMenuOpcoesLayout.setVerticalGroup(
            jPainelMenuOpcoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPainelMenuOpcoesLayout.createSequentialGroup()
                .addGroup(jPainelMenuOpcoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPainelMenuOpcoesLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPainelMenuOpcoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnEfetuarMatricula)
                            .addComponent(btnEfetuarPagamento)
                            .addComponent(btnGerenUsuarios)
                            .addComponent(btnGraficos)))
                    .addGroup(jPainelMenuOpcoesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblNumInativos)
                        .addComponent(lblInativo)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jMenu2.setText("Gerenciar");

        jMenuGerenAlunos.setText("Alunos");
        jMenuGerenAlunos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuGerenAlunosActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuGerenAlunos);

        jMenuGerenModulos.setText("Cursos");
        jMenuGerenModulos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuGerenModulosActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuGerenModulos);

        jMenuGerenLivros.setText("Livros");
        jMenuGerenLivros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuGerenLivrosActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuGerenLivros);

        jMenuGerenProfessores.setText("Professores");
        jMenuGerenProfessores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuGerenProfessoresActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuGerenProfessores);

        jMenuGerenTurmas.setText("Turmas");
        jMenuGerenTurmas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuGerenTurmasActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuGerenTurmas);

        jMenuBar1.add(jMenu2);

        jMenu1.setText("Emitir Relatório");

        jMenuGeraRelatAlunos.setText("Alunos Ativos");
        jMenuGeraRelatAlunos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuGeraRelatAlunosActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuGeraRelatAlunos);

        jMenuGeraRelatAlunoInativo.setText("Alunos Inativos");
        jMenuGeraRelatAlunoInativo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuGeraRelatAlunoInativoActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuGeraRelatAlunoInativo);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPainelMenuOpcoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPainelAreaDeTrabalho, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPainelAreaDeTrabalho, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPainelMenuOpcoes, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnEfetuarMatriculaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEfetuarMatriculaActionPerformed
        EfetuarMatricula JIEfetuarMatricula = new EfetuarMatricula();
        jDesktopPaneAreaDeTrabalho.add(JIEfetuarMatricula);
        JIEfetuarMatricula.setVisible(true);
        centralizaInternalFrame(JIEfetuarMatricula);
    }//GEN-LAST:event_btnEfetuarMatriculaActionPerformed

    private void btnEfetuarPagamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEfetuarPagamentoActionPerformed
        EfetuarPagamento JIEfetuarPagamento = new EfetuarPagamento();
        jDesktopPaneAreaDeTrabalho.add(JIEfetuarPagamento);
        JIEfetuarPagamento.setVisible(true);
        centralizaInternalFrame(JIEfetuarPagamento);
    }//GEN-LAST:event_btnEfetuarPagamentoActionPerformed

    private void jMenuGerenModulosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuGerenModulosActionPerformed
        Cursos JIGerenModulos = new Cursos();
        jDesktopPaneAreaDeTrabalho.add(JIGerenModulos);
        JIGerenModulos.setVisible(true);
        centralizaInternalFrame(JIGerenModulos);
    }//GEN-LAST:event_jMenuGerenModulosActionPerformed

    private void jMenuGerenLivrosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuGerenLivrosActionPerformed
        Livros JIGerenLivros = new Livros();
        jDesktopPaneAreaDeTrabalho.add(JIGerenLivros);
        JIGerenLivros.setVisible(true);
        centralizaInternalFrame(JIGerenLivros);
    }//GEN-LAST:event_jMenuGerenLivrosActionPerformed

    private void btnGerenUsuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGerenUsuariosActionPerformed
        Usuarios JIGerenUsuarios = new Usuarios();
        jDesktopPaneAreaDeTrabalho.add(JIGerenUsuarios);
        lblInativo.setVisible(false);
        lblNumInativos.setVisible(false);
        JIGerenUsuarios.setVisible(true);
        centralizaInternalFrame(JIGerenUsuarios);
    }//GEN-LAST:event_btnGerenUsuariosActionPerformed

    private void jMenuGerenProfessoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuGerenProfessoresActionPerformed
        try {
            Professores JIGerenProfessor = new Professores();
            jDesktopPaneAreaDeTrabalho.add(JIGerenProfessor);
            JIGerenProfessor.setMaximum(true);
            JIGerenProfessor.setVisible(true);
            centralizaInternalFrame(JIGerenProfessor);
        } catch (PropertyVetoException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(rootPane, "Falha ao carregar InternalFrame.", "Erro", 0);
        }
    }//GEN-LAST:event_jMenuGerenProfessoresActionPerformed

    private void jMenuGerenTurmasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuGerenTurmasActionPerformed
        Turmas JIGerenTurmas = new Turmas();
        jDesktopPaneAreaDeTrabalho.add(JIGerenTurmas);
        JIGerenTurmas.setVisible(true);
        centralizaInternalFrame(JIGerenTurmas);
    }//GEN-LAST:event_jMenuGerenTurmasActionPerformed

    private void jMenuGerenAlunosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuGerenAlunosActionPerformed
        try {
            Alunos JIGerenAlunos = new Alunos();
            jDesktopPaneAreaDeTrabalho.add(JIGerenAlunos);
            JIGerenAlunos.setMaximum(true);
            JIGerenAlunos.setVisible(true);
            centralizaInternalFrame(JIGerenAlunos);
        } catch (PropertyVetoException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(rootPane, "Falha ao carregar InternalFrame.", "Erro", 0);
        }
    }//GEN-LAST:event_jMenuGerenAlunosActionPerformed

    private void jMenuGeraRelatAlunosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuGeraRelatAlunosActionPerformed
        RelatoriosDAO relatoriosDAO = new RelatoriosDAO();
        JasperViewer.viewReport(relatoriosDAO.gerarRelatAlunos(), false, Locale.getDefault());
    }//GEN-LAST:event_jMenuGeraRelatAlunosActionPerformed

    private void jDesktopPaneAreaDeTrabalhoComponentRemoved(java.awt.event.ContainerEvent evt) {//GEN-FIRST:event_jDesktopPaneAreaDeTrabalhoComponentRemoved

    }//GEN-LAST:event_jDesktopPaneAreaDeTrabalhoComponentRemoved

    private void jMenuGeraRelatAlunoInativoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuGeraRelatAlunoInativoActionPerformed
        RelatoriosDAO relatoriosDAO = new RelatoriosDAO();
        JasperViewer.viewReport(relatoriosDAO.gerarRelatAlunosInativos(), false, Locale.getDefault());
    }//GEN-LAST:event_jMenuGeraRelatAlunoInativoActionPerformed

    private void btnGraficosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGraficosActionPerformed
        Graficos JIGraficos = new Graficos();
        jDesktopPaneAreaDeTrabalho.add(JIGraficos);
        JIGraficos.setVisible(true);
        centralizaInternalFrame(JIGraficos);
    }//GEN-LAST:event_btnGraficosActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ThePlace.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ThePlace.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ThePlace.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ThePlace.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ThePlace thePlace = new ThePlace();
                thePlace.setVisible(true);
                thePlace.nomeUsuario = lblNomeUsuario.getText();
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEfetuarMatricula;
    private javax.swing.JButton btnEfetuarPagamento;
    public static javax.swing.JButton btnGerenUsuarios;
    public static javax.swing.JButton btnGraficos;
    public static javax.swing.JDesktopPane jDesktopPaneAreaDeTrabalho;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuGeraRelatAlunoInativo;
    private javax.swing.JMenuItem jMenuGeraRelatAlunos;
    private javax.swing.JMenuItem jMenuGerenAlunos;
    private javax.swing.JMenuItem jMenuGerenLivros;
    private javax.swing.JMenuItem jMenuGerenModulos;
    private javax.swing.JMenuItem jMenuGerenProfessores;
    private javax.swing.JMenuItem jMenuGerenTurmas;
    private javax.swing.JPanel jPainelAreaDeTrabalho;
    private javax.swing.JPanel jPainelMenuOpcoes;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblData;
    public static javax.swing.JLabel lblInativo;
    public static javax.swing.JLabel lblNomeUsuario;
    public static javax.swing.JLabel lblNumInativos;
    // End of variables declaration//GEN-END:variables
}

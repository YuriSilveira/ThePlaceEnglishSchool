package br.com.theplace.telas;

import br.com.theplace.classes.EnviarEmail;
import br.com.theplace.classes.Loading;
import br.com.theplace.dao.UsuariosDAO;
import java.awt.Toolkit;
import java.sql.ResultSet;
import javax.swing.JOptionPane;

public class EsqueceuSenha extends javax.swing.JFrame {

    public EsqueceuSenha() {
        initComponents();
        setIcon();
        getRootPane().setDefaultButton(btnRecuperarSenha);
    }

    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/br/com/theplace/icones/logo.png")));
        jLabelMensagem.setText("<html>Para recuperar sua senha é necessário que insira o seu e-mail e logo <u>será enviado um E-MAIL para você.</u>");
        jLabelTitulo.setText("<html><font color='#cc3399'>RECUPERAR </font><font color='#b82e8a'>SENHA</font></html>");
    }

    private void limparCampos() {
        txtEmail.setText("");
        txtEmail.requestFocus();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabelTitulo = new javax.swing.JLabel();
        btnVoltar = new javax.swing.JButton();
        jLabelMensagem = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        txtEmail = new javax.swing.JTextField();
        btnRecuperarSenha = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("The Place English School ");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabelTitulo.setFont(new java.awt.Font("Microsoft YaHei UI", 1, 18)); // NOI18N
        jLabelTitulo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelTitulo.setText("RECUPERAR SENHA");
        jPanel1.add(jLabelTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 27, 310, -1));

        btnVoltar.setBackground(new java.awt.Color(204, 204, 255));
        btnVoltar.setText("Voltar");
        btnVoltar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVoltarActionPerformed(evt);
            }
        });
        jPanel1.add(btnVoltar, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 10, 103, -1));

        jLabelMensagem.setFont(new java.awt.Font("Microsoft YaHei Light", 0, 14)); // NOI18N
        jLabelMensagem.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelMensagem.setText("E-mail será ENVIADO para o usuário...");
        jPanel1.add(jLabelMensagem, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 62, 310, 57));

        jSeparator2.setBackground(new java.awt.Color(0, 0, 0));
        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 170, 310, 10));

        txtEmail.setFont(new java.awt.Font("Century Gothic", 0, 12)); // NOI18N
        txtEmail.setToolTipText("Insira seu E-mail");
        txtEmail.setBorder(null);
        jPanel1.add(txtEmail, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 141, 310, 30));

        btnRecuperarSenha.setBackground(new java.awt.Color(153, 255, 153));
        btnRecuperarSenha.setText("RECUPERAR");
        btnRecuperarSenha.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRecuperarSenha.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRecuperarSenhaActionPerformed(evt);
            }
        });
        jPanel1.add(btnRecuperarSenha, new org.netbeans.lib.awtextra.AbsoluteConstraints(326, 142, 130, 30));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 463, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btnRecuperarSenhaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRecuperarSenhaActionPerformed
        if ("".equals(txtEmail.getText())) {
            JOptionPane.showMessageDialog(rootPane, "E-mail não foi informado", "Erro", 0);
        } else {
            UsuariosDAO usuariosDAO = new UsuariosDAO();
            int retornoID = usuariosDAO.findEmail(txtEmail.getText());
            System.out.println(retornoID);
            if (retornoID != 0) {
                this.dispose();
                CadNovaSenha cadNovaSenha = new CadNovaSenha();
                CadNovaSenha.lblIdEmailUsuario.setText(String.valueOf(retornoID));
                new Loading(155, cadNovaSenha).start();
                new Thread() {
                    @Override
                    public void run() {
                        EnviarEmail objEnviarEmail = new EnviarEmail();
                        objEnviarEmail.enviarEmailRecuperarSenha(txtEmail.getText());
                    }
                }.start();
            } else {
                JOptionPane.showMessageDialog(rootPane, "E-mail não encontrado no sistema...", "Erro", 0);
                txtEmail.requestFocus();
            }
        }
    }//GEN-LAST:event_btnRecuperarSenhaActionPerformed

    private void btnVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVoltarActionPerformed
        limparCampos();
        this.dispose();
        Login login = new Login();
        login.setVisible(true);
    }//GEN-LAST:event_btnVoltarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EsqueceuSenha.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EsqueceuSenha.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EsqueceuSenha.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EsqueceuSenha.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EsqueceuSenha().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnRecuperarSenha;
    private javax.swing.JButton btnVoltar;
    private javax.swing.JLabel jLabelMensagem;
    private javax.swing.JLabel jLabelTitulo;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JTextField txtEmail;
    // End of variables declaration//GEN-END:variables
}

package br.com.theplace.telasInternas;

import br.com.theplace.dal.ModuloConexao;
import br.com.theplace.dal.WebServiceCep;
import br.com.theplace.dao.CidadesDAO;
import br.com.theplace.dao.EnderecosDAO;
import br.com.theplace.dao.EstadosDAO;
import br.com.theplace.dao.ProfessoresDAO;
import br.com.theplace.model.Cidade;
import br.com.theplace.model.Endereco;
import br.com.theplace.model.Estado;
import br.com.theplace.model.Professor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class Professores extends javax.swing.JInternalFrame {

    boolean permitirEdicao = false;

    public Professores() {
        initComponents();
        getRootPane().setDefaultButton(btnAdicionar);
        //tabela é atualizada ao ser inicializado o jInternalFrame
        atualizaTableProfessores();
    }

    /**
     * Método limpa os campos do jInternalFrame
     */
    private void limparCampos() {
        txtNomeProfessor.setText("");
        txtRg.setText("");
        txtCpf.setText("");
        txtSalario.setText("");
        txtDataNasc.setDate(null);
        txtCepProfessor.setText("");
        txtLogradouroProfessor.setText("");
        txtNumProfessor.setText("");
        txtComplementoProfessor.setText("");
        txtBairroProfessor.setText("");
        txtCidadeProfessor.setText("");
        txtEstadoProfessor.setText("");
        txtPesquisa.setText("");
        permitirEdicao = false;
        txtNomeProfessor.requestFocus();
    }

    /**
     * Método que atualiza a tabela de acordo com os dados do banco
     */
    private void atualizaTableProfessores() {
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "SELECT professor.nome, professor.dataNascimento, "
                    + "professor.rg, professor.cpf, professor.salario, "
                    + "endereco.cep, endereco.logradouro, endereco.numero, "
                    + "endereco.complemento, professor.status FROM professor "
                    + "INNER JOIN endereco ON professor.idEndereco = endereco.id";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            //organizando tabela para inserir os dados na mesma
            DefaultTableModel model = new DefaultTableModel();
            this.jTableProfessor.setModel(model);

            //adicionando colunas na tabela
            model.addColumn("Nome");
            model.addColumn("Data de Nasc.");
            model.addColumn("RG");
            model.addColumn("CPF");
            model.addColumn("Salario");
            model.addColumn("CEP");
            model.addColumn("Logradouro");
            model.addColumn("Num");
            model.addColumn("Complemento");

            //inserindo os dados na tabela em suas respectivas colunas [numeroDeColunasAki]
            while (rs.next()) {
                if ("1".equals(rs.getString(10))) {
                    Object[] fila = new Object[9];

                    for (int x = 0; x < 9; x++) {
                        if (x == 1) {
                            fila[x] = converterDataParaUsuario(rs.getDate(x + 1));
                        } else {
                            fila[x] = rs.getObject(x + 1);
                        }
                    }
                    model.addRow(fila);
                }
            }

            //alinhando no centro os dados que foram inseridos
            DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
            centerRenderer.setHorizontalAlignment(JLabel.CENTER);
            jTableProfessor.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
            jTableProfessor.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
            jTableProfessor.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
            jTableProfessor.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
            jTableProfessor.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);
            jTableProfessor.getColumnModel().getColumn(5).setCellRenderer(centerRenderer);
            jTableProfessor.getColumnModel().getColumn(6).setCellRenderer(centerRenderer);
            jTableProfessor.getColumnModel().getColumn(7).setCellRenderer(centerRenderer);
            jTableProfessor.getColumnModel().getColumn(8).setCellRenderer(centerRenderer);

            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(rootPane, "Falha ao carregar a tabela de professores.", "Erro", 0);
        }
    }

    private void buscaCepAluno(String cep) {
        WebServiceCep webServiceCep = WebServiceCep.searchCep(cep);

        if (webServiceCep.wasSuccessful()) {
            txtLogradouroProfessor.setText(webServiceCep.getLogradouroFull());
            txtCidadeProfessor.setText(webServiceCep.getCidade());
            txtBairroProfessor.setText(webServiceCep.getBairro());
            txtEstadoProfessor.setText(webServiceCep.getUf());
            txtNumProfessor.setText("");
            txtComplementoProfessor.setText("");
            txtNumProfessor.requestFocus();
        } else {
            JOptionPane.showMessageDialog(rootPane, "CEP inválido", "Erro", 0);
        }
    }

    /**
     * Método efetua pesquisa de acordo com o parâmetro recebido
     *
     * @param dadoRecebido - Texto informado pelo usuário
     */
    private void realizarPesquisa(String dadoRecebido) {
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            int numItensEncontrados = 0;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "SELECT professor.nome, professor.dataNascimento, "
                    + "professor.rg, professor.cpf, professor.salario, "
                    + "endereco.cep, endereco.logradouro, endereco.numero, "
                    + "endereco.complemento, professor.status FROM professor "
                    + "INNER JOIN endereco ON professor.idEndereco = endereco.id "
                    + "WHERE (professor.nome LIKE '%" + dadoRecebido + "%' "
                    + "OR professor.rg LIKE '%" + dadoRecebido + "%' "
                    + "OR professor.cpf LIKE '%" + dadoRecebido + "%' "
                    + "OR professor.salario LIKE '%" + dadoRecebido + "%' "
                    + "OR endereco.cep LIKE '%" + dadoRecebido + "%')";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            //organizando tabela para inserir os dados na mesma
            DefaultTableModel model = new DefaultTableModel();
            this.jTableProfessor.setModel(model);

            //adicionando colunas na tabela
            model.addColumn("Nome");
            model.addColumn("Data de Nasc.");
            model.addColumn("RG");
            model.addColumn("CPF");
            model.addColumn("Salario");
            model.addColumn("CEP");
            model.addColumn("Logradouro");
            model.addColumn("Num");
            model.addColumn("Complemento");

            //inserindo os dados na tabela em suas respectivas colunas [numeroDeColunasAki]
            while (rs.next()) {
                if ("1".equals(rs.getString(10))) {
                    Object[] fila = new Object[9];

                    for (int x = 0; x < 9; x++) {
                        if (x == 1) {
                            fila[x] = converterDataParaUsuario((Date) rs.getObject(x + 1));
                        } else {
                            fila[x] = rs.getObject(x + 1);
                        }
                    }
                    model.addRow(fila);
                    numItensEncontrados++;
                }
            }

            //alinhando no centro os dados que foram inseridos
            DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
            centerRenderer.setHorizontalAlignment(JLabel.CENTER);
            jTableProfessor.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
            jTableProfessor.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
            jTableProfessor.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
            jTableProfessor.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
            jTableProfessor.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);
            jTableProfessor.getColumnModel().getColumn(5).setCellRenderer(centerRenderer);
            jTableProfessor.getColumnModel().getColumn(6).setCellRenderer(centerRenderer);
            jTableProfessor.getColumnModel().getColumn(7).setCellRenderer(centerRenderer);
            jTableProfessor.getColumnModel().getColumn(8).setCellRenderer(centerRenderer);

            //mensagem se encontrou algo na pesquisa ou não
            if (numItensEncontrados == 0) {
                JOptionPane.showMessageDialog(rootPane, "Nenhum item foi encontrado na pesquisa por " + dadoRecebido, "Aviso", 1);
                txtPesquisa.setText("");
            } else {
                JOptionPane.showMessageDialog(rootPane, "Encontrado " + numItensEncontrados + " item(ns) na pesquisa por " + dadoRecebido, "Sucesso", 1);
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            JOptionPane.showMessageDialog(rootPane, "Falha ao realizar a pesquisa.", "Erro", 0);
        }
    }

    /**
     * Método verifica se já existi no banco, o dado informado por parâmetro
     *
     * @param dadoRecebido - Texto informado pelo usuário
     * @return - Retorna um boolean
     */
    private boolean verificaSeExisti(String dadoRecebido) {
        boolean result = false;
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select id from livro where nome = '" + dadoRecebido + "'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            if (rs.next()) {
                result = true;
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            Logger.getLogger(Professores.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * Método faz a converção da data para que se adapte a do MySQL
     *
     * @param dataRecebida - dado informado pelo usuário
     * @return String de data convertida
     */
    private String converterDataParaMysql(Date dataRecebida) {
        //recebo 03/09/1996 e devo gravar 1996-09-03(yyyy/mm/dd)
        SimpleDateFormat dataFormato = new SimpleDateFormat("yyyy-MM-dd");
        String resultado = dataFormato.format(dataRecebida);
        return resultado;
    }

    /**
     * Método faz a converção da data do mysql para o Usuário
     *
     * @param dataRecebida - dado informado
     * @return String de data convertida
     */
    private String converterDataParaUsuario(Date dataRecebida) {
        //recebo 1996-09-03 e devo exibir 03/09/1996(dd/mm/yyyy)
        SimpleDateFormat dataFormato = new SimpleDateFormat("dd/MM/yyyy");
        String resultado = dataFormato.format(dataRecebida);
        return resultado;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAdicionar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtNomeProfessor = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableProfessor = new javax.swing.JTable();
        btnEditar = new javax.swing.JButton();
        btnPesquisar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        txtPesquisa = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        btnAtualizarPagina = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtRg = new javax.swing.JFormattedTextField();
        txtCpf = new javax.swing.JFormattedTextField();
        jLabel7 = new javax.swing.JLabel();
        txtCepProfessor = new javax.swing.JFormattedTextField();
        jLabel8 = new javax.swing.JLabel();
        txtLogradouroProfessor = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtNumProfessor = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtComplementoProfessor = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtBairroProfessor = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtCidadeProfessor = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtEstadoProfessor = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtSalario = new javax.swing.JTextField();
        txtDataNasc = new com.toedter.calendar.JDateChooser();

        setClosable(true);
        setIconifiable(true);
        setResizable(true);
        setTitle("Gerenciar Professores");
        setMinimumSize(new java.awt.Dimension(725, 470));
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameActivated(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        btnAdicionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/create.png"))); // NOI18N
        btnAdicionar.setToolTipText("Cadastrar/Editar");
        btnAdicionar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdicionarActionPerformed(evt);
            }
        });

        jLabel1.setText("Nome:");

        jTableProfessor.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableProfessor.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jScrollPane1.setViewportView(jTableProfessor);

        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/edit.png"))); // NOI18N
        btnEditar.setToolTipText("Alterar");
        btnEditar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnPesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/search.png"))); // NOI18N
        btnPesquisar.setToolTipText("Pesquisar");
        btnPesquisar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });

        btnExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/delete.png"))); // NOI18N
        btnExcluir.setToolTipText("Excluir");
        btnExcluir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });

        jLabel3.setText("Pesquisar por Professor:");

        btnAtualizarPagina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/update.png"))); // NOI18N
        btnAtualizarPagina.setToolTipText("Atualizar Janela");
        btnAtualizarPagina.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAtualizarPagina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtualizarPaginaActionPerformed(evt);
            }
        });

        jLabel2.setText("Data de Nascimento:");

        jLabel4.setText("RG:");

        jLabel5.setText("CPF:");

        try {
            txtRg.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###########")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtRg.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        try {
            txtCpf.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtCpf.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel7.setText("CEP:");

        try {
            txtCepProfessor.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("#####-###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtCepProfessor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCepProfessorActionPerformed(evt);
            }
        });

        jLabel8.setText("Logradouro:");

        jLabel9.setText("Nº:");

        jLabel10.setText("Complemento:");

        jLabel11.setText("Bairro:");

        jLabel12.setText("Cidade:");

        jLabel13.setText("Estado:");

        jLabel6.setText("Salário:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(btnAtualizarPagina)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnExcluir)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnEditar)
                                .addGap(20, 20, 20)))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnAdicionar, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(62, 62, 62))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5)
                            .addComponent(jLabel2)
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtNomeProfessor, javax.swing.GroupLayout.DEFAULT_SIZE, 358, Short.MAX_VALUE)
                                .addGap(426, 426, 426))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txtSalario)
                                    .addComponent(txtCpf, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                                    .addComponent(txtRg, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtDataNasc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)
                            .addComponent(jLabel11)
                            .addComponent(jLabel12)
                            .addComponent(jLabel13))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtCidadeProfessor)
                            .addComponent(txtBairroProfessor)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(txtNumProfessor)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtComplementoProfessor, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtLogradouroProfessor)
                            .addComponent(txtEstadoProfessor)
                            .addComponent(txtCepProfessor, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(83, 83, 83))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(txtCepProfessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(4, 4, 4)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(txtLogradouroProfessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10)
                            .addComponent(txtNumProfessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtComplementoProfessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(11, 11, 11)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(txtBairroProfessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12)
                            .addComponent(txtCidadeProfessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtEstadoProfessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNomeProfessor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txtRg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(txtCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(txtSalario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(txtDataNasc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(txtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnPesquisar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAdicionar, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(btnEditar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnExcluir))
                    .addComponent(btnAtualizarPagina))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdicionarActionPerformed
        if (permitirEdicao == false) {
            if ("".equals(txtNomeProfessor.getText()) || "           ".equals(txtRg.getText())
                    || "   .   .   -  ".equals(txtCpf.getText()) || txtDataNasc.getDate() == null
                    || "     -   ".equals(txtCepProfessor.getText()) || "".equals(txtLogradouroProfessor.getText())
                    || "".equals(txtNumProfessor.getText()) || "".equals(txtBairroProfessor.getText())
                    || "".equals(txtCidadeProfessor.getText()) || "".equals(txtEstadoProfessor.getText())) {
                JOptionPane.showMessageDialog(rootPane, "Preencha o(s) campo(s).", "Erro", 0);
                txtNomeProfessor.requestFocus();
            } else {
                Estado estado = new Estado();
                estado.setNome(txtEstadoProfessor.getText());
                EstadosDAO estadosDAO = new EstadosDAO();
                int idEstado = estadosDAO.insert(estado);

                Cidade cidade = new Cidade();
                cidade.setNome(txtCidadeProfessor.getText());
                cidade.setIdEstado(idEstado);
                CidadesDAO cidadesDAO = new CidadesDAO();
                int idCidade = cidadesDAO.insert(cidade);

                Endereco endereco = new Endereco();
                endereco.setLogradouro(txtLogradouroProfessor.getText());
                endereco.setNumero(Integer.valueOf(txtNumProfessor.getText()));
                endereco.setComplemento(txtComplementoProfessor.getText());
                endereco.setCep(txtCepProfessor.getText());
                endereco.setBairro(txtBairroProfessor.getText());
                endereco.setIdCidade(idCidade);
                EnderecosDAO enderecosDAO = new EnderecosDAO();
                int idEndereco = enderecosDAO.insert(endereco);

                Professor professor = new Professor();
                professor.setNome(txtNomeProfessor.getText());
                professor.setDataNascimento(converterDataParaMysql(txtDataNasc.getDate()));
                professor.setRg(txtRg.getText());
                professor.setCpf(txtCpf.getText());
                professor.setSalario(Integer.valueOf(txtSalario.getText()));
                professor.setStatus(1);
                professor.setIdEndereco(idEndereco);
                ProfessoresDAO professoresDAO = new ProfessoresDAO();
                professoresDAO.insert(professor);

                limparCampos();
                atualizaTableProfessores();
            }
        } else if (permitirEdicao == true) {
            if ("".equals(txtNomeProfessor.getText()) || "           ".equals(txtRg.getText())
                    || "   .   .   -  ".equals(txtCpf.getText()) || txtDataNasc.getDate() == null
                    || "     -   ".equals(txtCepProfessor.getText()) || "".equals(txtLogradouroProfessor.getText())
                    || "".equals(txtNumProfessor.getText()) || "".equals(txtBairroProfessor.getText())
                    || "".equals(txtCidadeProfessor.getText()) || "".equals(txtEstadoProfessor.getText())) {
                JOptionPane.showMessageDialog(rootPane, "Preencha o(s) campo(s).", "Erro", 0);
                txtNomeProfessor.requestFocus();
            } else {
                Connection conexao;
                PreparedStatement pstSelect, pst;
                ResultSet rsSelect;

                //realizando a conexão estipulada na classe ModuloConexao
                conexao = ModuloConexao.conector();
                try {
                    String itemSelecionado = jTableProfessor.getValueAt(jTableProfessor.getSelectedRow(), 3).toString();
                    String sqlSelect = "SELECT professor.id AS idProfessor, endereco.id AS idEndereco, "
                            + "cidade.id AS idCidade, estado.id as idEstado FROM professor "
                            + "INNER JOIN endereco ON professor.idEndereco = endereco.id "
                            + "INNER JOIN cidade ON endereco.idCidade = cidade.id "
                            + "INNER JOIN estado ON cidade.idEstado = estado.id "
                            + "WHERE professor.cpf ='" + itemSelecionado + "'";
                    pstSelect = conexao.prepareStatement(sqlSelect);
                    rsSelect = pstSelect.executeQuery();

                    //se existir um id informado
                    if (rsSelect.next()) {
                        String sql;
                        sql = "update professor set nome=?, dataNascimento=?, rg=?, "
                                + "cpf=?, salario=? where id=" + rsSelect.getString(1);
                        pst = conexao.prepareStatement(sql);
                        pst.setString(1, txtNomeProfessor.getText());
                        pst.setString(2, converterDataParaMysql(txtDataNasc.getDate()));
                        pst.setString(3, txtRg.getText());
                        pst.setString(4, txtCpf.getText());
                        pst.setString(5, txtSalario.getText());
                        pst.executeUpdate();

                        sql = "update endereco set logradouro=?, numero=?, complemento=?, "
                                + "cep=?, bairro=? where id=" + rsSelect.getString(2);
                        pst = conexao.prepareStatement(sql);
                        pst.setString(1, txtLogradouroProfessor.getText());
                        pst.setString(2, txtNumProfessor.getText());
                        pst.setString(3, txtComplementoProfessor.getText());
                        pst.setString(4, txtCepProfessor.getText());
                        pst.setString(5, txtBairroProfessor.getText());
                        pst.executeUpdate();

                        sql = "update cidade set nome=? where id=" + rsSelect.getString(3);
                        pst = conexao.prepareStatement(sql);
                        pst.setString(1, txtCidadeProfessor.getText());
                        pst.executeUpdate();

                        sql = "update estado set nome=? where id=" + rsSelect.getString(4);
                        pst = conexao.prepareStatement(sql);
                        pst.setString(1, txtCidadeProfessor.getText());
                        pst.executeUpdate();

                        atualizaTableProfessores();
                        JOptionPane.showMessageDialog(rootPane, "Professor editado com sucesso!", "Sucesso", 1);
                        limparCampos();
                    }
                } catch (SQLException ex) {
                    //aviso caso tenha erro no try
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(rootPane, "Falha ao editar professor.", "Erro", 0);
                }
            }
        }
    }//GEN-LAST:event_btnAdicionarActionPerformed

    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed
        if ("".equals(txtPesquisa.getText())) {
            JOptionPane.showMessageDialog(rootPane, "Preencha o(s) campo(s).", "Erro", 0);
            txtPesquisa.requestFocus();
        } else {
            realizarPesquisa(txtPesquisa.getText());
        }
    }//GEN-LAST:event_btnPesquisarActionPerformed

    private void btnAtualizarPaginaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtualizarPaginaActionPerformed
        atualizaTableProfessores();
        limparCampos();
    }//GEN-LAST:event_btnAtualizarPaginaActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        //verifica se foi selecionado ou não uma linha da tabela
        if (jTableProfessor.getSelectedRowCount() == 0) {
            JOptionPane.showMessageDialog(rootPane, "Selecione uma linha da tabela para efetuar a exclusão.", "Erro", 0);
        } else {
            Connection conexao;
            PreparedStatement pstSelect, pst, pstUpdate;
            ResultSet rsSelect;
            String idLivro;
            String idModulo;
            String quantLivros;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            for (int i = 0; i < jTableProfessor.getSelectedRowCount(); i++) {
                try {
                    String itemSelecionado = jTableProfessor.getValueAt(jTableProfessor.getSelectedRows()[i], 0).toString();
                    String sqlSelect = "SELECT livro.id, livro.idModulo, modulo.quantLivros "
                            + "FROM livro INNER JOIN modulo ON livro.idModulo = modulo.id "
                            + "WHERE livro.nome='" + itemSelecionado + "'";
                    pstSelect = conexao.prepareStatement(sqlSelect);
                    rsSelect = pstSelect.executeQuery();

                    //se existir um id
                    if (rsSelect.next()) {
                        idLivro = rsSelect.getString(1);
                        idModulo = rsSelect.getString(2);
                        quantLivros = rsSelect.getString(3);

                        String sql = "UPDATE livro SET status = 0 WHERE id='" + idLivro + "'";
                        pst = conexao.prepareStatement(sql);
                        pst.executeUpdate();

                        atualizaTableProfessores();
                        JOptionPane.showMessageDialog(rootPane, "Livro(s) excluido(s) com sucesso!", "Sucesso", 1);
                        limparCampos();

                        conexao.close();

                    }
                } catch (SQLException ex) {
                    //aviso caso tenha erro no try
                    JOptionPane.showMessageDialog(rootPane, "Falha ao excluir professor(es).", "Erro", 0);
                }
            }
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        /*verifica se foi selecionado ou não uma linha da tabela
        para que em seguida seja efetuada a edição do mesmo*/
        switch (jTableProfessor.getSelectedRowCount()) {
            case 0:
                JOptionPane.showMessageDialog(rootPane, "Selecione uma linha da tabela para efetuar a edição.", "Erro", 0);
                break;
            case 1:
                Connection conexao;
                PreparedStatement pstSelect;
                ResultSet rsSelect;
                //realizando a conexão estipulada na classe ModuloConexao
                conexao = ModuloConexao.conector();
                try {
                    String itemSelecionado = jTableProfessor.getValueAt(jTableProfessor.getSelectedRow(), 3).toString();
                    String sqlSelect = "SELECT professor.nome, professor.dataNascimento, professor.rg, "
                            + "professor.cpf, professor.salario, endereco.cep, endereco.logradouro, "
                            + "endereco.numero, endereco.complemento, endereco.bairro, professor.status, "
                            + "cidade.nome as cidade, estado.nome as estado FROM professor "
                            + "INNER JOIN endereco ON professor.idEndereco = endereco.id "
                            + "INNER JOIN cidade ON endereco.idCidade = cidade.id "
                            + "INNER JOIN estado ON cidade.idEstado = estado.id "
                            + "WHERE professor.cpf = '" + itemSelecionado + "'";
                    pstSelect = conexao.prepareStatement(sqlSelect);
                    rsSelect = pstSelect.executeQuery();

                    //se existir um id do modulo informado
                    if (rsSelect.next()) {
                        txtNomeProfessor.setText(rsSelect.getString(1));
                        txtDataNasc.setDate(rsSelect.getDate(2));
                        txtRg.setText(rsSelect.getString(3));
                        txtCpf.setText(rsSelect.getString(4));
                        txtSalario.setText(rsSelect.getString(5));
                        txtCepProfessor.setText(rsSelect.getString(6));
                        txtLogradouroProfessor.setText(rsSelect.getString(7));
                        txtNumProfessor.setText(rsSelect.getString(8));
                        txtComplementoProfessor.setText(rsSelect.getString(9));
                        txtBairroProfessor.setText(rsSelect.getString(10));
                        txtCidadeProfessor.setText(rsSelect.getString(12));
                        txtEstadoProfessor.setText(rsSelect.getString(13));
                        permitirEdicao = true;
                        txtNomeProfessor.requestFocus();
                    }
                    //fechando a conexão
                    conexao.close();

                } catch (SQLException ex) {
                    //aviso caso tenha erro no try
                    JOptionPane.showMessageDialog(rootPane, "Falha ao solicitar edição de professor.", "Erro", 0);
                }
                break;
            default:
                JOptionPane.showMessageDialog(rootPane, "Selecione somente uma linha para efetuar a edição.", "Erro", 0);
                break;
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void txtCepProfessorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCepProfessorActionPerformed
        if (!"".equals(txtCepProfessor.getText())) {
            buscaCepAluno(txtCepProfessor.getText());
        } else {
            JOptionPane.showMessageDialog(rootPane, "Preencha o campo para que seja efetuada a busca por CEP.", "Erro", 0);
        }
    }//GEN-LAST:event_txtCepProfessorActionPerformed

    private void formInternalFrameActivated(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameActivated
        jTableProfessor.setDefaultEditor(Object.class, null);
        jTableProfessor.setRowSelectionAllowed(true);
    }//GEN-LAST:event_formInternalFrameActivated


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdicionar;
    private javax.swing.JButton btnAtualizarPagina;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnPesquisar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableProfessor;
    private javax.swing.JTextField txtBairroProfessor;
    private javax.swing.JFormattedTextField txtCepProfessor;
    private javax.swing.JTextField txtCidadeProfessor;
    private javax.swing.JTextField txtComplementoProfessor;
    private javax.swing.JFormattedTextField txtCpf;
    private com.toedter.calendar.JDateChooser txtDataNasc;
    private javax.swing.JTextField txtEstadoProfessor;
    private javax.swing.JTextField txtLogradouroProfessor;
    private javax.swing.JTextField txtNomeProfessor;
    private javax.swing.JTextField txtNumProfessor;
    private javax.swing.JTextField txtPesquisa;
    private javax.swing.JFormattedTextField txtRg;
    private javax.swing.JTextField txtSalario;
    // End of variables declaration//GEN-END:variables
}

package br.com.theplace.telasInternas;

import br.com.theplace.classes.EnviarEmail;
import br.com.theplace.classes.Loading;
import br.com.theplace.dal.ForcaSenha;
import br.com.theplace.dal.ModuloConexao;
import br.com.theplace.dao.UsuariosDAO;
import br.com.theplace.model.Usuario;
import br.com.theplace.telas.ThePlace;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class Usuarios extends javax.swing.JInternalFrame {

    //atributo para verficação da edição
    boolean permitirEdicao = false;

    ForcaSenha forcaSenha;
    boolean ligarThread = false;

    public Usuarios() {
        initComponents();
        getRootPane().setDefaultButton(btnAdicionar);
        //tabela é atualizada ao ser inicializado o jInternalFrame
        atualizaTableUsuarios();
        forcaSenha = new ForcaSenha(jCBTamanho, jCBCaracter, jCBMaiuscula, lblNivelSenha);
    }

    /**
     * Método limpa os campos do jInternalFrame
     */
    private void limparCampos() {
        txtNome.setText("");
        txtEmail.setText("");
        txtSenha.setText("");
        jCBTipoDeUsuário.setSelectedIndex(0);
        txtPesquisa.setText("");
        permitirEdicao = false;
        txtNome.requestFocus();
    }

    /**
     * Método que atualiza a tabela de acordo com os dados do banco
     */
    private void atualizaTableUsuarios() {
        //organizando tabela para inserir os dados na mesma
        DefaultTableModel model = new DefaultTableModel();
        this.jTableUsuarios.setModel(model);

        //adicionando colunas na tabela
        model.addColumn("Nome");
        model.addColumn("E-mail");
        model.addColumn("Tipo de usuário");

        UsuariosDAO usuariosDAO = new UsuariosDAO();
        String tipoUsuario;

        for (Usuario u : usuariosDAO.read()) {
            if (u.getStatus() != -1) {
                switch (u.getStatus()) {
                    case 0:
                        tipoUsuario = "Inativo";
                        break;
                    case 1:
                        tipoUsuario = "Funcionário";
                        break;
                    default:
                        tipoUsuario = "Administrador";
                        break;
                }
                model.addRow(new Object[]{
                    u.getNome(),
                    u.getEmail(),
                    tipoUsuario
                });
            }
        }
        //alinhando no centro os dados que foram inseridos
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        jTableUsuarios.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        jTableUsuarios.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        jTableUsuarios.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
    }

    /**
     * Método efetua pesquisa de acordo com o parâmetro recebido
     */
    private void realizarPesquisa() {
        //organizando tabela para inserir os dados na mesma
        DefaultTableModel model = new DefaultTableModel();
        this.jTableUsuarios.setModel(model);

        //adicionando colunas na tabela
        model.addColumn("Nome");
        model.addColumn("E-mail");
        model.addColumn("Tipo de usuário");

        UsuariosDAO usuariosDAO = new UsuariosDAO();
        String tipoUsuario;

        for (Usuario u : usuariosDAO.search(txtPesquisa.getText())) {
            if (u.getStatus() != -1) {
                switch (u.getStatus()) {
                    case 0:
                        tipoUsuario = "Inativo";
                        break;
                    case 1:
                        tipoUsuario = "Funcionário";
                        break;
                    default:
                        tipoUsuario = "Administrador";
                        break;
                }
                model.addRow(new Object[]{
                    u.getNome(),
                    u.getEmail(),
                    tipoUsuario
                });
            }
        }

        //alinhando no centro os dados que foram inseridos
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        jTableUsuarios.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        jTableUsuarios.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        jTableUsuarios.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
    }

    /**
     * Método verifica se já existi no banco, o dado informado por parâmetro
     *
     * @param dadoRecebido - Texto informado pelo usuário
     * @return - Retorna um boolean
     */
    private boolean verificaSeExisti(String dadoRecebido) {
        boolean result = false;
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select id from usuario where nome = '" + dadoRecebido + "'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            if (rs.next()) {
                result = true;
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(rootPane, "Falha ao verificar se existi.", "Erro", 0);
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAdicionar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableUsuarios = new javax.swing.JTable();
        btnEditar = new javax.swing.JButton();
        btnPesquisar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        txtPesquisa = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        btnAtualizarPagina = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtEmail = new javax.swing.JTextField();
        txtSenha = new javax.swing.JPasswordField();
        jLabel5 = new javax.swing.JLabel();
        jCBTipoDeUsuário = new javax.swing.JComboBox<>();
        btnAtivaUsuario = new javax.swing.JButton();
        btnDesativaUsuario = new javax.swing.JButton();
        jPanelForcaSenha = new javax.swing.JPanel();
        jCBCaracter = new javax.swing.JCheckBox();
        jCBMaiuscula = new javax.swing.JCheckBox();
        jCBTamanho = new javax.swing.JCheckBox();
        lblNivelSenha = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setResizable(true);
        setTitle("Gerenciar Usuários");
        setMinimumSize(new java.awt.Dimension(725, 470));
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameActivated(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        btnAdicionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/create.png"))); // NOI18N
        btnAdicionar.setToolTipText("Cadastrar/Editar");
        btnAdicionar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdicionarActionPerformed(evt);
            }
        });

        jLabel1.setText("Nome:");

        jTableUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableUsuarios.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jScrollPane1.setViewportView(jTableUsuarios);

        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/edit.png"))); // NOI18N
        btnEditar.setToolTipText("Alterar");
        btnEditar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnPesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/search.png"))); // NOI18N
        btnPesquisar.setToolTipText("Pesquisar");
        btnPesquisar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });

        btnExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/delete.png"))); // NOI18N
        btnExcluir.setToolTipText("Excluir");
        btnExcluir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });

        jLabel3.setText("Pesquisar por usuário:");

        btnAtualizarPagina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/update.png"))); // NOI18N
        btnAtualizarPagina.setToolTipText("Atualizar Janela");
        btnAtualizarPagina.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAtualizarPagina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtualizarPaginaActionPerformed(evt);
            }
        });

        jLabel2.setText("E-mail:");

        jLabel4.setText("Senha:");

        txtSenha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtSenhaKeyReleased(evt);
            }
        });

        jLabel5.setText("Usuário:");

        jCBTipoDeUsuário.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Escolha uma das opções", "Funcionário", "Administrador" }));

        btnAtivaUsuario.setText("Ativar");
        btnAtivaUsuario.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAtivaUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtivaUsuarioActionPerformed(evt);
            }
        });

        btnDesativaUsuario.setText("Desativar");
        btnDesativaUsuario.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnDesativaUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDesativaUsuarioActionPerformed(evt);
            }
        });

        jPanelForcaSenha.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jCBCaracter.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        jCBCaracter.setText("Possuir caracteres especiais. [@, !, _, &]");
        jCBCaracter.setEnabled(false);
        jPanelForcaSenha.add(jCBCaracter, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 23, -1, 27));

        jCBMaiuscula.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        jCBMaiuscula.setText("Uma letra maiúscula.");
        jCBMaiuscula.setEnabled(false);
        jPanelForcaSenha.add(jCBMaiuscula, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 50, 229, -1));

        jCBTamanho.setFont(new java.awt.Font("Century Gothic", 0, 11)); // NOI18N
        jCBTamanho.setText("No mínimo 8 caracteres.");
        jCBTamanho.setEnabled(false);
        jPanelForcaSenha.add(jCBTamanho, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 229, -1));

        lblNivelSenha.setBackground(new java.awt.Color(102, 0, 204));
        lblNivelSenha.setFont(new java.awt.Font("Century Gothic", 1, 12)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtSenha, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblNivelSenha))
                    .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, 342, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanelForcaSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addComponent(jLabel5)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(jCBTipoDeUsuário, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(btnAdicionar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnAtivaUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnDesativaUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(298, 298, 298))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(517, Short.MAX_VALUE)
                .addComponent(btnAtualizarPagina)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnExcluir)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnEditar)
                .addGap(22, 22, 22))
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 744, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jCBTipoDeUsuário, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnAdicionar)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txtSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblNivelSenha))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanelForcaSenha, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(19, 19, 19)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnPesquisar)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel3)
                                .addComponent(txtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnAtivaUsuario)
                                .addComponent(btnDesativaUsuario)))))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(btnEditar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnExcluir))
                    .addComponent(btnAtualizarPagina))
                .addGap(19, 19, 19))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdicionarActionPerformed
        if (permitirEdicao == false) {
            if ("".equals(txtNome.getText()) || "".equals(txtEmail.getText()) || "".equals(txtSenha.getText()) || "Escolha uma das opções".equals(jCBTipoDeUsuário.getSelectedItem().toString())) {
                JOptionPane.showMessageDialog(rootPane, "Preencha o(s) campo(s).", "Erro", 0);
                txtNome.requestFocus();
            } else {
                boolean existiJa = verificaSeExisti(txtNome.getText());
                /*Verificando qual foi o retorno no método verificaSeExisti
                Para que seja efetuada a ação de adicionar ou de editar o dado utilizado*/
                if (existiJa == true) {
                    JOptionPane.showMessageDialog(rootPane, "Usuário já cadastrado no sistema", "Erro", 0);
                    txtNome.requestFocus();
                } else if (forcaSenha.isResultadoInfo() == true) {
                    Usuario usuario = new Usuario();
                    usuario.setNome(txtNome.getText());
                    usuario.setEmail(txtEmail.getText());
                    usuario.setSenha(txtSenha.getText());
                    if (jCBTipoDeUsuário.getSelectedItem().toString() == "Funcionário") {
                        usuario.setStatus(1);
                    } else {
                        usuario.setStatus(2);
                    }
                    UsuariosDAO usuariosDAO = new UsuariosDAO();
                    usuariosDAO.insert(usuario);
                    atualizaTableUsuarios();
                    limparCampos();
                } else {
                    JOptionPane.showMessageDialog(rootPane, "Nível de senha é inválido.", "Erro", 0);
                }
            }
        } else if (permitirEdicao == true) {
            if ("".equals(txtNome.getText())) {
                JOptionPane.showMessageDialog(rootPane, "Preencha o(s) campo(s).", "Erro", 0);
                txtNome.requestFocus();
            } else if (forcaSenha.isResultadoInfo() == true) {
                String itemSelecionado = jTableUsuarios.getValueAt(jTableUsuarios.getSelectedRow(), 0).toString();
                Usuario usuario = new Usuario();
                usuario.setNome(txtNome.getText());
                usuario.setEmail(txtEmail.getText());
                usuario.setSenha(txtSenha.getText());
                if (jCBTipoDeUsuário.getSelectedItem().toString() == "Funcionário") {
                    usuario.setStatus(1);
                } else {
                    usuario.setStatus(2);
                }
                UsuariosDAO usuariosDAO = new UsuariosDAO();
                usuariosDAO.update(usuario, itemSelecionado);
                atualizaTableUsuarios();
                limparCampos();
            } else {
                JOptionPane.showMessageDialog(rootPane, "Nível de senha é inválido.", "Erro", 0);
            }
        }
    }//GEN-LAST:event_btnAdicionarActionPerformed

    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed
        if ("".equals(txtPesquisa.getText())) {
            JOptionPane.showMessageDialog(rootPane, "Preencha o(s) campo(s).", "Erro", 0);
            txtPesquisa.requestFocus();
        } else {
            realizarPesquisa();
        }
    }//GEN-LAST:event_btnPesquisarActionPerformed

    private void btnAtualizarPaginaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtualizarPaginaActionPerformed
        atualizaTableUsuarios();
        limparCampos();
    }//GEN-LAST:event_btnAtualizarPaginaActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        //verifica se foi selecionado ou não uma linha da tabela
        if (jTableUsuarios.getSelectedRowCount() == 0) {
            JOptionPane.showMessageDialog(rootPane, "Selecione uma linha da tabela para efetuar a exclusão.", "Erro", 0);
        } else {
            for (int i = 0; i < jTableUsuarios.getSelectedRowCount(); i++) {
                String itemSelecionado = jTableUsuarios.getValueAt(jTableUsuarios.getSelectedRows()[i], 0).toString();
                UsuariosDAO usuariosDAO = new UsuariosDAO();
                usuariosDAO.delete(itemSelecionado);
            }
            atualizaTableUsuarios();
            JOptionPane.showMessageDialog(rootPane, "Usuário(s) excluido(s) com sucesso!", "Sucesso", 1);
            limparCampos();
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        /*verifica se foi selecionado ou não uma linha da tabela
        para que em seguida seja efetuada a edição do mesmo*/
        switch (jTableUsuarios.getSelectedRowCount()) {
            case 0:
                JOptionPane.showMessageDialog(rootPane, "Selecione uma linha da tabela para efetuar a edição.", "Erro", 0);
                break;
            case 1:
                String itemSelecionado = jTableUsuarios.getValueAt(jTableUsuarios.getSelectedRow(), 0).toString();
                UsuariosDAO usuariosDAO = new UsuariosDAO();

                for (Usuario u : usuariosDAO.readUpdate(itemSelecionado)) {
                    if (u.getStatus() == 1 || u.getStatus() == 2) {
                        txtNome.setText(u.getNome());
                        txtEmail.setText(u.getEmail());
                        txtSenha.requestFocus();
                        if (u.getStatus() == 1) {
                            jCBTipoDeUsuário.setSelectedItem("Funcionário");
                        } else {
                            jCBTipoDeUsuário.setSelectedItem("Administrador");
                        }
                        permitirEdicao = true;
                    } else {
                        JOptionPane.showMessageDialog(null, "Não é permitida a alteração de usuários Inativos", "Erro", 0);
                    }
                }
                break;
            default:
                JOptionPane.showMessageDialog(rootPane, "Selecione somente uma linha para efetuar a edição.", "Erro", 0);
                break;
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void btnAtivaUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtivaUsuarioActionPerformed
        /*verifica se foi selecionado ou não uma linha da tabela
        para que em seguida seja efetuada a ATIVAÇÃO do mesmo*/
        switch (jTableUsuarios.getSelectedRowCount()) {
            case 0:
                JOptionPane.showMessageDialog(rootPane, "Selecione uma linha da tabela.", "Erro", 0);
                break;
            case 1:
                Connection conexao;
                PreparedStatement pst;
                ResultSet rsSelect;
                //realizando a conexão estipulada na classe ModuloConexao
                conexao = ModuloConexao.conector();
                try {
                    String itemSelecionado = jTableUsuarios.getValueAt(jTableUsuarios.getSelectedRow(), 0).toString();
                    String sqlSelect = "select id, status, nome, email from usuario where nome='" + itemSelecionado + "'";
                    pst = conexao.prepareStatement(sqlSelect);
                    rsSelect = pst.executeQuery();

                    //se existir um id
                    if (rsSelect.next()) {
                        if (null != rsSelect.getString(2) || "-1".equals(rsSelect.getString(2))) {
                            switch (rsSelect.getString(2)) {
                                case "2":
                                    JOptionPane.showMessageDialog(rootPane, "Usuário ADMINISTRADOR não pode ser modificado!", "Aviso", 1);
                                    break;
                                case "0":
                                    String idUsuario = rsSelect.getString(1);
                                    String nomeUsuario = rsSelect.getString(3);
                                    String emailUsuario = rsSelect.getString(4);
                                    String sql = "UPDATE usuario SET status = 1 WHERE id='" + idUsuario + "'";
                                    pst = conexao.prepareStatement(sql);
                                    pst.executeUpdate();
                                    new Loading(170).start();
                                    new Thread() {
                                        @Override
                                        public void run() {
                                            EnviarEmail enviar = new EnviarEmail();
                                            String[] partesNome = nomeUsuario.split(" ", 2);
                                            enviar.enviarEmailBemVindo(emailUsuario, partesNome[0]);
                                        }
                                    }.start();
                                    atualizaTableUsuarios();
                                    break;
                                default:
                                    JOptionPane.showMessageDialog(rootPane, "Usuário já está ATIVO.", "Erro", 0);
                                    break;
                            }
                        }
                    }

                    //fechando a conexão
                    conexao.close();
                } catch (SQLException ex) {
                    //aviso caso tenha erro no try
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(rootPane, "Falha ao solicitar ativação de usuário.", "Erro", 0);
                }
                break;
            default:
                JOptionPane.showMessageDialog(rootPane, "Selecione somente uma linha da tabela.", "Erro", 0);
                break;
        }
    }//GEN-LAST:event_btnAtivaUsuarioActionPerformed

    private void btnDesativaUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDesativaUsuarioActionPerformed
        /*verifica se foi selecionado ou não uma linha da tabela
        para que em seguida seja efetuada a ATIVAÇÃO do mesmo*/
        switch (jTableUsuarios.getSelectedRowCount()) {
            case 0:
                JOptionPane.showMessageDialog(rootPane, "Selecione uma linha da tabela.", "Erro", 0);
                break;
            case 1:
                Connection conexao;
                PreparedStatement pst;
                ResultSet rsSelect;
                //realizando a conexão estipulada na classe ModuloConexao
                conexao = ModuloConexao.conector();
                try {
                    String itemSelecionado = jTableUsuarios.getValueAt(jTableUsuarios.getSelectedRow(), 0).toString();
                    String sqlSelect = "select id, status from usuario where nome='" + itemSelecionado + "'";
                    pst = conexao.prepareStatement(sqlSelect);
                    rsSelect = pst.executeQuery();

                    //se existir um id
                    if (rsSelect.next()) {
                        if (null != rsSelect.getString(2)) {
                            switch (rsSelect.getString(2)) {
                                case "2":
                                    JOptionPane.showMessageDialog(rootPane, "Usuário ADMINISTRADOR não pode ser modificado!", "Aviso", 1);
                                    break;
                                case "1":
                                    String idModulo = rsSelect.getString(1);
                                    String sql = "UPDATE usuario SET status = 0 WHERE id='" + idModulo + "'";
                                    pst = conexao.prepareStatement(sql);
                                    pst.executeUpdate();
                                    atualizaTableUsuarios();
                                    JOptionPane.showMessageDialog(rootPane, "Usuário DESATIVADO com sucesso!", "Sucesso", 1);
                                    break;
                                default:
                                    JOptionPane.showMessageDialog(rootPane, "Usuário já está DESATIVADO.", "Erro", 0);
                                    break;
                            }
                        }
                    }

                    //fechando a conexão
                    conexao.close();
                } catch (SQLException ex) {
                    //aviso caso tenha erro no try
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(rootPane, "Falha ao solicitar ativação de usuário.", "Erro", 0);
                }
                break;
            default:
                JOptionPane.showMessageDialog(rootPane, "Selecione somente uma linha da tabela.", "Erro", 0);
                break;

        }
    }//GEN-LAST:event_btnDesativaUsuarioActionPerformed

    private void formInternalFrameActivated(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameActivated
        jTableUsuarios.setDefaultEditor(Object.class, null);
        jTableUsuarios.setRowSelectionAllowed(true);
    }//GEN-LAST:event_formInternalFrameActivated

    private void txtSenhaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSenhaKeyReleased
        if (txtSenha.getText().length() != 0) {
            if (ligarThread == false) {
                lblNivelSenha.setText("");
                forcaSenha.start();
                ligarThread = true;
            }
            forcaSenha.setSenhaInformada(txtSenha.getText());
        } else {
            jCBTamanho.setSelected(false);
            jCBCaracter.setSelected(false);
            jCBMaiuscula.setSelected(false);
            lblNivelSenha.setText("");
            ligarThread = false;
            forcaSenha.stop();
            forcaSenha = new ForcaSenha(jCBTamanho, jCBCaracter, jCBMaiuscula, lblNivelSenha);
        }
    }//GEN-LAST:event_txtSenhaKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdicionar;
    private javax.swing.JButton btnAtivaUsuario;
    private javax.swing.JButton btnAtualizarPagina;
    private javax.swing.JButton btnDesativaUsuario;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnPesquisar;
    private javax.swing.JCheckBox jCBCaracter;
    private javax.swing.JCheckBox jCBMaiuscula;
    private javax.swing.JCheckBox jCBTamanho;
    private javax.swing.JComboBox<String> jCBTipoDeUsuário;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanelForcaSenha;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableUsuarios;
    private javax.swing.JLabel lblNivelSenha;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtNome;
    private javax.swing.JTextField txtPesquisa;
    private javax.swing.JPasswordField txtSenha;
    // End of variables declaration//GEN-END:variables
}

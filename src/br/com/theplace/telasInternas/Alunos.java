package br.com.theplace.telasInternas;

import br.com.theplace.dal.ModuloConexao;
import br.com.theplace.dao.AlunosDAO;
import br.com.theplace.dao.HorariosDAO;
import br.com.theplace.dao.RelatoriosDAO;
import br.com.theplace.dao.ResponsaveisDAO;
import br.com.theplace.model.Aluno;
import br.com.theplace.model.Horario;
import br.com.theplace.telas.ThePlace;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.view.JasperViewer;

public class Alunos extends javax.swing.JInternalFrame {

    public Alunos() {
        initComponents();
        //jComboBox é atualizado ao ser inicializado o jInternalFrame
        exibiTurmasCB();
        //tabela é atualizada ao ser inicializado o jInternalFrame
        atualizaTableAlunos();
    }

    /**
     * Método limpa os campos do jInternalFrame
     */
    private void limparCampos() {
        txtPesquisa.setText("");
        jCBTurma.setSelectedIndex(0);
        txtPesquisa.requestFocus();
    }

    /**
     * Método que exibi as turmas no jComboBox
     */
    private void exibiTurmasCB() {
        HorariosDAO horariosDAO = new HorariosDAO();
        for (Horario h : horariosDAO.readComboBox()) {
            if (h.getStatus() == 1) {
                String diaSemana = h.getDiaSemana();
                String horaInicial = h.getHoraInicial();
                String horaFinal = h.getHoraFinal();
                jCBTurma.addItem(diaSemana + " (" + horaInicial + " até " + horaFinal + ")");
            }
        }
    }

    /**
     * Método que atualiza a tabela de acordo com os dados do banco
     */
    private void atualizaTableAlunos() {
        //organizando tabela para inserir os dados na mesma
        DefaultTableModel model = new DefaultTableModel();
        this.jTableAlunos.setModel(model);

        //adicionando colunas na tabela
        model.addColumn("Nome");
        model.addColumn("Data de Nasc.");
        model.addColumn("CPF");
        model.addColumn("E-mail");
        model.addColumn("Telefone");
        model.addColumn("Nome Responsav.");
        model.addColumn("TelefoneA Responsav.");

        AlunosDAO alunosDAO = new AlunosDAO();
        ResponsaveisDAO responsaveisDAO = new ResponsaveisDAO();

        for (Aluno a : alunosDAO.read()) {
            if (a.getStatus() == 1) {
                model.addRow(new Object[]{
                    a.getNome(),
                    a.getDataNascimento(),
                    a.getCpf(),
                    a.getEmail(),
                    a.getTelefone(),
                    responsaveisDAO.returnInfo(a.getIdResponsavel()),
                    responsaveisDAO.returnInfoTelefone(a.getIdResponsavel())
                });
            }
        }

        //alinhando no centro os dados que foram inseridos
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        jTableAlunos.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        jTableAlunos.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        jTableAlunos.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        jTableAlunos.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        jTableAlunos.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);
        jTableAlunos.getColumnModel().getColumn(5).setCellRenderer(centerRenderer);
        jTableAlunos.getColumnModel().getColumn(6).setCellRenderer(centerRenderer);
    }

    /**
     * Método efetua pesquisa de acordo com o parâmetro recebido
     *
     * @param dadoRecebido - Texto informado pelo usuário
     */
    private void realizarPesquisa(String dadoRecebido) {
        //organizando tabela para inserir os dados na mesma
        DefaultTableModel model = new DefaultTableModel();
        this.jTableAlunos.setModel(model);

        //adicionando colunas na tabela
        model.addColumn("Nome");
        model.addColumn("Data de Nasc.");
        model.addColumn("CPF");
        model.addColumn("E-mail");
        model.addColumn("Telefone");
        model.addColumn("Nome Responsav.");
        model.addColumn("TelefoneA Responsav.");

        AlunosDAO alunosDAO = new AlunosDAO();
        ResponsaveisDAO responsaveisDAO = new ResponsaveisDAO();

        for (Aluno a : alunosDAO.search(txtPesquisa.getText().trim())) {
            if (a.getStatus() == 1) {
                model.addRow(new Object[]{
                    a.getNome(),
                    a.getDataNascimento(),
                    a.getCpf(),
                    a.getEmail(),
                    a.getTelefone(),
                    responsaveisDAO.returnInfo(a.getIdResponsavel()),
                    responsaveisDAO.returnInfoTelefone(a.getIdResponsavel())
                });
            }
        }

        //alinhando no centro os dados que foram inseridos
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        jTableAlunos.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        jTableAlunos.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        jTableAlunos.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        jTableAlunos.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        jTableAlunos.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);
        jTableAlunos.getColumnModel().getColumn(5).setCellRenderer(centerRenderer);
        jTableAlunos.getColumnModel().getColumn(6).setCellRenderer(centerRenderer);
    }

    /**
     * Método faz a converção da data para que se adapte a do MySQL
     *
     * @param dataRecebida - dado informado pelo usuário
     * @return String de data convertida
     */
    private String converterDataParaMysql(Date dataRecebida) {
        //recebo 03/09/1996 e devo gravar 1996-09-03(yyyy/mm/dd)
        SimpleDateFormat dataFormato = new SimpleDateFormat("yyyy-MM-dd");
        String resultado = dataFormato.format(dataRecebida);
        return resultado;
    }

    /**
     * Método faz a converção da data do mysql para o Usuário
     *
     * @param dataRecebida - dado informado
     * @return String de data convertida
     */
    private String converterDataParaUsuario(Date dataRecebida) {
        //recebo 1996-09-03 e devo exibir 03/09/1996(dd/mm/yyyy)
        SimpleDateFormat dataFormato = new SimpleDateFormat("dd/MM/yyyy");
        String resultado = dataFormato.format(dataRecebida);
        return resultado;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTableAlunos = new javax.swing.JTable();
        btnEditar = new javax.swing.JButton();
        btnPesquisar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        txtPesquisa = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        btnAtualizarPagina = new javax.swing.JButton();
        jLabel33 = new javax.swing.JLabel();
        jCBTurma = new javax.swing.JComboBox<>();
        btnEmitirCarnes = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setResizable(true);
        setTitle("Gerenciar Alunos");
        setMinimumSize(new java.awt.Dimension(725, 470));
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameActivated(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        jTableAlunos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableAlunos.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jScrollPane1.setViewportView(jTableAlunos);

        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/edit.png"))); // NOI18N
        btnEditar.setToolTipText("Alterar");
        btnEditar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnPesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/search.png"))); // NOI18N
        btnPesquisar.setToolTipText("Pesquisar");
        btnPesquisar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });

        btnExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/delete.png"))); // NOI18N
        btnExcluir.setToolTipText("Excluir");
        btnExcluir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });

        jLabel3.setText("Pesquisar por aluno:");

        btnAtualizarPagina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/update.png"))); // NOI18N
        btnAtualizarPagina.setToolTipText("Atualizar Janela");
        btnAtualizarPagina.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAtualizarPagina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtualizarPaginaActionPerformed(evt);
            }
        });

        jLabel33.setText("Filtrar por turma:");

        jCBTurma.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Escolha uma das opções" }));
        jCBTurma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCBTurmaActionPerformed(evt);
            }
        });

        btnEmitirCarnes.setText("Emitir Carnês");
        btnEmitirCarnes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEmitirCarnesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(72, 72, 72)
                .addComponent(btnEmitirCarnes, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel33)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCBTurma, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1124, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAtualizarPagina)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnExcluir)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnEditar)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3))
                    .addComponent(btnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel33)
                        .addComponent(jCBTurma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnEmitirCarnes)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 17, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(btnEditar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnExcluir))
                    .addComponent(btnAtualizarPagina))
                .addGap(38, 38, 38))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed
        if ("".equals(txtPesquisa.getText())) {
            JOptionPane.showMessageDialog(rootPane, "Preencha o(s) campo(s).", "Erro", 0);
            txtPesquisa.requestFocus();
        } else {
            realizarPesquisa(txtPesquisa.getText());
        }
    }//GEN-LAST:event_btnPesquisarActionPerformed

    private void btnAtualizarPaginaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtualizarPaginaActionPerformed
        atualizaTableAlunos();
        limparCampos();
    }//GEN-LAST:event_btnAtualizarPaginaActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        //verifica se foi selecionado ou não uma linha da tabela
        if (jTableAlunos.getSelectedRowCount() == 0) {
            JOptionPane.showMessageDialog(rootPane, "Selecione uma linha da tabela para efetuar a exclusão.", "Erro", 0);
        } else {
            for (int i = 0; i < jTableAlunos.getSelectedRowCount(); i++) {
                String itemSelecionado = jTableAlunos.getValueAt(jTableAlunos.getSelectedRows()[i], 2).toString();
                AlunosDAO alunosDAO = new AlunosDAO();
                alunosDAO.delete(itemSelecionado);
                System.out.println(itemSelecionado);
            }
            atualizaTableAlunos();
            JOptionPane.showMessageDialog(rootPane, "Aluno(s) excluido(s) com sucesso!", "Sucesso", 1);
            limparCampos();
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        /*verifica se foi selecionado ou não uma linha da tabela
        para que em seguida seja efetuada a edição do mesmo*/
        switch (jTableAlunos.getSelectedRowCount()) {
            case 0:
                JOptionPane.showMessageDialog(rootPane, "Selecione uma linha da tabela para efetuar a edição.", "Erro", 0);
                break;
            case 1:
                String itemSelecionado = jTableAlunos.getValueAt(jTableAlunos.getSelectedRow(), 2).toString();
                AlunosDAO alunosDAO = new AlunosDAO();
                int retornoID = alunosDAO.returnID(itemSelecionado);
                if (retornoID != 0) {
                    this.dispose();
                    EfetuarMatricula JIGerenMatricula = new EfetuarMatricula();
                    JIGerenMatricula.permitirEdicao = true;
                    JIGerenMatricula.idAlunoEdicao = retornoID;
                    ThePlace.jDesktopPaneAreaDeTrabalho.add(JIGerenMatricula);
                    ThePlace.centralizaInternalFrame(JIGerenMatricula);
                    JIGerenMatricula.setVisible(true);
                }
                break;
            default:
                JOptionPane.showMessageDialog(rootPane, "Selecione somente uma linha para efetuar a edição.", "Erro", 0);
                break;
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void formInternalFrameActivated(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameActivated
        jTableAlunos.setDefaultEditor(Object.class, null);
        jTableAlunos.setRowSelectionAllowed(true);
    }//GEN-LAST:event_formInternalFrameActivated

    private void jCBTurmaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCBTurmaActionPerformed
        if (jCBTurma.getSelectedIndex() != 0) {
            try {
                Connection conexao;
                PreparedStatement pst;
                ResultSet rs;

                String[] nomeCB = jCBTurma.getSelectedItem().toString().trim().split("\\(|\\)|até");
                String diaTurma = nomeCB[0];
                String horaInicial = nomeCB[1];
                String horaFinal = nomeCB[2];

                conexao = ModuloConexao.conector();
                String sql = "SELECT horario.idTurma, horario.status "
                        + "FROM horario WHERE horario.diaSemana =  '" + diaTurma.trim() + "'"
                        + "AND horario.horaInicial = '" + horaInicial.trim() + "' AND horario.horaFinal = '" + horaFinal.trim() + "'";
                pst = conexao.prepareStatement(sql);
                rs = pst.executeQuery();

                while (rs.next()) {
                    if ("1".equals(rs.getString(2))) {
                        sql = "SELECT aluno.nome, aluno.dataNascimento, aluno.cpf, aluno.email, "
                                + "aluno.telefone, responsavel.nome AS nomeResponsavel, "
                                + "responsavel.telefoneA, aluno.status FROM aluno "
                                + "INNER JOIN responsavel ON aluno.idResponsavel = responsavel.id "
                                + "WHERE aluno.idTurma = " + rs.getString(1);
                        pst = conexao.prepareStatement(sql);
                        rs = pst.executeQuery();

                        //organizando tabela para inserir os dados na mesma
                        DefaultTableModel model = new DefaultTableModel();
                        this.jTableAlunos.setModel(model);

                        //adicionando colunas na tabela
                        model.addColumn("Nome");
                        model.addColumn("Data de Nasc.");
                        model.addColumn("CPF");
                        model.addColumn("E-mail");
                        model.addColumn("Telefone");
                        model.addColumn("Nome Responsav.");
                        model.addColumn("TelefoneA Responsav.");

                        //inserindo os dados na tabela em suas respectivas colunas [numeroDeColunasAki]
                        while (rs.next()) {
                            if ("1".equals(rs.getString(8))) {
                                Object[] fila = new Object[7];

                                for (int x = 0; x < 7; x++) {
                                    if (x == 1) {
                                        fila[x] = converterDataParaUsuario(rs.getDate(x + 1));
                                    } else {
                                        fila[x] = rs.getObject(x + 1);
                                    }
                                }
                                model.addRow(fila);
                            }
                        }

                        //alinhando no centro os dados que foram inseridos
                        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
                        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
                        jTableAlunos.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
                        jTableAlunos.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
                        jTableAlunos.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
                        jTableAlunos.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
                        jTableAlunos.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);
                        jTableAlunos.getColumnModel().getColumn(5).setCellRenderer(centerRenderer);
                        jTableAlunos.getColumnModel().getColumn(6).setCellRenderer(centerRenderer);

                    }
                }
                //fechando a conexão
                conexao.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(rootPane, "Falha ao reailizar filtro por turma.", "Erro", 0);
            }
        } else {
            atualizaTableAlunos();
        }
    }//GEN-LAST:event_jCBTurmaActionPerformed

    private void btnEmitirCarnesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEmitirCarnesActionPerformed
        if (jTableAlunos.getSelectedRowCount() == 0) {
            JOptionPane.showMessageDialog(rootPane, "Nenhuma linha da tabela foi selecionada.", "Erro", 0);
        } else {
            String itemSelecionado = jTableAlunos.getValueAt(jTableAlunos.getSelectedRow(), 2).toString();
            AlunosDAO alunosDAO = new AlunosDAO();
            int retornoIDMatricula = alunosDAO.returnIDMatricula(itemSelecionado);
            RelatoriosDAO relatoriosDAO = new RelatoriosDAO();
            JasperViewer.viewReport(relatoriosDAO.gerarRelatCarnes(retornoIDMatricula), false, Locale.getDefault());
        }
    }//GEN-LAST:event_btnEmitirCarnesActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAtualizarPagina;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnEmitirCarnes;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnPesquisar;
    private javax.swing.JComboBox<String> jCBTurma;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableAlunos;
    private javax.swing.JTextField txtPesquisa;
    // End of variables declaration//GEN-END:variables
}

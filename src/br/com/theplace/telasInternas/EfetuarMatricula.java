package br.com.theplace.telasInternas;

import br.com.theplace.classes.ValidarCpf;
import br.com.theplace.dal.ModuloConexao;
import br.com.theplace.dal.WebServiceCep;
import br.com.theplace.dal.WebcamCapture;
import br.com.theplace.dao.AlunosDAO;
import br.com.theplace.dao.CidadesDAO;
import br.com.theplace.dao.CursosDAO;
import br.com.theplace.dao.EnderecosDAO;
import br.com.theplace.dao.EstadosDAO;
import br.com.theplace.dao.HorariosDAO;
import br.com.theplace.dao.LivrosDAO;
import br.com.theplace.dao.MatriculasDAO;
import br.com.theplace.dao.ParcelasDAO;
import br.com.theplace.dao.RelatoriosDAO;
import br.com.theplace.dao.ResponsaveisDAO;
import br.com.theplace.model.Aluno;
import br.com.theplace.model.Cidade;
import br.com.theplace.model.Endereco;
import br.com.theplace.model.Estado;
import br.com.theplace.model.Horario;
import br.com.theplace.model.Livro;
import br.com.theplace.model.Matricula;
import br.com.theplace.model.Parcela;
import br.com.theplace.model.Responsavel;
import java.awt.Image;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import net.sf.jasperreports.view.JasperViewer;

public class EfetuarMatricula extends javax.swing.JInternalFrame {

    boolean permitirEdicao = false;
    Thread threadWebcam;
    private final JLabel lblRetornoIDLivro = new JLabel();
    private final JLabel lblRetornoIDTurma = new JLabel();
    private final JLabel lblRetornoIDMatricula = new JLabel();
    private int idEndereco = 0;
    private int idResponsavel = 0;
    private int idMatricula = 0;
    private int idTurma = 0;
    int idAlunoEdicao = 0;
    WebcamCapture webcamCapture;

    public EfetuarMatricula() {
        initComponents();
        //jComboBox é atualizado ao ser inicializado o jInternalFrame
        exibiLivrosCB();
        //jComboBox é atualizado ao ser inicializado o jInternalFrame
        exibiTurmasCB();
        lblRetornoIDLivro.setVisible(false);
        lblRetornoIDTurma.setVisible(false);
        lblRetornoIDMatricula.setVisible(false);
        btnEmitirCarnes.setVisible(false);
        btnEmitirCarnes.setEnabled(false);
        btnTirarFoto.setVisible(false);
        lblValorParcela.setEnabled(true);
        txtValorParcela.setEnabled(true);
        Date diaAtual = new Date();
        txtDataVencParcela.setDate(diaAtual);
        jLabelMoraOuNao.setText("<html><strong>Aluno mora com responsável?</strong> <font size='5' color='red'>Não</font></html>");
    }

    private void buscaCepAluno(String cep) {
        WebServiceCep webServiceCep = WebServiceCep.searchCep(cep);

        if (webServiceCep.wasSuccessful()) {
            txtLogradouroAluno.setText(webServiceCep.getLogradouroFull());
            txtCidadeAluno.setText(webServiceCep.getCidade());
            txtBairroAluno.setText(webServiceCep.getBairro());
            txtEstadoAluno.setText(webServiceCep.getUf());
            txtNumAluno.setText("");
            txtComplementoAluno.setText("");
            txtNumAluno.requestFocus();
        } else {
            JOptionPane.showMessageDialog(rootPane, "Falha de comunicação ao realizar a busca de CEP...", "Erro", 0);
        }
    }

    private boolean buscaCepResponsavel(String cep) {
        WebServiceCep webServiceCep = WebServiceCep.searchCep(cep);
        boolean result = false;
        if (webServiceCep.wasSuccessful()) {
            txtLogradouroResponsavel.setText(webServiceCep.getLogradouroFull());
            txtCidadeResponsavel.setText(webServiceCep.getCidade());
            txtBairroResponsavel.setText(webServiceCep.getBairro());
            txtEstadoResponsavel.setText(webServiceCep.getUf());
            txtNumResponsavel.setText("");
            txtComplementoResponsavel.setText("");
            txtNumResponsavel.requestFocus();
            result = true;
        } else {
            JOptionPane.showMessageDialog(rootPane, "Falha de comunicação ao realizar a busca de CEP...", "Erro", 0);
        }
        return result;
    }

    /**
     * Método limpa os campos
     */
    private void limparCampos() {
        txtNome.setText("");
        txtRG.setText("");
        txtCPF.setText("");
        txtDataNasc.setDate(null);
        txtEmail.setText("");
        txtTelefone.setText("");
        txtCepAluno.setText("");
        txtLogradouroAluno.setText("");
        txtNumAluno.setText("");
        txtComplementoAluno.setText("");
        txtBairroAluno.setText("");
        txtCidadeAluno.setText("");
        txtEstadoAluno.setText("");
        txtNomeRespon.setText("");
        txtDataNascRespon.setDate(null);
        txtEmailRespon.setText("");
        txtRGRespon.setText("");
        txtCPFRespon.setText("");
        imgDocumentos.setText("");
        txtTelefone01Respon.setText("");
        txtTelefone02Respon.setText("");
        txtTelefone03Respon.setText("");
        txtCepResponsavel.setText("");
        txtLogradouroResponsavel.setText("");
        txtNumResponsavel.setText("");
        txtComplementoResponsavel.setText("");
        txtBairroResponsavel.setText("");
        txtCidadeResponsavel.setText("");
        txtEstadoResponsavel.setText("");
        jCBTurma.setSelectedIndex(0);
        jCBLivro.setSelectedIndex(0);
        jCBFormasPagamento.setSelectedIndex(0);
        permitirEdicao = false;
        txtNome.requestFocus();
    }

    /**
     * Método que exibi os livros no jComboBox
     */
    private void exibiLivrosCB() {
        LivrosDAO livrosDAO = new LivrosDAO();
        CursosDAO cursoDAO = new CursosDAO();
        for (Livro l : livrosDAO.read()) {
            if (l.getStatus() == 1) {
                jCBLivro.addItem(l.getNome() + " (" + cursoDAO.returnInfo(l.getIdCurso()) + ")");
            }
        }
    }

    /**
     * Método que exibi as turmas no jComboBox
     */
    private void exibiTurmasCB() {
        Connection conexao;
        PreparedStatement pst;
        ResultSet rs;
        try {
            conexao = ModuloConexao.conector();
            String sql = "SELECT diaSemana, horaInicial, horaFinal, status FROM horario";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            while (rs.next()) {
                if ("1".equals(rs.getString(4))) {
                    String diaSemana = rs.getString(1);
                    String horaInicial = rs.getString(2);
                    String horaFinal = rs.getString(3);
                    jCBTurma.addItem(diaSemana + " (" + horaInicial + " até " + horaFinal + ")");
                }
            }
            conexao.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(rootPane, "Falha exibir Turmas no ComboBox.", "Erro", 0);
        }
    }

    /**
     * Método faz a converção da data para que se adapte a do MySQL
     *
     * @param dataRecebida - dado informado pelo usuário
     * @return String de data convertida
     */
    private String converterDataParaMysql(Date dataRecebida) {
        //recebo 03/09/1996 e devo gravar 1996-09-03(yyyy/mm/dd)
        SimpleDateFormat dataFormato = new SimpleDateFormat("yyyy-MM-dd");
        String resultado = dataFormato.format(dataRecebida);
        return resultado;
    }

    /**
     * Método faz a converção da data do mysql para o Usuário
     *
     * @param dataRecebida - dado informado
     * @return String de data convertida
     */
    private String converterDataParaUsuario(Date dataRecebida) {
        //recebo 1996-09-03 e devo exibir 03/09/1996(dd/mm/yyyy)
        SimpleDateFormat dataFormato = new SimpleDateFormat("dd/MM/yyyy");
        String resultado = dataFormato.format(dataRecebida);
        return resultado;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel5 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        txtEmail = new javax.swing.JTextField();
        txtDataNasc = new com.toedter.calendar.JDateChooser();
        jLabel7 = new javax.swing.JLabel();
        txtCepAluno = new javax.swing.JFormattedTextField();
        jLabel8 = new javax.swing.JLabel();
        txtLogradouroAluno = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtNumAluno = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txtComplementoAluno = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtBairroAluno = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txtCidadeAluno = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtEstadoAluno = new javax.swing.JTextField();
        lblWebcam = new javax.swing.JLabel();
        btnLigarWebcam = new javax.swing.JButton();
        btnTirarFoto = new javax.swing.JButton();
        lblFotoAluno = new javax.swing.JLabel();
        txtCPF = new javax.swing.JFormattedTextField();
        txtTelefone = new javax.swing.JFormattedTextField();
        txtRG = new javax.swing.JFormattedTextField();
        jPanel6 = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        txtNomeRespon = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        txtDataNascRespon = new com.toedter.calendar.JDateChooser();
        jLabel25 = new javax.swing.JLabel();
        txtEmailRespon = new javax.swing.JTextField();
        jLabelMoraOuNao = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txtCepResponsavel = new javax.swing.JFormattedTextField();
        jLabel15 = new javax.swing.JLabel();
        txtLogradouroResponsavel = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txtNumResponsavel = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        txtComplementoResponsavel = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txtBairroResponsavel = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txtCidadeResponsavel = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        txtEstadoResponsavel = new javax.swing.JTextField();
        jLabel27 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        imgDocumentos = new javax.swing.JFormattedTextField();
        btnSelecionaFile = new javax.swing.JButton();
        lblFotoDocumentos = new javax.swing.JLabel();
        jCBAlunoResponsavel = new javax.swing.JCheckBox();
        jCBAlunoMoraRespon = new javax.swing.JCheckBox();
        txtCPFRespon = new javax.swing.JFormattedTextField();
        txtTelefone01Respon = new javax.swing.JFormattedTextField();
        txtTelefone02Respon = new javax.swing.JFormattedTextField();
        txtTelefone03Respon = new javax.swing.JFormattedTextField();
        txtRGRespon = new javax.swing.JFormattedTextField();
        jPanel7 = new javax.swing.JPanel();
        jLabel30 = new javax.swing.JLabel();
        jCBFormasPagamento = new javax.swing.JComboBox<>();
        jLabel33 = new javax.swing.JLabel();
        jCBTurma = new javax.swing.JComboBox<>();
        jLabel31 = new javax.swing.JLabel();
        jCBLivro = new javax.swing.JComboBox<>();
        btnAtualizarPagina = new javax.swing.JButton();
        btnAdicionar = new javax.swing.JButton();
        btnEmitirCarnes = new javax.swing.JButton();
        txtDataVencParcela = new com.toedter.calendar.JDateChooser();
        jLabel32 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jCBNumParcelas = new javax.swing.JComboBox<>();
        lblValorParcela = new javax.swing.JLabel();
        txtValorParcela = new javax.swing.JTextField();

        setClosable(true);
        setIconifiable(true);
        setResizable(true);
        setTitle("Efetuar Matrícula");
        setMinimumSize(new java.awt.Dimension(725, 470));
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameActivated(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));

        jTabbedPane1.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);

        jLabel4.setText("CPF:");

        jLabel2.setText("Data de Nascimento:");

        jLabel6.setText("Telefone:");

        jLabel1.setText("Nome:");

        jLabel3.setText("RG:");

        jLabel5.setText("E-mail:");

        jLabel7.setText("CEP:");

        try {
            txtCepAluno.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("#####-###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtCepAluno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCepAlunoActionPerformed(evt);
            }
        });

        jLabel8.setText("Logradouro:");

        jLabel9.setText("Nº:");

        jLabel10.setText("Complemento:");

        jLabel11.setText("Bairro:");

        jLabel12.setText("Cidade:");

        jLabel13.setText("Estado:");

        lblWebcam.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btnLigarWebcam.setText("Webcam");
        btnLigarWebcam.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLigarWebcamActionPerformed(evt);
            }
        });

        btnTirarFoto.setText("Tirar Foto");
        btnTirarFoto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTirarFotoActionPerformed(evt);
            }
        });

        lblFotoAluno.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblFotoAluno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/imagens/user.jpg"))); // NOI18N
        lblFotoAluno.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        try {
            txtCPF.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtCPF.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtCPFFocusLost(evt);
            }
        });

        try {
            txtTelefone.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##) #########")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        try {
            txtRG.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##########")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txtNome, javax.swing.GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE)
                    .addComponent(txtDataNasc, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
                    .addComponent(txtEmail, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
                    .addComponent(txtCPF)
                    .addComponent(txtTelefone)
                    .addComponent(txtRG))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtCepAluno, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtLogradouroAluno, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(txtNumAluno, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtComplementoAluno))
                            .addComponent(txtBairroAluno, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCidadeAluno, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtEstadoAluno, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(lblFotoAluno, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(38, 38, 38))
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(btnLigarWebcam, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnTirarFoto, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblWebcam, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 261, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(421, Short.MAX_VALUE))
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtDataNasc, txtEmail, txtNome});

        jPanel5Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel1, jLabel2, jLabel3, jLabel4});

        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txtNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtRG, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txtCPF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(17, 17, 17)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(txtDataNasc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(15, 15, 15)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(txtEmail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(txtTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(txtCepAluno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtLogradouroAluno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtNumAluno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtComplementoAluno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtBairroAluno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtCidadeAluno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtEstadoAluno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(13, 13, 13)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnTirarFoto)
                    .addComponent(btnLigarWebcam))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblWebcam, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblFotoAluno, javax.swing.GroupLayout.PREFERRED_SIZE, 203, Short.MAX_VALUE)))
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel1, jLabel2, jLabel3, jLabel4, txtDataNasc, txtNome});

        jTabbedPane1.addTab("Dados Pessoais", jPanel5);

        jLabel21.setText("Nome:");

        jLabel22.setText("RG:");

        jLabel23.setText("CPF:");

        jLabel24.setText("Data de Nascimento:");

        jLabel25.setText("E-mail:");

        jLabelMoraOuNao.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabelMoraOuNao.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelMoraOuNao.setText("Aluno mora com responsável? NÃO");
        jLabelMoraOuNao.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel14.setText("CEP:");

        try {
            txtCepResponsavel.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("#####-###")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtCepResponsavel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCepResponsavelActionPerformed(evt);
            }
        });

        jLabel15.setText("Logradouro:");

        jLabel16.setText("Nº:");

        jLabel17.setText("Complemento:");

        jLabel18.setText("Bairro:");

        jLabel19.setText("Cidade:");

        jLabel20.setText("Estado:");

        jLabel27.setText("Telefone 02:");

        jLabel26.setText("Telefone 01:");

        jLabel28.setText("Telefone 03:");

        jLabel29.setText("Imagem:");

        btnSelecionaFile.setText("Selecionar Arquivo");
        btnSelecionaFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelecionaFileActionPerformed(evt);
            }
        });

        lblFotoDocumentos.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblFotoDocumentos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/imagens/user.jpg"))); // NOI18N
        lblFotoDocumentos.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jCBAlunoResponsavel.setText("Aluno é o próprio responsável?");
        jCBAlunoResponsavel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCBAlunoResponsavelActionPerformed(evt);
            }
        });

        jCBAlunoMoraRespon.setText("Aluno mora com o responsável?");
        jCBAlunoMoraRespon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCBAlunoMoraResponActionPerformed(evt);
            }
        });

        try {
            txtCPFRespon.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtCPFRespon.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtCPFResponFocusLost(evt);
            }
        });

        try {
            txtTelefone01Respon.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##) #########")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        try {
            txtTelefone02Respon.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##) #########")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        try {
            txtTelefone03Respon.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(##) #########")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        try {
            txtRGRespon.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##########")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jCBAlunoResponsavel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGap(72, 72, 72)
                                .addComponent(imgDocumentos, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jCBAlunoMoraRespon, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSelecionaFile)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblFotoDocumentos)
                        .addGap(21, 21, 21))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel21)
                            .addComponent(jLabel23)
                            .addComponent(jLabel24)
                            .addComponent(jLabel25)
                            .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel28, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel27, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel26, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jLabel29))
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtNomeRespon, javax.swing.GroupLayout.DEFAULT_SIZE, 274, Short.MAX_VALUE)
                            .addComponent(txtDataNascRespon, javax.swing.GroupLayout.DEFAULT_SIZE, 230, Short.MAX_VALUE)
                            .addComponent(txtEmailRespon, javax.swing.GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE)
                            .addComponent(txtCPFRespon, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtTelefone01Respon)
                            .addComponent(txtTelefone02Respon)
                            .addComponent(txtTelefone03Respon)
                            .addComponent(txtRGRespon))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelMoraOuNao, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel6Layout.createSequentialGroup()
                                    .addComponent(jLabel14)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtCepResponsavel, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel6Layout.createSequentialGroup()
                                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel15)
                                        .addComponent(jLabel16)
                                        .addComponent(jLabel18)
                                        .addComponent(jLabel19)
                                        .addComponent(jLabel20))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(txtCidadeResponsavel)
                                        .addComponent(txtBairroResponsavel)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                                            .addComponent(txtNumResponsavel)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jLabel17)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtComplementoResponsavel, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(txtLogradouroResponsavel)
                                        .addComponent(txtEstadoResponsavel, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addContainerGap())))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel21, jLabel23, jLabel24, jLabel25});

        jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel14, jLabel15, jLabel16, jLabel18, jLabel19, jLabel20});

        jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txtDataNascRespon, txtEmailRespon, txtNomeRespon});

        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNomeRespon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21)
                    .addComponent(jLabelMoraOuNao))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel22)
                            .addComponent(txtRGRespon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel23)
                            .addComponent(txtCPFRespon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(7, 7, 7)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel24)
                            .addComponent(txtDataNascRespon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel25)
                            .addComponent(txtEmailRespon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jLabel26))
                            .addGroup(jPanel6Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtTelefone01Respon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel27)
                            .addComponent(txtTelefone02Respon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel28)
                            .addComponent(txtTelefone03Respon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14)
                            .addComponent(txtCepResponsavel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15)
                            .addComponent(txtLogradouroResponsavel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel16)
                            .addComponent(jLabel17)
                            .addComponent(txtNumResponsavel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtComplementoResponsavel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(11, 11, 11)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel18)
                            .addComponent(txtBairroResponsavel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel19)
                            .addComponent(txtCidadeResponsavel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtEstadoResponsavel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel20))))
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(lblFotoDocumentos, javax.swing.GroupLayout.PREFERRED_SIZE, 212, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel29)
                            .addComponent(imgDocumentos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSelecionaFile))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jCBAlunoResponsavel)
                        .addGap(18, 18, 18)
                        .addComponent(jCBAlunoMoraRespon)
                        .addGap(23, 23, 23))))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel21, jLabel22, jLabel23, jLabel24, jLabel25, jLabel26, jLabel27, jLabel28, jLabel29});

        jPanel6Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel14, jLabel15, jLabel16, jLabel18, jLabel19, jLabel20});

        jTabbedPane1.addTab("Dados do Responsável", jPanel6);

        jLabel30.setText("Formas de Pagamento:");

        jCBFormasPagamento.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Escolha uma das opções", "Cartão", "Boleto", "Carnê" }));
        jCBFormasPagamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCBFormasPagamentoActionPerformed(evt);
            }
        });

        jLabel33.setText("Turma:");

        jCBTurma.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Escolha uma das opções" }));
        jCBTurma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCBTurmaActionPerformed(evt);
            }
        });

        jLabel31.setText("Livro:");

        jCBLivro.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Escolha uma das opções" }));
        jCBLivro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCBLivroActionPerformed(evt);
            }
        });

        btnAtualizarPagina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/update.png"))); // NOI18N
        btnAtualizarPagina.setToolTipText("Atualizar Janela");
        btnAtualizarPagina.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAtualizarPagina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtualizarPaginaActionPerformed(evt);
            }
        });

        btnAdicionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/create.png"))); // NOI18N
        btnAdicionar.setToolTipText("Cadastrar/Editar");
        btnAdicionar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdicionarActionPerformed(evt);
            }
        });

        btnEmitirCarnes.setText("Emitir Carnês");
        btnEmitirCarnes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEmitirCarnesActionPerformed(evt);
            }
        });

        jLabel32.setText("Data vencimento:");

        jLabel34.setText("Nº de parcelas:");

        jCBNumParcelas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "1", "2", "3", "4", "5", "6" }));

        lblValorParcela.setText("Valor parcela (R$):");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(btnEmitirCarnes)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 421, Short.MAX_VALUE)
                        .addComponent(btnAtualizarPagina, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(41, 41, 41)
                        .addComponent(btnAdicionar)
                        .addContainerGap())
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel33)
                            .addComponent(jLabel31)
                            .addComponent(jLabel30))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jCBLivro, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jCBTurma, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jCBFormasPagamento, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel32)
                                    .addComponent(jLabel34))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtDataVencParcela, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jCBNumParcelas, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(lblValorParcela)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtValorParcela)))
                        .addGap(38, 38, 38))))
        );

        jPanel7Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jCBFormasPagamento, jCBLivro, jCBTurma});

        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel31)
                        .addComponent(jCBLivro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel32))
                    .addComponent(txtDataVencParcela, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel34)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel33)
                        .addComponent(jCBTurma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jCBNumParcelas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel30)
                    .addComponent(jCBFormasPagamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblValorParcela)
                    .addComponent(txtValorParcela, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 293, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btnAdicionar)
                        .addComponent(btnAtualizarPagina))
                    .addComponent(btnEmitirCarnes, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Dados do Curso", jPanel7);

        getContentPane().add(jTabbedPane1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtCepAlunoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCepAlunoActionPerformed
        if (!"".equals(txtCepAluno.getText())) {
            buscaCepAluno(txtCepAluno.getText());
            if (!"".equals(txtCepAluno.getText()) && !"".equals(txtCepResponsavel.getText())) {
                jLabelMoraOuNao.setText("<html><strong>Aluno mora com responsável?</strong> <font size='5' color='red'>Não</font></html>");
            }
            if (txtCepResponsavel.getText().equals(txtCepAluno.getText())) {
                if ("".equals(txtNumAluno.getText()) && "".equals(txtNumResponsavel.getText())) {
                    JOptionPane.showMessageDialog(rootPane, "Informe o número dos endereços.", "Erro", 0);
                } else if (!"".equals(txtNumAluno.getText()) && "".equals(txtNumResponsavel.getText())) {
                    txtNumResponsavel.setText(txtNumAluno.getText());
                } else {
                    txtNumAluno.setText(txtNumResponsavel.getText());
                }
                jLabelMoraOuNao.setText("<html><strong>Aluno mora com responsável?</strong> <font size='5' color='green'>Sim</font></html>");
            }
        } else {
            txtCepAluno.setText("");
            txtLogradouroAluno.setText("");
            txtNumAluno.setText("");
            txtComplementoAluno.setText("");
            txtBairroAluno.setText("");
            txtCidadeAluno.setText("");
            txtEstadoAluno.setText("");
            txtCepAluno.requestFocus();
            JOptionPane.showMessageDialog(rootPane, "Preencha o campo para que seja efetuada a busca por CEP.", "Erro", 0);
        }
    }//GEN-LAST:event_txtCepAlunoActionPerformed

    private void txtCepResponsavelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCepResponsavelActionPerformed
        if (!"".equals(txtCepResponsavel.getText())) {
            buscaCepResponsavel(txtCepResponsavel.getText());
            if (!"".equals(txtCepAluno.getText()) && !"".equals(txtCepResponsavel.getText())) {
                jLabelMoraOuNao.setText("<html><strong>Aluno mora com responsável?</strong> <font size='5' color='red'>Não</font></html>");
            }
            if (txtCepResponsavel.getText().equals(txtCepAluno.getText())) {
                if ("".equals(txtNumAluno.getText()) && "".equals(txtNumResponsavel.getText())) {
                    JOptionPane.showMessageDialog(rootPane, "Informe o número dos endereços.", "Erro", 0);
                } else if (!"".equals(txtNumAluno.getText()) && "".equals(txtNumResponsavel.getText())) {
                    txtNumResponsavel.setText(txtNumAluno.getText());
                } else {
                    txtNumAluno.setText(txtNumResponsavel.getText());
                }
                jLabelMoraOuNao.setText("<html><strong>Aluno mora com responsável?</strong> <font size='5' color='green'>Sim</font></html>");
            }
        } else {
            txtCepAluno.setText("");
            txtLogradouroAluno.setText("");
            txtNumAluno.setText("");
            txtComplementoAluno.setText("");
            txtBairroAluno.setText("");
            txtCidadeAluno.setText("");
            txtEstadoAluno.setText("");
            txtCepAluno.requestFocus();
            JOptionPane.showMessageDialog(rootPane, "Preencha o campo para que seja efetuada a busca por CEP.", "Erro", 0);
        }
    }//GEN-LAST:event_txtCepResponsavelActionPerformed

    private void btnAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdicionarActionPerformed
        Estado estado = new Estado();
        EstadosDAO estadosDAO = new EstadosDAO();
        Cidade cidade = new Cidade();
        CidadesDAO cidadesDAO = new CidadesDAO();
        Endereco endereco = new Endereco();
        EnderecosDAO enderecosDAO = new EnderecosDAO();
        Responsavel responsavel = new Responsavel();
        Matricula matricula = new Matricula();
        MatriculasDAO matriculasDAO = new MatriculasDAO();
        ResponsaveisDAO responsaveisDAO = new ResponsaveisDAO();
        Aluno aluno = new Aluno();
        GregorianCalendar gc = new GregorianCalendar();
        ParcelasDAO parcelasDAO = new ParcelasDAO();
        Parcela parcela = new Parcela();
        AlunosDAO alunosDAO = new AlunosDAO();

        if (permitirEdicao == false) {
            if ("".equals(txtNome.getText())) {
                JOptionPane.showMessageDialog(rootPane, "Preencha o(s) campo(s).", "Erro", 0);
                txtNome.requestFocus();
            } else {
                estado.setNome(txtEstadoAluno.getText());
                int idEstadoAluno = estadosDAO.insert(estado);
                estado.setNome(txtEstadoResponsavel.getText());
                int idEstadoRespon = estadosDAO.insert(estado);

                cidade.setNome(txtCidadeAluno.getText());
                cidade.setIdEstado(idEstadoAluno);
                int idCidadeAluno = cidadesDAO.insert(cidade);
                cidade.setNome(txtCidadeResponsavel.getText());
                cidade.setIdEstado(idEstadoRespon);
                int idCidadeRespon = cidadesDAO.insert(cidade);

                endereco.setLogradouro(txtLogradouroAluno.getText());
                endereco.setNumero(Integer.valueOf(txtNumAluno.getText()));
                endereco.setComplemento(txtComplementoAluno.getText());
                endereco.setCep(txtCepAluno.getText());
                endereco.setBairro(txtBairroAluno.getText());
                endereco.setIdCidade(idCidadeAluno);
                int idEnderecoAluno = enderecosDAO.insert(endereco);
                endereco.setLogradouro(txtLogradouroResponsavel.getText());
                endereco.setNumero(Integer.valueOf(txtNumResponsavel.getText()));
                endereco.setComplemento(txtComplementoResponsavel.getText());
                endereco.setCep(txtCepResponsavel.getText());
                endereco.setBairro(txtBairroResponsavel.getText());
                endereco.setIdCidade(idCidadeRespon);
                int idEnderecoRespon = enderecosDAO.insert(endereco);

                responsavel.setNome(txtNomeRespon.getText());
                responsavel.setDataNascimento(converterDataParaMysql(txtDataNascRespon.getDate()));
                responsavel.setRg(txtRGRespon.getText());
                responsavel.setCpf(txtCPFRespon.getText());
                responsavel.setEmail(txtEmailRespon.getText());
                responsavel.setImagem(imgDocumentos.getText());
                responsavel.setTelefoneA(txtTelefone01Respon.getText());
                responsavel.setTelefoneB(txtTelefone02Respon.getText());
                responsavel.setTelefoneC(txtTelefone03Respon.getText());
                responsavel.setIdEndereco(idEnderecoRespon);
                idResponsavel = responsaveisDAO.insert(responsavel);

                matricula.setTipoPagamento(jCBFormasPagamento.getSelectedItem().toString());
                matricula.setIdLivro(Integer.valueOf(lblRetornoIDLivro.getText()));
                idMatricula = matriculasDAO.insert(matricula);

                int contTotal = Integer.valueOf(jCBNumParcelas.getSelectedItem().toString());
                for (int i = 1; i <= contTotal; i++) {
                    parcela.setNumParcela(i);
                    parcela.setTotalParcela(contTotal);
                    parcela.setValorParcela(Double.valueOf(txtValorParcela.getText()));
                    gc.setTime(txtDataVencParcela.getDate());
                    gc.roll(GregorianCalendar.MONTH, i);
                    Date date = gc.getTime();
                    parcela.setDataVencimento(converterDataParaMysql(date));
                    parcela.setIdMatricula(idMatricula);
                    parcelasDAO.insert(parcela);
                }

                aluno.setNome(txtNome.getText());
                aluno.setDataNascimento(converterDataParaMysql(txtDataNasc.getDate()));
                aluno.setRg(txtRG.getText());
                aluno.setCpf(txtCPF.getText());
                aluno.setEmail(txtEmail.getText());
                aluno.setTelefone(txtTelefone.getText());
                aluno.setIdEndereco(idEnderecoAluno);
                aluno.setIdResponsavel(idResponsavel);
                aluno.setIdMatricula(idMatricula);
                aluno.setIdTurma(Integer.valueOf(lblRetornoIDTurma.getText()));
                aluno.setStatus(1);
                alunosDAO.insert(aluno);
                lblRetornoIDMatricula.setText(String.valueOf(aluno.getIdMatricula()));

                limparCampos();

                JOptionPane.showMessageDialog(rootPane, "Aluno matriculado com sucesso!", "Sucesso", 1);

                btnEmitirCarnes.setEnabled(true);
            }
        } else if (permitirEdicao == true) {
            int idEnderecoAluno = idEndereco;
            int idRespon = this.idResponsavel;
            int idMatric = this.idMatricula;
            int idAluno = idAlunoEdicao;
            int idTurm = Integer.valueOf(lblRetornoIDTurma.getText());
            int idEnderecoRespon = responsaveisDAO.returnIDEndereco(idRespon);
            int idCidadeAluno = enderecosDAO.returnIDCidade(idEnderecoAluno);
            int idCidadeRespon = enderecosDAO.returnIDCidade(idEndereco);
            int idEstadoAluno = cidadesDAO.returnIDEstado(idCidadeAluno);
            int idEstadoRespon = cidadesDAO.returnIDEstado(idCidadeRespon);

            estado.setNome(txtEstadoAluno.getText());
            estadosDAO.update(estado, idEstadoAluno);
            estado.setNome(txtEstadoResponsavel.getText());
            estadosDAO.update(estado, idEstadoRespon);

            cidade.setNome(txtCidadeAluno.getText());
            cidade.setIdEstado(idEstadoAluno);
            cidadesDAO.update(cidade, idCidadeAluno);
            cidade.setNome(txtCidadeResponsavel.getText());
            cidade.setIdEstado(idEstadoRespon);
            cidadesDAO.update(cidade, idCidadeRespon);

            endereco.setLogradouro(txtLogradouroAluno.getText());
            endereco.setNumero(Integer.valueOf(txtNumAluno.getText()));
            endereco.setComplemento(txtComplementoAluno.getText());
            endereco.setCep(txtCepAluno.getText());
            endereco.setBairro(txtBairroAluno.getText());
            endereco.setIdCidade(idCidadeAluno);
            enderecosDAO.update(endereco, idEnderecoAluno);
            endereco.setLogradouro(txtLogradouroResponsavel.getText());
            endereco.setNumero(Integer.valueOf(txtNumResponsavel.getText()));
            endereco.setComplemento(txtComplementoResponsavel.getText());
            endereco.setCep(txtCepResponsavel.getText());
            endereco.setBairro(txtBairroResponsavel.getText());
            endereco.setIdCidade(idCidadeRespon);
            enderecosDAO.update(endereco, idEnderecoRespon);

            responsavel.setNome(txtNomeRespon.getText());
            responsavel.setDataNascimento(converterDataParaMysql(txtDataNascRespon.getDate()));
            responsavel.setRg(txtRGRespon.getText());
            responsavel.setCpf(txtCPFRespon.getText());
            responsavel.setEmail(txtEmailRespon.getText());
            responsavel.setImagem(imgDocumentos.getText());
            responsavel.setTelefoneA(txtTelefone01Respon.getText());
            responsavel.setTelefoneB(txtTelefone02Respon.getText());
            responsavel.setTelefoneC(txtTelefone03Respon.getText());
            responsavel.setIdEndereco(idEnderecoRespon);
            responsaveisDAO.update(responsavel, idRespon);

            matricula.setTipoPagamento(jCBFormasPagamento.getSelectedItem().toString());
            matricula.setIdLivro(Integer.valueOf(lblRetornoIDLivro.getText()));
            matriculasDAO.update(matricula, idMatric);

//            int[] idsParcela = parcelasDAO.returnIDArray(idMatric);
//            JOptionPane.showMessageDialog(rootPane, idsParcela);
//            int contTotal = idsParcela.length;
//            for (int i = 1; i <= contTotal; i++) {
//                parcela.setNumParcela(i);
//                parcela.setTotalParcela(contTotal);
//                parcela.setValorParcela(Double.valueOf(txtValorParcela.getText()));
//                gc.setTime(txtDataVencParcela.getDate());
//                gc.roll(GregorianCalendar.MONTH, i);
//                Date date = gc.getTime();
//                parcela.setDataVencimento(converterDataParaMysql(date));
//                parcela.setIdMatricula(idMatric);
//                parcelasDAO.update(parcela, idsParcela[i]);
//            }
            aluno.setNome(txtNome.getText());
            aluno.setDataNascimento(converterDataParaMysql(txtDataNasc.getDate()));
            aluno.setRg(txtRG.getText());
            aluno.setCpf(txtCPF.getText());
            aluno.setEmail(txtEmail.getText());
            aluno.setTelefone(txtTelefone.getText());
            aluno.setIdEndereco(idEnderecoAluno);
            aluno.setIdResponsavel(idRespon);
            aluno.setIdMatricula(idMatric);
            aluno.setIdTurma(idTurm);
            aluno.setStatus(1);
            alunosDAO.update(aluno, idAluno);
            lblRetornoIDMatricula.setText(String.valueOf(aluno.getIdMatricula()));

            limparCampos();

            JOptionPane.showMessageDialog(rootPane, "Aluno editado com sucesso!", "Sucesso", 1);
        }
    }//GEN-LAST:event_btnAdicionarActionPerformed

    private void btnAtualizarPaginaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtualizarPaginaActionPerformed
        limparCampos();
    }//GEN-LAST:event_btnAtualizarPaginaActionPerformed

    private void jCBLivroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCBLivroActionPerformed
        if (jCBLivro.getSelectedIndex() != 0) {
            try {
                Connection conexao;
                PreparedStatement pst;
                ResultSet rs;

                String[] nomeCB = jCBLivro.getSelectedItem().toString().split("\\(|\\)");
                String nomeLivro = nomeCB[0];
                String nomeModulo = nomeCB[1];

                conexao = ModuloConexao.conector();
                String sql = "SELECT livro.id, livro.status FROM livro "
                        + "INNER JOIN curso ON livro.idCurso = curso.id "
                        + "WHERE livro.nome = '" + nomeLivro + "' AND curso.nome = '" + nomeModulo + "'";
                pst = conexao.prepareStatement(sql);
                rs = pst.executeQuery();

                while (rs.next()) {
                    if ("1".equals(rs.getString(2))) {
                        lblRetornoIDLivro.setText(rs.getString(1));
                    }
                }
                conexao.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(rootPane, "Falha ao buscar registro do livro.", "Erro", 0);
            }
        } else {
            lblRetornoIDLivro.setText("");
        }
    }//GEN-LAST:event_jCBLivroActionPerformed

    private void jCBTurmaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCBTurmaActionPerformed
        if (jCBTurma.getSelectedIndex() != 0) {
            try {
                Connection conexao;
                PreparedStatement pst;
                ResultSet rs;

                String[] nomeCB = jCBTurma.getSelectedItem().toString().trim().split("\\(|\\)|até");
                String diaTurma = nomeCB[0];
                String horaInicial = nomeCB[1];
                String horaFinal = nomeCB[2];

                conexao = ModuloConexao.conector();
                String sql = "SELECT horario.idTurma, horario.status "
                        + "FROM horario WHERE horario.diaSemana =  '" + diaTurma.trim() + "'"
                        + "AND horario.horaInicial = '" + horaInicial.trim() + "' AND horario.horaFinal = '" + horaFinal.trim() + "'";
                pst = conexao.prepareStatement(sql);
                rs = pst.executeQuery();

                while (rs.next()) {
                    if ("1".equals(rs.getString(2))) {
                        lblRetornoIDTurma.setText(rs.getString(1));
                    }
                }
                conexao.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(rootPane, "Falha ao buscar registro da turma.", "Erro", 0);
            }
        } else {
            lblRetornoIDTurma.setText("");
        }
    }//GEN-LAST:event_jCBTurmaActionPerformed

    private void formInternalFrameActivated(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameActivated
        if (permitirEdicao == true) {
            AlunosDAO alunosDAO = new AlunosDAO();
            ResponsaveisDAO responsaveisDAO = new ResponsaveisDAO();
            EnderecosDAO enderecosDAO = new EnderecosDAO();
            CidadesDAO cidadesDAO = new CidadesDAO();
            EstadosDAO estadosDAO = new EstadosDAO();

            for (Aluno a : alunosDAO.readUpdate(String.valueOf(idAlunoEdicao))) {
                if (a.getStatus() == 1) {
                    try {
                        txtNome.setText(a.getNome());
                        String dataFormatoMySQL = a.getDataNascimento();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        Date dataView = sdf.parse(dataFormatoMySQL);
                        sdf.applyPattern("dd/MM/yyyy");
                        String dataFormatada = sdf.format(dataView);
                        java.util.Date data = sdf.parse(dataFormatada);
                        txtDataNasc.setDate(data);
                        txtRG.setText(a.getRg());
                        txtCPF.setText(a.getCpf());
                        txtEmail.setText(a.getEmail());
                        txtTelefone.setText(a.getTelefone());
                        idEndereco = a.getIdEndereco();
                        idResponsavel = a.getIdResponsavel();
                        idMatricula = a.getIdMatricula();
                        idTurma = a.getIdTurma();
                    } catch (ParseException ex) {
                        ex.printStackTrace();
                    }
                }
                for (Endereco e : enderecosDAO.readUpdate(String.valueOf(idEndereco))) {
                    txtCepAluno.setText(e.getCep());
                    txtLogradouroAluno.setText(e.getLogradouro());
                    txtNumAluno.setText(String.valueOf(e.getNumero()));
                    txtComplementoAluno.setText(e.getComplemento());
                    txtBairroAluno.setText(e.getBairro());
                    int idCidade = e.getIdCidade();
                    for (Cidade c : cidadesDAO.readUpdate(String.valueOf(idCidade))) {
                        txtCidadeAluno.setText(c.getNome());
                        int idEstado = c.getIdEstado();
                        for (Estado estado : estadosDAO.readUpdate(String.valueOf(idEstado))) {
                            txtEstadoAluno.setText(estado.getNome());
                        }
                    }
                }
            }

            for (Responsavel r : responsaveisDAO.readUpdate(String.valueOf(idResponsavel))) {
                try {
                    txtNomeRespon.setText(r.getNome());
                    txtRGRespon.setText(r.getRg());
                    txtCPFRespon.setText(r.getCpf());
                    String dataFormatoMySQL = r.getDataNascimento();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date dataView = sdf.parse(dataFormatoMySQL);
                    sdf.applyPattern("dd/MM/yyyy");
                    String dataFormatada = sdf.format(dataView);
                    java.util.Date data = sdf.parse(dataFormatada);
                    txtDataNascRespon.setDate(data);
                    txtEmailRespon.setText(r.getEmail());
                    txtTelefone01Respon.setText(r.getTelefoneA());
                    txtTelefone02Respon.setText(r.getTelefoneB());
                    txtTelefone03Respon.setText(r.getTelefoneC());
                    imgDocumentos.setText(r.getImagem());
                    idEndereco = r.getIdEndereco();
                    for (Endereco e : enderecosDAO.readUpdate(String.valueOf(idEndereco))) {
                        txtCepResponsavel.setText(e.getCep());
                        txtLogradouroResponsavel.setText(e.getLogradouro());
                        txtNumResponsavel.setText(String.valueOf(e.getNumero()));
                        txtComplementoResponsavel.setText(e.getComplemento());
                        txtBairroResponsavel.setText(e.getBairro());
                        int idCidade = e.getIdCidade();
                        for (Cidade c : cidadesDAO.readUpdate(String.valueOf(idCidade))) {
                            txtCidadeResponsavel.setText(c.getNome());
                            int idEstado = c.getIdEstado();
                            for (Estado estado : estadosDAO.readUpdate(String.valueOf(idEstado))) {
                                txtEstadoResponsavel.setText(estado.getNome());
                            }
                        }
                    }
                } catch (ParseException ex) {
                    ex.printStackTrace();
                }
            }
            //exibindo informações do livro e forma de pagamento
            MatriculasDAO matriculasDAO = new MatriculasDAO();
            LivrosDAO livrosDAO = new LivrosDAO();
            CursosDAO cursosDAO = new CursosDAO();
            HorariosDAO horariosDAO = new HorariosDAO();
            jCBFormasPagamento.setSelectedItem(matriculasDAO.returnInfo(idMatricula));
            int idLivro = matriculasDAO.returnIDLivro(idMatricula);
            if (idLivro != 0) {
                String nomeLivro = livrosDAO.returnInfo(idLivro);
                int idCurso = livrosDAO.returnIDCurso(idLivro);
                String nomeCurso = cursosDAO.returnInfo(idCurso);
                jCBLivro.setSelectedItem(nomeLivro + " (" + nomeCurso + ")");
            }
            for (Horario h : horariosDAO.read(idTurma)) {
                if (h.getStatus() == 1) {
                    String diaSemana = h.getDiaSemana();
                    String horaInicial = h.getHoraInicial();
                    String horaFinal = h.getHoraFinal();
                    jCBTurma.setSelectedItem(diaSemana + " (" + horaInicial + " até " + horaFinal + ")");
                }
            }
            permitirEdicao = true;
            lblValorParcela.setEnabled(false);
            txtValorParcela.setEnabled(false);
            txtNome.requestFocus();
        }
    }//GEN-LAST:event_formInternalFrameActivated

    private void btnLigarWebcamActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLigarWebcamActionPerformed
        threadWebcam = new Thread(webcamCapture = new WebcamCapture(lblWebcam));
        threadWebcam.start();
        btnTirarFoto.setVisible(true);
        btnLigarWebcam.setEnabled(false);
    }//GEN-LAST:event_btnLigarWebcamActionPerformed

    private void btnTirarFotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTirarFotoActionPerformed
        URL url = this.getClass().getResource("/br/com/theplace/imagens/");
        String urlDiretorioLocal = url.getPath() + "teste.jpg";
        Image resultado = webcamCapture.tirarFotoWebcam(urlDiretorioLocal);
        if (resultado != null) {
            lblFotoAluno.setIcon(null);
            lblFotoAluno.setIcon(new ImageIcon(resultado));
            JOptionPane.showMessageDialog(null, "Foto tirada com sucesso!", "Sucesso", 1);
        }
        btnTirarFoto.setVisible(false);
        btnLigarWebcam.setEnabled(true);
        threadWebcam.stop();
    }//GEN-LAST:event_btnTirarFotoActionPerformed

    private void jCBAlunoResponsavelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCBAlunoResponsavelActionPerformed
        if (jCBAlunoResponsavel.isSelected() == true) {
            txtNomeRespon.setText(txtNome.getText());
            txtRGRespon.setText(txtRG.getText());
            txtCPFRespon.setText(txtCPF.getText());
            txtDataNascRespon.setDate(txtDataNasc.getDate());
            txtEmailRespon.setText(txtEmail.getText());
            txtTelefone01Respon.setText(txtTelefone.getText());
        }
    }//GEN-LAST:event_jCBAlunoResponsavelActionPerformed

    private void btnEmitirCarnesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEmitirCarnesActionPerformed
        RelatoriosDAO relatoriosDAO = new RelatoriosDAO();
        JasperViewer.viewReport(relatoriosDAO.gerarRelatCarnes(Integer.valueOf(lblRetornoIDMatricula.getText())), false, Locale.getDefault());
    }//GEN-LAST:event_btnEmitirCarnesActionPerformed

    private void jCBFormasPagamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCBFormasPagamentoActionPerformed
        if (jCBFormasPagamento.getSelectedItem().toString() == "Carnê") {
            btnEmitirCarnes.setVisible(true);
        }
    }//GEN-LAST:event_jCBFormasPagamentoActionPerformed

    private void jCBAlunoMoraResponActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCBAlunoMoraResponActionPerformed
        if (jCBAlunoMoraRespon.isSelected() == true) {
            if (txtCepAluno.getText() != "     -   ") {
                boolean result = buscaCepResponsavel(txtCepAluno.getText());
                txtCepResponsavel.setText(txtCepAluno.getText());
                txtNumResponsavel.setText(txtNumAluno.getText());
                txtComplementoResponsavel.setText(txtComplementoAluno.getText());
                if (result) {
                    jLabelMoraOuNao.setText("<html><strong>Aluno mora com responsável?</strong> <font size='5' color='green'>Sim</font></html>");
                }
            }
        }
    }//GEN-LAST:event_jCBAlunoMoraResponActionPerformed

    private void btnSelecionaFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelecionaFileActionPerformed
        JFileChooser jFileChooser = new JFileChooser();
        jFileChooser.setFileFilter(new FileNameExtensionFilter("Imagem", "jpg", "jpeg", "png", "gif"));
        jFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        jFileChooser.showDialog(jFileChooser, "Selecione");
        String caminho = jFileChooser.getSelectedFile().getAbsolutePath();
        ImageIcon icon = new ImageIcon(caminho);
        Image image = icon.getImage();
        Image newimg = image.getScaledInstance(211, 225, java.awt.Image.SCALE_SMOOTH);
        lblFotoDocumentos.setIcon(null);
        lblFotoDocumentos.setIcon(new ImageIcon(newimg));
    }//GEN-LAST:event_btnSelecionaFileActionPerformed

    private void txtCPFFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtCPFFocusLost
        ValidarCpf validarCpf = new ValidarCpf();
        String cpfRecebido = txtCPF.getText();
        cpfRecebido = cpfRecebido.replace(".", "");
        cpfRecebido = cpfRecebido.replace("-", "");
        cpfRecebido = cpfRecebido.replace(" ", "");
        if (cpfRecebido.length() == 11) {
            boolean verificado = validarCpf.isValidCPF(cpfRecebido);
            if (verificado == false) {
                JOptionPane.showMessageDialog(null, "CPF informado é inválido.", "Erro", 0);
                txtCPF.requestFocus();
            }
        }
    }//GEN-LAST:event_txtCPFFocusLost

    private void txtCPFResponFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtCPFResponFocusLost
        ValidarCpf validarCpf = new ValidarCpf();
        String cpfRecebido = txtCPFRespon.getText();
        cpfRecebido = cpfRecebido.replace(".", "");
        cpfRecebido = cpfRecebido.replace("-", "");
        cpfRecebido = cpfRecebido.replace(" ", "");
        if (cpfRecebido.length() == 11) {
            boolean verificado = validarCpf.isValidCPF(cpfRecebido);
            if (verificado == false) {
                JOptionPane.showMessageDialog(null, "CPF informado é inválido.", "Erro", 0);
                txtCPFRespon.requestFocus();
            }
        }
    }//GEN-LAST:event_txtCPFResponFocusLost


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdicionar;
    private javax.swing.JButton btnAtualizarPagina;
    private javax.swing.JButton btnEmitirCarnes;
    private javax.swing.JButton btnLigarWebcam;
    private javax.swing.JButton btnSelecionaFile;
    private javax.swing.JButton btnTirarFoto;
    private javax.swing.JFormattedTextField imgDocumentos;
    private javax.swing.JCheckBox jCBAlunoMoraRespon;
    private javax.swing.JCheckBox jCBAlunoResponsavel;
    private javax.swing.JComboBox<String> jCBFormasPagamento;
    private javax.swing.JComboBox<String> jCBLivro;
    private javax.swing.JComboBox<String> jCBNumParcelas;
    private javax.swing.JComboBox<String> jCBTurma;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelMoraOuNao;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lblFotoAluno;
    private javax.swing.JLabel lblFotoDocumentos;
    private javax.swing.JLabel lblValorParcela;
    private javax.swing.JLabel lblWebcam;
    private javax.swing.JTextField txtBairroAluno;
    private javax.swing.JTextField txtBairroResponsavel;
    private javax.swing.JFormattedTextField txtCPF;
    private javax.swing.JFormattedTextField txtCPFRespon;
    private javax.swing.JFormattedTextField txtCepAluno;
    private javax.swing.JFormattedTextField txtCepResponsavel;
    private javax.swing.JTextField txtCidadeAluno;
    private javax.swing.JTextField txtCidadeResponsavel;
    private javax.swing.JTextField txtComplementoAluno;
    private javax.swing.JTextField txtComplementoResponsavel;
    private com.toedter.calendar.JDateChooser txtDataNasc;
    private com.toedter.calendar.JDateChooser txtDataNascRespon;
    private com.toedter.calendar.JDateChooser txtDataVencParcela;
    private javax.swing.JTextField txtEmail;
    private javax.swing.JTextField txtEmailRespon;
    private javax.swing.JTextField txtEstadoAluno;
    private javax.swing.JTextField txtEstadoResponsavel;
    private javax.swing.JTextField txtLogradouroAluno;
    private javax.swing.JTextField txtLogradouroResponsavel;
    private javax.swing.JTextField txtNome;
    private javax.swing.JTextField txtNomeRespon;
    private javax.swing.JTextField txtNumAluno;
    private javax.swing.JTextField txtNumResponsavel;
    private javax.swing.JFormattedTextField txtRG;
    private javax.swing.JFormattedTextField txtRGRespon;
    private javax.swing.JFormattedTextField txtTelefone;
    private javax.swing.JFormattedTextField txtTelefone01Respon;
    private javax.swing.JFormattedTextField txtTelefone02Respon;
    private javax.swing.JFormattedTextField txtTelefone03Respon;
    private javax.swing.JTextField txtValorParcela;
    // End of variables declaration//GEN-END:variables
}

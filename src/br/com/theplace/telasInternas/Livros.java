package br.com.theplace.telasInternas;

import br.com.theplace.dal.ModuloConexao;
import br.com.theplace.dao.CursosDAO;
import br.com.theplace.dao.LivrosDAO;
import br.com.theplace.model.Curso;
import br.com.theplace.model.Livro;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class Livros extends javax.swing.JInternalFrame {

    boolean permitirEdicao = false;

    public Livros() {
        initComponents();
        getRootPane().setDefaultButton(btnAdicionar);
        //jComboBox é atualizado ao ser inicializado o jInternalFrame
        exibiCursosCB();
        //tabela é atualizada ao ser inicializado o jInternalFrame
        atualizaTableLivro();
    }

    /**
     * Método limpa os campos do jInternalFrame
     */
    private void limparCampos() {
        txtNomeLivro.setText("");
        jCBCurso.setSelectedIndex(0);
        txtPesquisa.setText("");
        permitirEdicao = false;
        txtNomeLivro.requestFocus();
    }

    /**
     * Método que exibi os cursos no jComboBox
     */
    private void exibiCursosCB() {
        CursosDAO cursoDAO = new CursosDAO();
        for (Curso c : cursoDAO.read()) {
            if (c.getStatus() == 1) {
                jCBCurso.addItem(c.getNome());
            }
        }
    }

    /**
     * Método que atualiza a tabela de acordo com os dados do banco
     */
    private void atualizaTableLivro() {
        //organizando tabela para inserir os dados na mesma
        DefaultTableModel model = new DefaultTableModel();
        this.jTableLivros.setModel(model);

        //adicionando colunas na tabela
        model.addColumn("Nome do Livro");
        model.addColumn("Curso");

        LivrosDAO livrosDAO = new LivrosDAO();
        CursosDAO cursoDAO = new CursosDAO();

        for (Livro l : livrosDAO.read()) {
            if (l.getStatus() == 1) {
                model.addRow(new Object[]{
                    l.getNome(),
                    cursoDAO.returnInfo(l.getIdCurso())
                });
            }
        }

        //alinhando no centro os dados que foram inseridos
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        jTableLivros.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        jTableLivros.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
    }

    /**
     * Método efetua pesquisa de acordo com o parâmetro recebido
     */
    private void realizarPesquisa() {
        //organizando tabela para inserir os dados na mesma
        DefaultTableModel model = new DefaultTableModel();
        this.jTableLivros.setModel(model);

        //adicionando colunas na tabela
        model.addColumn("Nome do Livro");
        model.addColumn("Módulo");
        
        LivrosDAO livrosDAO = new LivrosDAO();
        CursosDAO cursoDAO = new CursosDAO();

        for (Livro l : livrosDAO.search(txtPesquisa.getText())) {
            if (l.getStatus() == 1) {
                model.addRow(new Object[]{
                    l.getNome(),
                    cursoDAO.returnInfo(l.getIdCurso())
                });
            }
        }

        //alinhando no centro os dados que foram inseridos
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        jTableLivros.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        jTableLivros.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
    }

    /**
     * Método verifica se já existi no banco, o dado informado por parâmetro
     *
     * @param dadoRecebido - Texto informado pelo usuário
     * @return - Retorna um boolean
     */
    private boolean verificaSeExisti(String dadoRecebido) {
        boolean result = false;
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select id from livro where nome = '" + dadoRecebido + "'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            if (rs.next()) {
                result = true;
            }
            //fechando a conexão
            conexao.close();

        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(rootPane, "Falha ao verificar se existi.", "Erro", 0);
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAdicionar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtNomeLivro = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableLivros = new javax.swing.JTable();
        btnEditar = new javax.swing.JButton();
        btnPesquisar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        txtPesquisa = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        btnAtualizarPagina = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jCBCurso = new javax.swing.JComboBox<>();

        setClosable(true);
        setIconifiable(true);
        setResizable(true);
        setTitle("Gerenciar Livros");
        setMinimumSize(new java.awt.Dimension(725, 470));
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameActivated(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        btnAdicionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/create.png"))); // NOI18N
        btnAdicionar.setToolTipText("Cadastrar/Editar");
        btnAdicionar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdicionarActionPerformed(evt);
            }
        });

        jLabel1.setText("Nome do livro:");

        jTableLivros.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableLivros.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jScrollPane1.setViewportView(jTableLivros);

        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/edit.png"))); // NOI18N
        btnEditar.setToolTipText("Alterar");
        btnEditar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnPesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/search.png"))); // NOI18N
        btnPesquisar.setToolTipText("Pesquisar");
        btnPesquisar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });

        btnExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/delete.png"))); // NOI18N
        btnExcluir.setToolTipText("Excluir");
        btnExcluir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });

        jLabel3.setText("Pesquisar por livro:");

        btnAtualizarPagina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/update.png"))); // NOI18N
        btnAtualizarPagina.setToolTipText("Atualizar Janela");
        btnAtualizarPagina.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAtualizarPagina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtualizarPaginaActionPerformed(evt);
            }
        });

        jLabel2.setText("Curso:");

        jCBCurso.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Escolha uma das opções" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 559, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAtualizarPagina)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnExcluir)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnEditar)
                .addGap(20, 20, 20))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNomeLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jCBCurso, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(96, 96, 96)
                        .addComponent(btnAdicionar, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNomeLivro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jCBCurso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(btnAdicionar)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3))
                    .addComponent(btnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 297, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(btnEditar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnExcluir))
                    .addComponent(btnAtualizarPagina))
                .addGap(23, 23, 23))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdicionarActionPerformed
        if (permitirEdicao == false) {
            if ("".equals(txtNomeLivro.getText()) || "Escolha uma das opções".equals(jCBCurso.getSelectedItem().toString())) {
                JOptionPane.showMessageDialog(rootPane, "Preencha o(s) campo(s).", "Erro", 0);
                txtNomeLivro.requestFocus();
            } else {
                CursosDAO cursoDAO = new CursosDAO();
                int idCurso = cursoDAO.returnID(jCBCurso.getSelectedItem().toString());
                Livro livro = new Livro();
                livro.setNome(txtNomeLivro.getText());
                livro.setIdCurso(idCurso);
                livro.setStatus(1);
                LivrosDAO livrosDAO = new LivrosDAO();
                livrosDAO.insert(livro);
                atualizaTableLivro();
                limparCampos();
            }
        } else if (permitirEdicao == true) {
            if ("".equals(txtNomeLivro.getText())) {
                JOptionPane.showMessageDialog(rootPane, "Preencha o(s) campo(s).", "Erro", 0);
                txtNomeLivro.requestFocus();
            } else {
                boolean existiJa = verificaSeExisti(txtNomeLivro.getText());
                if (existiJa == true) {
                    JOptionPane.showMessageDialog(rootPane, "Livro já cadastrado no sistema", "Erro", 0);
                    txtNomeLivro.requestFocus();
                } else {
                    String itemSelecionado = jTableLivros.getValueAt(jTableLivros.getSelectedRow(), 0).toString();
                    CursosDAO cursoDAO = new CursosDAO();
                    int idCurso = cursoDAO.returnID(jCBCurso.getSelectedItem().toString());
                    Livro livro = new Livro();
                    livro.setNome(txtNomeLivro.getText());
                    livro.setIdCurso(idCurso);
                    LivrosDAO livrosDAO = new LivrosDAO();
                    livrosDAO.update(livro, itemSelecionado);
                    atualizaTableLivro();
                    limparCampos();
                }
            }
        }
    }//GEN-LAST:event_btnAdicionarActionPerformed

    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed
        if ("".equals(txtPesquisa.getText())) {
            JOptionPane.showMessageDialog(rootPane, "Preencha o(s) campo(s).", "Erro", 0);
            txtPesquisa.requestFocus();
        } else {
            realizarPesquisa();
        }
    }//GEN-LAST:event_btnPesquisarActionPerformed

    private void btnAtualizarPaginaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtualizarPaginaActionPerformed
        atualizaTableLivro();
        limparCampos();
    }//GEN-LAST:event_btnAtualizarPaginaActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        //verifica se foi selecionado ou não uma linha da tabela
        if (jTableLivros.getSelectedRowCount() == 0) {
            JOptionPane.showMessageDialog(rootPane, "Selecione uma linha da tabela para efetuar a exclusão.", "Erro", 0);
        } else {
            for (int i = 0; i < jTableLivros.getSelectedRowCount(); i++) {
                String itemSelecionado = jTableLivros.getValueAt(jTableLivros.getSelectedRows()[i], 0).toString();
                LivrosDAO livrosDAO = new LivrosDAO();
                livrosDAO.delete(itemSelecionado);
            }
            JOptionPane.showMessageDialog(null, "Livro(s) excluido(s) com sucesso!", "Sucesso", 1);
            atualizaTableLivro();
            limparCampos();
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        /*verifica se foi selecionado ou não uma linha da tabela
        para que em seguida seja efetuada a edição do mesmo*/
        switch (jTableLivros.getSelectedRowCount()) {
            case 0:
                JOptionPane.showMessageDialog(rootPane, "Selecione uma linha da tabela para efetuar a edição.", "Erro", 0);
                break;
            case 1:
                String itemSelecionado = jTableLivros.getValueAt(jTableLivros.getSelectedRow(), 0).toString();
                LivrosDAO livrosDAO = new LivrosDAO();
                CursosDAO cursoDAO = new CursosDAO();

                for (Livro l : livrosDAO.readUpdate(itemSelecionado)) {
                    if (l.getStatus() == 1) {
                        txtNomeLivro.setText(l.getNome());
                        jCBCurso.setSelectedItem(cursoDAO.returnInfo(l.getIdCurso()));
                    }
                }
                permitirEdicao = true;
                break;
            default:
                JOptionPane.showMessageDialog(rootPane, "Selecione somente uma linha para efetuar a edição.", "Erro", 0);
                break;

        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void formInternalFrameActivated(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameActivated
        jTableLivros.setDefaultEditor(Object.class, null);
        jTableLivros.setRowSelectionAllowed(true);
    }//GEN-LAST:event_formInternalFrameActivated


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdicionar;
    private javax.swing.JButton btnAtualizarPagina;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnPesquisar;
    private javax.swing.JComboBox<String> jCBCurso;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableLivros;
    private javax.swing.JTextField txtNomeLivro;
    private javax.swing.JTextField txtPesquisa;
    // End of variables declaration//GEN-END:variables
}

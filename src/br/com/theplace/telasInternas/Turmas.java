package br.com.theplace.telasInternas;

import br.com.theplace.dal.ModuloConexao;
import br.com.theplace.dao.HorariosDAO;
import br.com.theplace.dao.TurmasDAO;
import br.com.theplace.model.Horario;
import br.com.theplace.model.Turma;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class Turmas extends javax.swing.JInternalFrame {

    boolean permitirEdicao = false;

    public Turmas() {
        initComponents();
        getRootPane().setDefaultButton(btnAdicionar);
        //tabela é atualizada ao ser inicializado o jInternalFrame
        atualizaTableTurmas();
    }

    /**
     * Método limpa os campos do jInternalFrame
     */
    private void limparCampos() {
        txtHoraInicial.setText("");
        txtHoraFinal.setText("");
        jDateDataInicial.setDate(null);
        jCBDiaSemana.setSelectedIndex(0);
        jDateDataFinal.setDate(null);
        txtPesquisa.setText("");
        jCBDiaSemana.requestFocus();
        permitirEdicao = false;
    }

    /**
     * Método que atualiza a tabela de acordo com os dados do banco
     */
    private void atualizaTableTurmas() {
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "SELECT diaSemana, horaInicial, horaFinal, turma.dataInicial, "
                    + "turma.dataFinal, horario.status FROM horario "
                    + "INNER JOIN turma ON horario.idTurma = turma.id";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            //organizando tabela para inserir os dados na mesma
            DefaultTableModel model = new DefaultTableModel();
            this.jTableTurmas.setModel(model);

            //adicionando colunas na tabela
            model.addColumn("Dia da Semana");
            model.addColumn("Hora Inicial");
            model.addColumn("Hora Final");
            model.addColumn("Data Inicial");
            model.addColumn("Data Final");

            //inserindo os dados na tabela em suas respectivas colunas [numeroDeColunasAki]
            while (rs.next()) {
                if ("1".equals(rs.getString(6))) {
                    Object[] fila = new Object[5];
                    for (int x = 0; x < 5; x++) {
                        if (x >= 3) {
                            fila[x] = converterDataParaUsuario(rs.getDate(x + 1));
                        } else {
                            fila[x] = rs.getObject(x + 1);
                        }
                    }
                    model.addRow(fila);
                }
            }

            //alinhando no centro os dados que foram inseridos
            DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
            centerRenderer.setHorizontalAlignment(JLabel.CENTER);
            jTableTurmas.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
            jTableTurmas.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
            jTableTurmas.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
            jTableTurmas.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
            jTableTurmas.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);

            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(rootPane, "Falha ao carregar a tabela de turmas.", "Erro", 0);
        }
    }

    /**
     * Método efetua pesquisa de acordo com o parâmetro recebido
     *
     * @param dadoRecebido - Texto informado pelo usuário
     */
    private void realizarPesquisa(String dadoRecebido) {
        try {

            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            int numItensEncontrados = 0;
            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "SELECT diaSemana, horaInicial, horaFinal, turma.dataInicial, "
                    + "turma.dataFinal, horario.status FROM horario "
                    + "INNER JOIN turma ON horario.idTurma = turma.id "
                    + "WHERE (diaSemana LIKE '%" + dadoRecebido + "%' "
                    + "OR horaInicial LIKE '%" + dadoRecebido + "%' "
                    + "OR horaFinal LIKE '%" + dadoRecebido + "%')";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            //organizando tabela para inserir os dados na mesma
            DefaultTableModel model = new DefaultTableModel();
            this.jTableTurmas.setModel(model);

            //adicionando colunas na tabela
            model.addColumn("Dia da Semana");
            model.addColumn("Hora Inicial");
            model.addColumn("Hora Final");
            model.addColumn("Data Inicial");
            model.addColumn("Data Final");

            //inserindo os dados na tabela em suas respectivas colunas [numeroDeColunasAki]
            while (rs.next()) {
                if ("1".equals(rs.getString(6))) {
                    Object[] fila = new Object[5];
                    for (int x = 0; x < 5; x++) {
                        if (x >= 3) {
                            fila[x] = converterDataParaUsuario((Date) rs.getObject(x + 1));
                        } else {
                            fila[x] = rs.getObject(x + 1);
                        }
                    }
                    model.addRow(fila);
                    numItensEncontrados++;
                }
            }

            //alinhando no centro os dados que foram inseridos
            DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
            centerRenderer.setHorizontalAlignment(JLabel.CENTER);
            jTableTurmas.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
            jTableTurmas.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
            jTableTurmas.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
            jTableTurmas.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
            jTableTurmas.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);

            //mensagem se encontrou algo na pesquisa ou não
            if (numItensEncontrados == 0) {
                JOptionPane.showMessageDialog(rootPane, "Nenhum item foi encontrado na pesquisa por " + dadoRecebido, "Aviso", 1);
                txtPesquisa.setText("");
            } else {
                JOptionPane.showMessageDialog(rootPane, "Encontrado " + numItensEncontrados + " item(ns) na pesquisa por " + dadoRecebido, "Sucesso", 1);
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try[
            ex.printStackTrace();
            JOptionPane.showMessageDialog(rootPane, "Falha ao realizar a pesquisa.", "Erro", 0);
        }
    }

    /**
     * Método faz a converção da data para que se adapte a do MySQL
     *
     * @param dataRecebida - dado informado pelo usuário
     * @return String de data convertida
     */
    private String converterDataParaMysql(Date dataRecebida) {
        //recebo 03/09/1996 e devo gravar 1996-09-03(yyyy/mm/dd)
        SimpleDateFormat dataFormato = new SimpleDateFormat("yyyy-MM-dd");
        String resultado = dataFormato.format(dataRecebida);
        return resultado;
    }

    /**
     * Método faz a converção da data do mysql para o Usuário
     *
     * @param dataRecebida - dado informado
     * @return String de data convertida
     */
    private String converterDataParaUsuario(Date dataRecebida) {
        //recebo 1996-09-03 e devo exibir 03/09/1996(dd/mm/yyyy)
        SimpleDateFormat dataFormato = new SimpleDateFormat("dd/MM/yyyy");
        String resultado = dataFormato.format(dataRecebida);
        return resultado;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDayChooser1 = new com.toedter.calendar.JDayChooser();
        btnAdicionar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableTurmas = new javax.swing.JTable();
        btnEditar = new javax.swing.JButton();
        btnPesquisar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        txtPesquisa = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        btnAtualizarPagina = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtHoraInicial = new javax.swing.JFormattedTextField();
        txtHoraFinal = new javax.swing.JFormattedTextField();
        jLabel6 = new javax.swing.JLabel();
        jCBDiaSemana = new javax.swing.JComboBox<>();
        jDateDataFinal = new com.toedter.calendar.JDateChooser();
        jDateDataInicial = new com.toedter.calendar.JDateChooser();

        setClosable(true);
        setIconifiable(true);
        setResizable(true);
        setTitle("Gerenciar Turmas");
        setMinimumSize(new java.awt.Dimension(725, 470));
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameActivated(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        btnAdicionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/create.png"))); // NOI18N
        btnAdicionar.setToolTipText("Cadastrar/Editar");
        btnAdicionar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdicionarActionPerformed(evt);
            }
        });

        jLabel1.setText("Dia da semana:");

        jTableTurmas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableTurmas.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jScrollPane1.setViewportView(jTableTurmas);

        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/edit.png"))); // NOI18N
        btnEditar.setToolTipText("Alterar");
        btnEditar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnPesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/search.png"))); // NOI18N
        btnPesquisar.setToolTipText("Pesquisar");
        btnPesquisar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });

        btnExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/delete.png"))); // NOI18N
        btnExcluir.setToolTipText("Excluir");
        btnExcluir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });

        jLabel3.setText("Pesquisar por Turma:");

        btnAtualizarPagina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/update.png"))); // NOI18N
        btnAtualizarPagina.setToolTipText("Atualizar Janela");
        btnAtualizarPagina.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAtualizarPagina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtualizarPaginaActionPerformed(evt);
            }
        });

        jLabel2.setText("Data Final:");

        jLabel4.setText("Hora Inicial:");

        jLabel5.setText("Hora Final:");

        try {
            txtHoraInicial.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtHoraInicial.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        try {
            txtHoraFinal.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##:##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtHoraFinal.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel6.setText("Data Inicial:");

        jCBDiaSemana.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Escolha uma das opções", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab", "Dom" }));
        jCBDiaSemana.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCBDiaSemanaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAtualizarPagina)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnExcluir)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnEditar)
                .addGap(28, 28, 28))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel3)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(txtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel4)
                                        .addComponent(jLabel6))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jDateDataInicial, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
                                        .addComponent(txtHoraInicial))
                                    .addGap(27, 27, 27)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel2)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jDateDataFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(jLabel5)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtHoraFinal, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCBDiaSemana, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                        .addComponent(btnAdicionar, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jCBDiaSemana, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(txtHoraInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jDateDataInicial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(txtHoraFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jDateDataFinal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(txtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnPesquisar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAdicionar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(btnEditar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnExcluir))
                    .addComponent(btnAtualizarPagina))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdicionarActionPerformed
        if (permitirEdicao == false) {
            if ("Escolha uma das opções".equals(jCBDiaSemana.getSelectedItem().toString()) || "  :  ".equals(txtHoraInicial.getText())
                    || "  :  ".equals(txtHoraFinal.getText()) || jDateDataInicial.getDate() == null
                    || jDateDataFinal.getDate() == null) {
                JOptionPane.showMessageDialog(rootPane, "Preencha o(s) campo(s).", "Erro", 0);
            } else {
                Turma turma = new Turma();
                turma.setDataInicial(converterDataParaMysql(jDateDataInicial.getDate()));
                turma.setDataFinal(converterDataParaMysql(jDateDataFinal.getDate()));
                turma.setStatus(1);
                TurmasDAO turmasDAO = new TurmasDAO();
                int idTurma = turmasDAO.insert(turma);

                Horario horario = new Horario();
                horario.setDiaSemana(jCBDiaSemana.getSelectedItem().toString());
                horario.setHoraInicial(txtHoraInicial.getText());
                horario.setHoraFinal(txtHoraFinal.getText());
                horario.setIdTurma(idTurma);
                horario.setStatus(1);
                HorariosDAO horariosDAO = new HorariosDAO();
                horariosDAO.insert(horario);

                limparCampos();
                atualizaTableTurmas();
                JOptionPane.showMessageDialog(rootPane, "Turma cadastrada com sucesso!", "Sucesso", 1);
            }
        } else if (permitirEdicao == true) {
            if ("Escolha uma das opções".equals(jCBDiaSemana.getSelectedItem().toString()) || "  :  ".equals(txtHoraInicial.getText())
                    || "  :  ".equals(txtHoraFinal.getText()) || jDateDataInicial.getDate() == null
                    || jDateDataFinal.getDate() == null) {
                JOptionPane.showMessageDialog(rootPane, "Preencha o(s) campo(s).", "Erro", 0);
            } else {
                Connection conexao;
                PreparedStatement pstSelect, pst, pstSelectModulo;
                ResultSet rsSelect, rsSelectModulo;
                String sql;

                //realizando a conexão estipulada na classe ModuloConexao
                conexao = ModuloConexao.conector();
                try {
                    String itemSelecionadoDiaSemana = jTableTurmas.getValueAt(jTableTurmas.getSelectedRow(), 0).toString();
                    String itemSelecionadoHoraInicial = jTableTurmas.getValueAt(jTableTurmas.getSelectedRow(), 1).toString();
                    String itemSelecionadoHoraFinal = jTableTurmas.getValueAt(jTableTurmas.getSelectedRow(), 2).toString();
                    String sqlSelect = "SELECT horario.id, turma.id FROM horario "
                            + "INNER JOIN turma ON horario.idTurma = turma.id "
                            + "WHERE horario.diaSemana='" + itemSelecionadoDiaSemana + "'"
                            + "AND horario.horaInicial='" + itemSelecionadoHoraInicial + "'"
                            + "AND horario.horaFinal='" + itemSelecionadoHoraFinal + "'";
                    pst = conexao.prepareStatement(sqlSelect);
                    rsSelect = pst.executeQuery();

                    //se existir um id do item informado
                    if (rsSelect.next()) {
                        String idHorario = rsSelect.getString(1);
                        String idTurma = rsSelect.getString(2);
                        JOptionPane.showMessageDialog(rootPane, idTurma);

                        sql = "UPDATE turma SET dataInicial=?, dataFinal=? WHERE id = " + idTurma;
                        pst = conexao.prepareStatement(sql);
                        pst.setString(1, converterDataParaMysql(jDateDataInicial.getDate()));
                        pst.setString(2, converterDataParaMysql(jDateDataFinal.getDate()));
                        pst.executeUpdate();

                        sql = "UPDATE horario SET diaSemana=?, horaInicial=?, horaFinal=? WHERE id = " + idHorario;
                        pst = conexao.prepareStatement(sql);
                        pst.setString(1, jCBDiaSemana.getSelectedItem().toString());
                        pst.setString(2, txtHoraInicial.getText());
                        pst.setString(3, txtHoraFinal.getText());
                        pst.executeUpdate();
                    }
                    atualizaTableTurmas();
                    JOptionPane.showMessageDialog(rootPane, "Turma editada com sucesso!", "Sucesso", 1);
                    limparCampos();
                    conexao.close();
                } catch (SQLException ex) {
                    //aviso caso tenha erro no try
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(rootPane, "Falha ao editar a turma.", "Erro", 0);
                }
            }
        }
    }//GEN-LAST:event_btnAdicionarActionPerformed

    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed
        if ("".equals(txtPesquisa.getText())) {
            JOptionPane.showMessageDialog(rootPane, "Preencha o(s) campo(s).", "Erro", 0);
            txtPesquisa.requestFocus();
        } else {
            realizarPesquisa(txtPesquisa.getText());
        }
    }//GEN-LAST:event_btnPesquisarActionPerformed

    private void btnAtualizarPaginaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtualizarPaginaActionPerformed
        atualizaTableTurmas();
        limparCampos();
    }//GEN-LAST:event_btnAtualizarPaginaActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        //verifica se foi selecionado ou não uma linha da tabela
        if (jTableTurmas.getSelectedRowCount() == 0) {
            JOptionPane.showMessageDialog(rootPane, "Selecione uma linha da tabela para efetuar a exclusão.", "Erro", 0);
        } else {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rsSelect;
            String idHorario;
            String idTurma;
            String sql;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            for (int i = 0; i < jTableTurmas.getSelectedRowCount(); i++) {
                try {
                    String itemSelecionadoDiaSemana = jTableTurmas.getValueAt(jTableTurmas.getSelectedRows()[i], 0).toString();
                    String itemSelecionadoHoraInicial = jTableTurmas.getValueAt(jTableTurmas.getSelectedRows()[i], 1).toString();
                    String itemSelecionadoHoraFinal = jTableTurmas.getValueAt(jTableTurmas.getSelectedRows()[i], 2).toString();
                    String sqlSelect = "SELECT horario.id, turma.id FROM horario "
                            + "INNER JOIN turma ON horario.idTurma = turma.id "
                            + "WHERE horario.diaSemana='" + itemSelecionadoDiaSemana + "'"
                            + "AND horario.horaInicial='" + itemSelecionadoHoraInicial + "'"
                            + "AND horario.horaFinal='" + itemSelecionadoHoraFinal + "'";
                    pst = conexao.prepareStatement(sqlSelect);
                    rsSelect = pst.executeQuery();

                    //se existir um id
                    if (rsSelect.next()) {
                        idHorario = rsSelect.getString(1);
                        idTurma = rsSelect.getString(2);

                        sql = "UPDATE horario SET status = 0 WHERE id='" + idHorario + "'";
                        pst = conexao.prepareStatement(sql);
                        pst.executeUpdate();

                        sql = "UPDATE turma SET status = 0 WHERE id='" + idTurma + "'";
                        pst = conexao.prepareStatement(sql);
                        pst.executeUpdate();

                        atualizaTableTurmas();
                        JOptionPane.showMessageDialog(rootPane, "Turma(s) excluida(s) com sucesso!", "Sucesso", 1);
                        limparCampos();

                        conexao.close();

                    }
                } catch (SQLException ex) {
                    //aviso caso tenha erro no try
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(rootPane, "Falha ao excluir turma(s).", "Erro", 0);
                }
            }
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        /*verifica se foi selecionado ou não uma linha da tabela
        para que em seguida seja efetuada a edição do mesmo*/
        switch (jTableTurmas.getSelectedRowCount()) {
            case 0:
                JOptionPane.showMessageDialog(rootPane, "Selecione uma linha da tabela para efetuar a edição.", "Erro", 0);
                break;
            case 1:
                Connection conexao;
                PreparedStatement pst;
                ResultSet rsSelect;
                //realizando a conexão estipulada na classe ModuloConexao
                conexao = ModuloConexao.conector();
                try {
                    String itemSelecionadoDiaSemana = jTableTurmas.getValueAt(jTableTurmas.getSelectedRow(), 0).toString();
                    String itemSelecionadoHoraInicial = jTableTurmas.getValueAt(jTableTurmas.getSelectedRow(), 1).toString();
                    String itemSelecionadoHoraFinal = jTableTurmas.getValueAt(jTableTurmas.getSelectedRow(), 2).toString();
                    String sqlSelect = "SELECT horario.diaSemana, horario.horaInicial, "
                            + "horario.horaFinal, turma.dataInicial, turma.dataFinal FROM horario "
                            + "INNER JOIN turma ON horario.idTurma = turma.id "
                            + "WHERE horario.diaSemana='" + itemSelecionadoDiaSemana + "'"
                            + "AND horario.horaInicial='" + itemSelecionadoHoraInicial + "'"
                            + "AND horario.horaFinal='" + itemSelecionadoHoraFinal + "'";
                    pst = conexao.prepareStatement(sqlSelect);
                    rsSelect = pst.executeQuery();
                    //se existir um id do dado informado
                    if (rsSelect.next()) {
                        jCBDiaSemana.setSelectedItem(rsSelect.getString(1));
                        txtHoraInicial.setText(rsSelect.getString(2));
                        txtHoraFinal.setText(rsSelect.getString(3));
                        jDateDataInicial.setDate(rsSelect.getDate(4));
                        jDateDataFinal.setDate(rsSelect.getDate(5));
                        permitirEdicao = true;
                    }
                    //fechando a conexão
                    conexao.close();

                } catch (SQLException ex) {
                    //aviso caso tenha erro no try
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(rootPane, "Falha ao solicitar edição de turma.", "Erro", 0);
                }
                break;
            default:
                JOptionPane.showMessageDialog(rootPane, "Selecione somente uma linha para efetuar a edição.", "Erro", 0);
                break;

        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void formInternalFrameActivated(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameActivated
        jTableTurmas.setDefaultEditor(Object.class, null);
        jTableTurmas.setRowSelectionAllowed(true);
    }//GEN-LAST:event_formInternalFrameActivated

    private void jCBDiaSemanaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCBDiaSemanaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCBDiaSemanaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdicionar;
    private javax.swing.JButton btnAtualizarPagina;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnPesquisar;
    private javax.swing.JComboBox<String> jCBDiaSemana;
    private com.toedter.calendar.JDateChooser jDateDataFinal;
    private com.toedter.calendar.JDateChooser jDateDataInicial;
    private com.toedter.calendar.JDayChooser jDayChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableTurmas;
    private javax.swing.JFormattedTextField txtHoraFinal;
    private javax.swing.JFormattedTextField txtHoraInicial;
    private javax.swing.JTextField txtPesquisa;
    // End of variables declaration//GEN-END:variables
}

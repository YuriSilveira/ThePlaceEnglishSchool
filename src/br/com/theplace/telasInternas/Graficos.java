package br.com.theplace.telasInternas;

import br.com.theplace.dal.ModuloConexao;
import br.com.theplace.dao.CursosDAO;
import java.awt.BorderLayout;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

public class Graficos extends javax.swing.JInternalFrame {

    public Graficos() {
        initComponents();
    }

    private void gerarGraficoAtivosInativos() {
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs01;
            ResultSet rs02;
            String sql;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            sql = "SELECT count(aluno.id) FROM aluno WHERE aluno.status = 1";
            pst = conexao.prepareStatement(sql);
            rs01 = pst.executeQuery();

            sql = "SELECT count(aluno.id) FROM aluno WHERE aluno.status = 0";
            pst = conexao.prepareStatement(sql);
            rs02 = pst.executeQuery();

            DefaultPieDataset dataset = new DefaultPieDataset();
            while (rs01.next() && rs02.next()) {
                for (int i = 1; i <= 2; i++) {
                    if (i == 1) {
                        dataset.setValue("Alunos Ativos (" + rs01.getInt(1) + ") ", rs01.getInt(1));
                    } else {
                        dataset.setValue("Alunos Inativos (" + rs02.getInt(1) + ") ", rs02.getInt(1));
                    }
                }
            }
            JFreeChart chart = ChartFactory.createPieChart(
                    "Alunos Ativos e Inativos",
                    dataset,
                    true,
                    true,
                    false);
            ChartPanel chartPanel = new ChartPanel(chart);
            jPanelGraficoAlunos.removeAll();
            jPanelGraficoAlunos.add(chartPanel, BorderLayout.CENTER);
            jPanelGraficoAlunos.validate();

            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(rootPane, "Falha ao apresentar gráfico.", "Erro", 0);
        }
    }

    private void gerarCursosQuantidade() {
        try {
            CursosDAO cursosDAO = new CursosDAO();
            cursosDAO.read();      
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;
            String sql;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            sql = "SELECT curso.nome , curso.quantLivros FROM curso";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            DefaultPieDataset dataset = new DefaultPieDataset();
            while (rs.next()) {
                dataset.setValue(rs.getString(1) + " (" + rs.getInt(2) + ") ", rs.getInt(2));
            }
            JFreeChart chart = ChartFactory.createPieChart(
                    "Quantidade de Livros nos cursos",
                    dataset,
                    true,
                    true,
                    false);
            ChartPanel chartPanel = new ChartPanel(chart);
            jPanelGraficoQuantLivros.removeAll();
            jPanelGraficoQuantLivros.add(chartPanel, BorderLayout.CENTER);
            jPanelGraficoQuantLivros.validate();

            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(rootPane, "Falha ao apresentar gráfico.", "Erro", 0);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelGraficoAlunos = new javax.swing.JPanel();
        jPanelGraficoQuantLivros = new javax.swing.JPanel();

        setClosable(true);
        setIconifiable(true);
        setResizable(true);
        setTitle("Apresentação de Gráficos");
        setMinimumSize(new java.awt.Dimension(725, 470));
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameActivated(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        jPanelGraficoAlunos.setLayout(new java.awt.BorderLayout());

        jPanelGraficoQuantLivros.setLayout(new java.awt.BorderLayout());

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanelGraficoAlunos, javax.swing.GroupLayout.PREFERRED_SIZE, 463, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanelGraficoQuantLivros, javax.swing.GroupLayout.PREFERRED_SIZE, 463, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanelGraficoQuantLivros, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanelGraficoAlunos, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formInternalFrameActivated(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameActivated
        gerarGraficoAtivosInativos();
        gerarCursosQuantidade();
    }//GEN-LAST:event_formInternalFrameActivated


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanelGraficoAlunos;
    private javax.swing.JPanel jPanelGraficoQuantLivros;
    // End of variables declaration//GEN-END:variables
}

package br.com.theplace.telasInternas;

import br.com.theplace.dal.ModuloConexao;
import br.com.theplace.dao.CursosDAO;
import br.com.theplace.model.Curso;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class Cursos extends javax.swing.JInternalFrame {

    //atributo para verficação da edição
    boolean permitirEdicao = false;

    public Cursos() {
        initComponents();
        getRootPane().setDefaultButton(btnAdicionar);
        //tabela é atualizada ao ser inicializado o jInternalFrame
        atualizaTableCurso();
    }

    /**
     * Método limpa os campos do jInternalFrame
     */
    private void limparCampos() {
        txtNomeCurso.setText("");
        txtPesquisa.setText("");
        permitirEdicao = false;
        txtNomeCurso.requestFocus();
    }

    /**
     * Método que atualiza a tabela de acordo com os dados do banco
     */
    private void atualizaTableCurso() {
        //organizando tabela para inserir os dados na mesma
        DefaultTableModel model = new DefaultTableModel();
        this.jTableModulos.setModel(model);

        //adicionando colunas na tabela
        model.addColumn("Nome");
        model.addColumn("Quantidade de Livros");

        CursosDAO cursoDAO = new CursosDAO();

        for (Curso c : cursoDAO.read()) {
            if (c.getStatus() == 1) {
                model.addRow(new Object[]{
                    c.getNome(),
                    c.getQuantLivros()
                });
            }
        }

        //alinhando no centro os dados que foram inseridos
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        jTableModulos.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        jTableModulos.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
    }

    /**
     * Método efetua pesquisa de acordo com o parâmetro recebido
     */
    private void realizarPesquisa() {
        //organizando tabela para inserir os dados na mesma
        DefaultTableModel model = new DefaultTableModel();
        this.jTableModulos.setModel(model);

        //adicionando colunas na tabela
        model.addColumn("Nome");
        model.addColumn("Quantidade de Livros");

        CursosDAO cursoDAO = new CursosDAO();

        for (Curso c : cursoDAO.search(txtPesquisa.getText())) {
            if (c.getStatus() == 1) {
                model.addRow(new Object[]{
                    c.getNome(),
                    c.getQuantLivros()
                });
            }
        }

        //alinhando no centro os dados que foram inseridos
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        jTableModulos.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        jTableModulos.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
    }

    /**
     * Método verifica se já existi no banco, o dado informado por parâmetro
     *
     * @param dadoRecebido - Texto informado pelo usuário
     * @return - Retorna um boolean
     */
    private boolean verificaSeExisti(String dadoRecebido) {
        boolean result = false;
        try {
            Connection conexao;
            PreparedStatement pst;
            ResultSet rs;

            //realizando a conexão estipulada na classe ModuloConexao
            conexao = ModuloConexao.conector();
            String sql = "select id from curso where nome = '" + dadoRecebido + "'";
            pst = conexao.prepareStatement(sql);
            rs = pst.executeQuery();

            if (rs.next()) {
                result = true;
            }
            //fechando a conexão
            conexao.close();
        } catch (SQLException ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(rootPane, "Falha ao verificar se existi.", "Erro", 0);
        }
        return result;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnAdicionar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtNomeCurso = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableModulos = new javax.swing.JTable();
        btnEditar = new javax.swing.JButton();
        btnPesquisar = new javax.swing.JButton();
        btnExcluir = new javax.swing.JButton();
        txtPesquisa = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        btnAtualizarPagina = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setResizable(true);
        setTitle("Gerenciar Cursos");
        setMinimumSize(new java.awt.Dimension(725, 470));
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameActivated(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        btnAdicionar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/create.png"))); // NOI18N
        btnAdicionar.setToolTipText("Cadastrar/Editar");
        btnAdicionar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAdicionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdicionarActionPerformed(evt);
            }
        });

        jLabel1.setText("Nome do curso:");

        jTableModulos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableModulos.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jScrollPane1.setViewportView(jTableModulos);

        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/edit.png"))); // NOI18N
        btnEditar.setToolTipText("Alterar");
        btnEditar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnPesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/search.png"))); // NOI18N
        btnPesquisar.setToolTipText("Pesquisar");
        btnPesquisar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });

        btnExcluir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/delete.png"))); // NOI18N
        btnExcluir.setToolTipText("Excluir");
        btnExcluir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnExcluir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExcluirActionPerformed(evt);
            }
        });

        jLabel3.setText("Pesquisar por curso:");

        btnAtualizarPagina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/update.png"))); // NOI18N
        btnAtualizarPagina.setToolTipText("Atualizar Janela");
        btnAtualizarPagina.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAtualizarPagina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtualizarPaginaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtNomeCurso, javax.swing.GroupLayout.PREFERRED_SIZE, 306, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnAdicionar, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(97, Short.MAX_VALUE))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 559, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnAtualizarPagina)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnExcluir)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnEditar)
                .addGap(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtNomeCurso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1)))
                    .addComponent(btnAdicionar))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3))
                    .addComponent(btnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(btnEditar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnExcluir))
                    .addComponent(btnAtualizarPagina))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAdicionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdicionarActionPerformed
        if (permitirEdicao == false) {
            if ("".equals(txtNomeCurso.getText())) {
                JOptionPane.showMessageDialog(rootPane, "Preencha o(s) campo(s).", "Erro", 0);
                txtNomeCurso.requestFocus();
            } else {
                boolean existiJa = verificaSeExisti(txtNomeCurso.getText());
                /*Verificando qual foi o retorno no método verificaSeExisti
                Para que seja efetuada a ação de adicionar ou de editar o dado utilizado*/
                if (existiJa == true) {
                    JOptionPane.showMessageDialog(rootPane, "Curso já cadastrado no sistema", "Erro", 0);
                    txtNomeCurso.requestFocus();
                } else {
                    Curso curso = new Curso();
                    curso.setNome(txtNomeCurso.getText());
                    curso.setQuantLivros(0);
                    curso.setStatus(1);
                    CursosDAO cursoDAO = new CursosDAO();
                    cursoDAO.insert(curso);

                    atualizaTableCurso();
                    limparCampos();
                }
            }
        } else if (permitirEdicao == true) {
            if ("".equals(txtNomeCurso.getText())) {
                JOptionPane.showMessageDialog(rootPane, "Preencha o(s) campo(s).", "Erro", 0);
                txtNomeCurso.requestFocus();
            } else {
                String itemSelecionado = jTableModulos.getValueAt(jTableModulos.getSelectedRow(), 0).toString();
                Curso curso = new Curso();
                curso.setNome(txtNomeCurso.getText());
                CursosDAO cursoDAO = new CursosDAO();
                cursoDAO.update(curso, itemSelecionado);

                atualizaTableCurso();
                limparCampos();
            }
        }
    }//GEN-LAST:event_btnAdicionarActionPerformed

    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed
        if ("".equals(txtPesquisa.getText())) {
            JOptionPane.showMessageDialog(rootPane, "Preencha o(s) campo(s).", "Erro", 0);
            txtPesquisa.requestFocus();
        } else {
            realizarPesquisa();
        }
    }//GEN-LAST:event_btnPesquisarActionPerformed

    private void btnAtualizarPaginaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtualizarPaginaActionPerformed
        atualizaTableCurso();
        limparCampos();
    }//GEN-LAST:event_btnAtualizarPaginaActionPerformed

    private void btnExcluirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExcluirActionPerformed
        //verifica se foi selecionado ou não uma linha da tabela
        if (jTableModulos.getSelectedRowCount() == 0) {
            JOptionPane.showMessageDialog(rootPane, "Selecione uma linha da tabela para efetuar a exclusão.", "Erro", 0);
        } else {
            for (int i = 0; i < jTableModulos.getSelectedRowCount(); i++) {
                String itemSelecionado = jTableModulos.getValueAt(jTableModulos.getSelectedRows()[i], 0).toString();
                CursosDAO cursoDAO = new CursosDAO();
                cursoDAO.delete(itemSelecionado);
            }
            JOptionPane.showMessageDialog(null, "Curso(s) excluido(s) com sucesso!", "Sucesso", 1);
            atualizaTableCurso();
            limparCampos();
        }
    }//GEN-LAST:event_btnExcluirActionPerformed

    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        /*verifica se foi selecionado ou não uma linha da tabela
        para que em seguida seja efetuada a edição do mesmo*/
        switch (jTableModulos.getSelectedRowCount()) {
            case 0:
                JOptionPane.showMessageDialog(rootPane, "Selecione uma linha da tabela para efetuar a edição.", "Erro", 0);
                break;
            case 1:
                String itemSelecionado = jTableModulos.getValueAt(jTableModulos.getSelectedRow(), 0).toString();
                CursosDAO cursoDAO = new CursosDAO();

                for (Curso c : cursoDAO.readUpdate(itemSelecionado)) {
                    if (c.getStatus() == 1) {
                        txtNomeCurso.setText(c.getNome());
                        permitirEdicao = true;
                    }
                }
                break;
            default:
                JOptionPane.showMessageDialog(rootPane, "Selecione somente uma linha para efetuar a edição.", "Erro", 0);
                break;
        }
    }//GEN-LAST:event_btnEditarActionPerformed

    private void formInternalFrameActivated(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameActivated
        jTableModulos.setDefaultEditor(Object.class, null);
        jTableModulos.setRowSelectionAllowed(true);
    }//GEN-LAST:event_formInternalFrameActivated


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdicionar;
    private javax.swing.JButton btnAtualizarPagina;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnExcluir;
    private javax.swing.JButton btnPesquisar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableModulos;
    private javax.swing.JTextField txtNomeCurso;
    private javax.swing.JTextField txtPesquisa;
    // End of variables declaration//GEN-END:variables
}

package br.com.theplace.telasInternas;

import br.com.theplace.classes.PagSeguroParcelas;
import br.com.theplace.dao.AlunosDAO;
import br.com.theplace.dao.HorariosDAO;
import br.com.theplace.dao.ParcelasDAO;
import br.com.theplace.model.Aluno;
import br.com.theplace.model.Horario;
import br.com.theplace.model.Parcela;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

public class EfetuarPagamento extends javax.swing.JInternalFrame {

    boolean permitirEdicao = false;

    public EfetuarPagamento() {
        initComponents();
        getRootPane().setDefaultButton(btnPagamento);
        //exibir dados ComboBox
        exibiTurmasCB();
        //tabela é atualizada ao ser inicializado o jInternalFrame
        atualizaTableAlunos();

    }

    /**
     * Método limpa os campos do jInternalFrame
     */
    private void limparCampos() {
        txtPesquisa.setText("");
        jCBDiaSemana.setSelectedIndex(0);
        jCBTurma.setSelectedIndex(0);
        atualizaTableAlunos();
        limparTabelaParcelas();
    }

    private void limparTabelaParcelas() {
        //organizando tabela para inserir os dados na mesma
        DefaultTableModel model = new DefaultTableModel();
        this.jTableParcelasAluno.setModel(model);

        //alinhando no centro os dados que foram inseridos
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
    }

    /**
     * Método que exibi as turmas no jComboBox
     */
    private void exibiTurmasCB() {
        HorariosDAO horariosDAO = new HorariosDAO();
        for (Horario h : horariosDAO.readComboBox()) {
            if (h.getStatus() == 1) {
                String diaSemana = h.getDiaSemana();
                String horaInicial = h.getHoraInicial();
                String horaFinal = h.getHoraFinal();
                jCBTurma.addItem(diaSemana + " (" + horaInicial + " até " + horaFinal + ")");
            }
        }
    }

    /**
     * Método que atualiza a tabela de acordo com os dados do banco
     */
    private void atualizaTableAlunos() {
        //organizando tabela para inserir os dados na mesma
        DefaultTableModel model = new DefaultTableModel();
        this.jTableAlunos.setModel(model);

        //adicionando colunas na tabela
        model.addColumn("Nome");
        model.addColumn("CPF");
        model.addColumn("Turma");

        AlunosDAO alunosDAO = new AlunosDAO();
        HorariosDAO horariosDAO = new HorariosDAO();
        ArrayList<Horario> horario;

        for (Aluno a : alunosDAO.read()) {
            for (Horario h : horariosDAO.retornoInfo(a.getIdTurma())) {
                if (a.getStatus() == 1) {
                    model.addRow(new Object[]{
                        a.getNome(),
                        a.getCpf(),
                        h.getDiaSemana() + " (" + h.getHoraInicial() + " até " + h.getHoraFinal() + ")"
                    });
                }
            }
        }

        //alinhando no centro os dados que foram inseridos
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();

        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        jTableAlunos.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        jTableAlunos.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        jTableAlunos.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
    }

    /**
     * Método que atualiza a tabela de acordo com os dados do banco
     */
    private void atualizaTableParcelasAluno(int idMatricula) {
        //organizando tabela para inserir os dados na mesma
        DefaultTableModel model = new DefaultTableModel();
        this.jTableParcelasAluno.setModel(model);

        //adicionando colunas na tabela
        model.addColumn("Parcela");
        model.addColumn("Valor Parcela");
        model.addColumn("Data Vencimento");
        model.addColumn("Status");

        ParcelasDAO parcelasDAO = new ParcelasDAO();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        for (Parcela p : parcelasDAO.readParcelaAluno(idMatricula)) {
            try {
                if (p.getStatus() != "Pago") {
                    Date date = sdf.parse(p.getDataVencimento());
                    model.addRow(new Object[]{
                        p.getNumParcela() + "/" + p.getTotalParcela(),
                        "R$ " + p.getValorParcela(),
                        converterDataParaUsuario(date),
                        p.getStatus()
                    });
                }
            } catch (ParseException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(rootPane, "Falha na conversão da data.", "Erro", 0);
            }
        }

        //alinhando no centro os dados que foram inseridos
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();

        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        jTableParcelasAluno.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        jTableParcelasAluno.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        jTableParcelasAluno.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        jTableParcelasAluno.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
    }

    /**
     * Método efetua pesquisa de acordo com o parâmetro recebido
     */
    private void realizarPesquisa() {
//organizando tabela para inserir os dados na mesma
        DefaultTableModel model = new DefaultTableModel();
        this.jTableAlunos.setModel(model);

        //adicionando colunas na tabela
        model.addColumn("Nome");
        model.addColumn("CPF");
        model.addColumn("Turma");

        AlunosDAO alunosDAO = new AlunosDAO();
        HorariosDAO horariosDAO = new HorariosDAO();
        ArrayList<Horario> horario;

        for (Aluno a : alunosDAO.search(txtPesquisa.getText())) {
            for (Horario h : horariosDAO.retornoInfo(a.getIdTurma())) {
                if (a.getStatus() == 1) {
                    model.addRow(new Object[]{
                        a.getNome(),
                        a.getCpf(),
                        h.getDiaSemana() + " (" + h.getHoraInicial() + " até " + h.getHoraFinal() + ")"
                    });
                }
            }
        }

        //alinhando no centro os dados que foram inseridos
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();

        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        jTableAlunos.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        jTableAlunos.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        jTableAlunos.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
    }

    /**
     * Método faz a converção da data para que se adapte a do MySQL
     *
     * @param dataRecebida - dado informado pelo usuário
     * @return String de data convertida
     */
    private String converterDataParaMysql(Date dataRecebida) {
        //recebo 03/09/1996 e devo gravar 1996-09-03(yyyy/mm/dd)
        SimpleDateFormat dataFormato = new SimpleDateFormat("yyyy-MM-dd");
        String resultado = dataFormato.format(dataRecebida);
        return resultado;
    }

    /**
     * Método faz a converção da data do mysql para o Usuário
     *
     * @param dataRecebida - dado informado
     * @return String de data convertida
     */
    private String converterDataParaUsuario(Date dataRecebida) {
        //recebo 1996-09-03 e devo exibir 03/09/1996(dd/mm/yyyy)
        SimpleDateFormat dataFormato = new SimpleDateFormat("dd/MM/yyyy");
        String resultado = dataFormato.format(dataRecebida);
        return resultado;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnPagamento = new javax.swing.JButton();
        btnPesquisar = new javax.swing.JButton();
        txtPesquisa = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jCBTurma = new javax.swing.JComboBox<>();
        jLabel15 = new javax.swing.JLabel();
        jCBDiaSemana = new javax.swing.JComboBox<>();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableAlunos = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableParcelasAluno = new javax.swing.JTable();
        btnAtualizarPagina = new javax.swing.JButton();

        setClosable(true);
        setIconifiable(true);
        setResizable(true);
        setTitle("Efetuar Pagamento");
        setMinimumSize(new java.awt.Dimension(725, 470));
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameActivated(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosed(evt);
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        btnPagamento.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/create.png"))); // NOI18N
        btnPagamento.setText("Efetuar Pagamento");
        btnPagamento.setToolTipText("Cadastrar/Editar");
        btnPagamento.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPagamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPagamentoActionPerformed(evt);
            }
        });

        btnPesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/search.png"))); // NOI18N
        btnPesquisar.setToolTipText("Pesquisar");
        btnPesquisar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPesquisarActionPerformed(evt);
            }
        });

        jLabel3.setText("Pesquisar por aluno:");

        jLabel14.setText("Turma:");

        jCBTurma.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Escolha uma das opções" }));
        jCBTurma.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCBTurmaActionPerformed(evt);
            }
        });

        jLabel15.setText("Dia da semana:");

        jCBDiaSemana.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Escolha uma das opções", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab", "Dom" }));
        jCBDiaSemana.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCBDiaSemanaActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Informações sobre o aluno"));

        jTableAlunos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableAlunos.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jTableAlunos.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTableAlunosFocusLost(evt);
            }
        });
        jTableAlunos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableAlunosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTableAlunos);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 623, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 315, Short.MAX_VALUE)
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Parcelas do aluno"));

        jTableParcelasAluno.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableParcelasAluno.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jScrollPane2.setViewportView(jTableParcelasAluno);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 588, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 231, Short.MAX_VALUE)
        );

        btnAtualizarPagina.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/com/theplace/icones/update.png"))); // NOI18N
        btnAtualizarPagina.setToolTipText("Atualizar Janela");
        btnAtualizarPagina.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnAtualizarPagina.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtualizarPaginaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, 385, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(89, 89, 89))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel14)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCBTurma, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel15)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCBDiaSemana, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnAtualizarPagina)
                        .addGap(18, 18, 18)
                        .addComponent(btnPagamento))
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnPagamento, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btnAtualizarPagina, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(7, 7, 7)
                                .addComponent(btnPesquisar, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtPesquisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel3))))
                        .addGap(6, 6, 6)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel15)
                            .addComponent(jCBDiaSemana, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14)
                            .addComponent(jCBTurma, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnPagamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPagamentoActionPerformed
        try {
            String itemSelecionado = jTableAlunos.getValueAt(jTableAlunos.getSelectedRow(), 1).toString();
            int idMatricula = new AlunosDAO().returnIDMatricula(itemSelecionado);
            int idRespon = new AlunosDAO().returnIDResponsavel(itemSelecionado);
            PagSeguroParcelas psParcelas = new PagSeguroParcelas(idMatricula, idRespon);
            String resultado = psParcelas.criarPagamento();
            java.awt.Desktop.getDesktop().browse(new java.net.URI(resultado));
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (URISyntaxException ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_btnPagamentoActionPerformed

    private void btnPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPesquisarActionPerformed
        if ("".equals(txtPesquisa.getText())) {
            JOptionPane.showMessageDialog(rootPane, "Preencha o(s) campo(s).", "Erro", 0);
            txtPesquisa.requestFocus();
        } else {
            realizarPesquisa();
        }
    }//GEN-LAST:event_btnPesquisarActionPerformed

    private void formInternalFrameActivated(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameActivated
        jTableAlunos.setDefaultEditor(Object.class, null);
        jTableAlunos.setRowSelectionAllowed(true);
    }//GEN-LAST:event_formInternalFrameActivated

    private void jCBDiaSemanaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCBDiaSemanaActionPerformed
        if (jCBDiaSemana.getSelectedItem().toString() != "") {

        } else {

        }
    }//GEN-LAST:event_jCBDiaSemanaActionPerformed

    private void jCBTurmaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCBTurmaActionPerformed
        if (jCBDiaSemana.getSelectedItem().toString() != "") {

        } else {

        }
    }//GEN-LAST:event_jCBTurmaActionPerformed

    private void jTableAlunosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableAlunosMouseClicked
        String itemSelecionado = jTableAlunos.getValueAt(jTableAlunos.getSelectedRow(), 1).toString();
        int idMatricula = new AlunosDAO().returnIDMatricula(itemSelecionado);
        atualizaTableParcelasAluno(idMatricula);
    }//GEN-LAST:event_jTableAlunosMouseClicked

    private void formInternalFrameClosed(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosed

    }//GEN-LAST:event_formInternalFrameClosed

    private void btnAtualizarPaginaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtualizarPaginaActionPerformed
        limparCampos();
    }//GEN-LAST:event_btnAtualizarPaginaActionPerformed

    private void jTableAlunosFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTableAlunosFocusLost
        limparTabelaParcelas();
    }//GEN-LAST:event_jTableAlunosFocusLost


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAtualizarPagina;
    private javax.swing.JButton btnPagamento;
    private javax.swing.JButton btnPesquisar;
    private javax.swing.JComboBox<String> jCBDiaSemana;
    private javax.swing.JComboBox<String> jCBTurma;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTableAlunos;
    private javax.swing.JTable jTableParcelasAluno;
    private javax.swing.JTextField txtPesquisa;
    // End of variables declaration//GEN-END:variables
}

package br.com.theplace.model;

import java.sql.Date;

public class Parcela {

    private int numParcela;
    private int totalParcela;
    private double valorParcela;
    private String dataVencimento;
    private String status;
    private int idMatricula;

    public int getNumParcela() {
        return numParcela;
    }

    public void setNumParcela(int numParcela) {
        this.numParcela = numParcela;
    }

    public int getTotalParcela() {
        return totalParcela;
    }

    public void setTotalParcela(int totalParcela) {
        this.totalParcela = totalParcela;
    }

    public double getValorParcela() {
        return valorParcela;
    }

    public void setValorParcela(double valorParcela) {
        this.valorParcela = valorParcela;
    }

    public String getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(String dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getIdMatricula() {
        return idMatricula;
    }

    public void setIdMatricula(int idMatricula) {
        this.idMatricula = idMatricula;
    }
}

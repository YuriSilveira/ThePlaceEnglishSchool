package br.com.theplace.model;

public class Usuario {

    private String nome;
    private String email;
    private String senha;
    private String hashTemporaria;
    private int status;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getHashTemporaria() {
        return hashTemporaria;
    }

    public void setHashTemporaria(String hashTemporaria) {
        this.hashTemporaria = hashTemporaria;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}

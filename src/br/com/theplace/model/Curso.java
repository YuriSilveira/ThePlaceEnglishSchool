package br.com.theplace.model;

public class Curso {

    private String nome;
    private int quantLivros;
    private int status;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQuantLivros() {
        return quantLivros;
    }

    public void setQuantLivros(int quantLivros) {
        this.quantLivros = quantLivros;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}

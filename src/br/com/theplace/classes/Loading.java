package br.com.theplace.classes;

import java.awt.BorderLayout;
import java.awt.Color;
import java.net.URL;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

public class Loading extends Thread {

    JFrame janela;
    JLabel lblLoading = new JLabel();
    int tempo;
    JFrame frameRecebido = null;
    boolean possuiFrame;

    public Loading(int tempo, JFrame frameRecebido) {
        janela = new JFrame();
        janela.setUndecorated(true);
        janela.setSize(600, 500);
        janela.getContentPane().setBackground(Color.WHITE);
        janela.setLocationRelativeTo(null);
        janela.add(BorderLayout.CENTER, lblLoading);
        lblLoading.setHorizontalAlignment(SwingConstants.CENTER);
        janela.setVisible(true);
        this.tempo = tempo;
        this.frameRecebido = frameRecebido;
        possuiFrame = true;
    }

    public Loading(int tempo) {
        janela = new JFrame();
        janela.setUndecorated(true);
        janela.setSize(800, 600);
        janela.getContentPane().setBackground(Color.WHITE);
        janela.setLocationRelativeTo(null);
        janela.add(BorderLayout.CENTER, lblLoading);
        lblLoading.setHorizontalAlignment(SwingConstants.CENTER);
        janela.setVisible(true);
        this.tempo = tempo;
        possuiFrame = false;
    }

    @Override
    public void run() {
        URL urlDiretorioLocal = this.getClass().getResource("/br/com/theplace/imagens/");
        lblLoading.setIcon(new ImageIcon(urlDiretorioLocal.getPath() + "loading.gif"));
        for (int i = 1; i <= 100; i++) {
            if (i == 100) {
                janela.dispose();
                if (possuiFrame == true) {
                    frameRecebido.setVisible(true);
                }
            }
            try {
                Thread.sleep(tempo);
            } catch (InterruptedException ex) {
                //aviso caso tenha erro no try
                ex.printStackTrace();
                JOptionPane.showMessageDialog(null, "Falha ao realizar o Loading", "Erro", 0);
            }
        }
    }
}

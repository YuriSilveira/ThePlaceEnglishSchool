package br.com.theplace.classes;

import br.com.theplace.dal.PagSeguroInterface;
import br.com.theplace.dao.CidadesDAO;
import br.com.theplace.dao.EnderecosDAO;
import br.com.theplace.dao.EstadosDAO;
import br.com.theplace.dao.ParcelasDAO;
import br.com.theplace.dao.ResponsaveisDAO;
import br.com.theplace.model.Endereco;
import br.com.theplace.model.Parcela;
import br.com.theplace.model.Responsavel;
import br.com.uol.pagseguro.domain.AccountCredentials;
import br.com.uol.pagseguro.domain.Address;
import br.com.uol.pagseguro.domain.Credentials;
import br.com.uol.pagseguro.domain.PaymentRequest;
import br.com.uol.pagseguro.domain.Phone;
import br.com.uol.pagseguro.domain.Sender;
import br.com.uol.pagseguro.domain.Shipping;
import br.com.uol.pagseguro.domain.Item;
import br.com.uol.pagseguro.domain.Transaction;
import br.com.uol.pagseguro.enums.Currency;
import br.com.uol.pagseguro.enums.ShippingType;
import br.com.uol.pagseguro.exception.PagSeguroServiceException;
import br.com.uol.pagseguro.service.NotificationService;
import java.math.BigDecimal;

public class PagSeguroParcelas implements PagSeguroInterface {

    private final String emailPagSeguro = "cristina_falcaosoares@hotmail.com";
    private final String tokenPagSeguro = "06C97C2DD364450AB71EED1B1D0B96CF";

    int idItem;
    int idSender;

    public PagSeguroParcelas(int idItem, int idSender) {
        this.idItem = idItem;
        this.idSender = idSender;
    }

    @Override
    public String criarPagamento() {
        try {
            PaymentRequest request = new PaymentRequest();
            ParcelasDAO parcelasDAO = new ParcelasDAO();
            //id que será identificado no PagSeguro
            request.setReference("001");
            request.setCurrency(Currency.BRL);
            request.setSender(getSender());
            //setando o frete caso seja necessário
            request.setShipping(getShipping());
            //adicionando produtos na compra
            for (Parcela p : parcelasDAO.readParcelaAluno(idItem)) {
                request.addItem(getItem(p));
            }
            request.setNotificationURL("http://licensascurae.doware.com.br/contratos/pagseguro-notificacao");
            request.setRedirectURL("www.seusite.com.br");
            return request.register(getCredentials());
        } catch (PagSeguroServiceException ex) {
            ex.printStackTrace();
            return ex.getMessage();
        }
    }

    //dados do pagador...
    @Override
    public Sender getSender() {
        Sender sender = new Sender();
        ResponsaveisDAO responsaveisDAO = new ResponsaveisDAO();
        for (Responsavel r : responsaveisDAO.readUpdate(String.valueOf(idSender))) {
            sender.setName(r.getNome());
            sender.setEmail(r.getEmail());
            sender.setPhone(new Phone(r.getTelefoneA().substring(1, 4), r.getTelefoneA().substring(5)));
        }
        return sender;
    }

    //dados do frete...
    @Override
    public Shipping getShipping() {
        Shipping shipping = new Shipping();
        int idEndereco = new ResponsaveisDAO().returnIDEndereco(idSender);
        shipping.setAddress(getAddress(idEndereco));
        shipping.setCost(new BigDecimal("0.00"));
        shipping.setType(ShippingType.PAC);
        return shipping;
    }

    //dados do endereço para o frete
    @Override
    public Address getAddress(int id) {
        Address address = new Address();
        EnderecosDAO enderecosDAO = new EnderecosDAO();
        CidadesDAO cidadesDAO = new CidadesDAO();
        EstadosDAO estadosDAO = new EstadosDAO();
        for (Endereco e : enderecosDAO.readUpdate(String.valueOf(id))) {
            address.setCity(cidadesDAO.returnInfo(id));
            int idEstado = cidadesDAO.returnIDEstado(id);
            address.setComplement(e.getComplemento());
            address.setCountry("Brasil");
            address.setState(estadosDAO.returnInfo(idEstado));
            address.setPostalCode(e.getCep());
            address.setNumber(String.valueOf(e.getNumero()));
            address.setDistrict(e.getBairro());
        }
        return address;
    }

    //dados do produto
    @Override
    public Item getItem(Parcela p) {
        //idItem nesse caso é o idMatricula ATENÇÃO
        Item item = new Item();
        item.setId("001");
        item.setDescription(p.getNumParcela() + " de " + p.getTotalParcela() + " -> THE PLACE");
        item.setQuantity(1);
        item.setAmount(new BigDecimal("100.00"));
        //cada item da compra tem um valor de frete 
        //ai seta o ShippingCost....
        //item.setShippingCost(new BigDecimal("10.00"));
        return item;
    }
    //dados credentiais da conta do pagSeguro

    private Credentials getCredentials() throws PagSeguroServiceException {
        return new AccountCredentials(emailPagSeguro, tokenPagSeguro);
    }

    void registrarNotificacao() {
        try {
            //pegar o código da notificação gerado pelo pagseguro
            String nCode = null;
            Transaction transaction = NotificationService.checkTransaction(getCredentials(), nCode);
            ParcelasDAO parcelasDAO = new ParcelasDAO();
            switch (transaction.getStatus()) {
                case PAID:
                    parcelasDAO.updateStatus("PAGA", idItem);
                    break;
                case CANCELLED:
                    //TRANSAÇÃO CANCELADA
                    parcelasDAO.updateStatus("TRANSAÇÃO CANCELADA", idItem);
                    break;
                case WAITING_PAYMENT:
                    //TRANSAÇÃO AGUARDANDO PAGAMENTO
                    parcelasDAO.updateStatus("TRANSAÇÃO AGUARDANDO PAGAMENTO", idItem);
                    break;
                case IN_ANALYSIS:
                    //TRANSAÇÃO EM ANÁLISE
                    parcelasDAO.updateStatus("TRANSAÇÃO EM ANÁLISE", idItem);
                    break;
            }
        } catch (PagSeguroServiceException ex) {
            ex.printStackTrace();
        }
    }

}

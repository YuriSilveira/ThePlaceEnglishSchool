package br.com.theplace.classes;

import br.com.theplace.dal.ModuloConexao;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.swing.JOptionPane;

public class EnviarEmail {

    Connection conexao = null;
    PreparedStatement pst = null;
    ResultSet rs = null;

    public EnviarEmail() {
        conexao = ModuloConexao.conector();
    }

    private static String gerarSenhaTemporaria() {
        char[] chart = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
        char[] senha = new char[10];
        int chartLenght = chart.length;
        Random rdm = new Random();
        for (int x = 0; x < 10; x++) {
            senha[x] = chart[rdm.nextInt(chartLenght)];
        }
        return new String(senha);
    }

//Dados para ser realizado o envio do e-mail;
    /*
     * E-mail: theplaceenglishschoolrh@gmail.com -- Senha: The@place
     */
    public void enviarEmailRecuperarSenha(String emailUsuario) {
        try {
            String senhaTemporariaGerada = gerarSenhaTemporaria();

            String sql = "update usuario set hashTemporaria = ? where email = '" + emailUsuario + "'";
            pst = conexao.prepareStatement(sql);
            pst.setString(1, senhaTemporariaGerada);
            pst.executeUpdate();
            conexao.close();

            String host = "smtp.gmail.com";
            String user = "theplaceenglishschoolrh@gmail.com";
            String pass = "The@place";
            String to = emailUsuario; //para quem será enviado
            String from = "theplaceenglishschoolrh@gmail.com";
            String subject = "The Place English School"; //assunto

            String messageText = "<h1 style='color: #25383C; text-align: center;'> The Place English School </h1>"
                    + "<h2 style='color: #616D7E; text-align: center;'>Recuperação de Senha</h2>"
                    + "<h4 style='color: #2C3539; text-align: center;'>Foi solicitada a recuperação de senha.<br>"
                    + "É necessário que insira o código abaixo na página de renovação de senha.</h4>"
                    + "<h1 style='color: #4cae4c; text-align: center;'>" + senhaTemporariaGerada + "</h1>"
                    + "<h6 style='color: #2C3539; text-align: center;'>Se você não solicitou uma recuperação de senha, ignore este e-mail.<br>"
                    + "Sua senha não será alterada até que você adicione o códido acima e crie uma nova.</h6>";
            boolean sessionDebug = false;

            Properties props = System.getProperties();

            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.required", "true");

            java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            // we need security so java already provide security
            Session mailSession = Session.getDefaultInstance(props, null);
            mailSession.setDebug(sessionDebug);
            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(from, "The Place English School"));
            InternetAddress[] address = {new InternetAddress(to)}; //address of sender
            msg.setRecipients(Message.RecipientType.TO, address); //receiver e-mail
            msg.setSubject(subject);
            msg.setSentDate(new Date());
            msg.setContent(messageText, "text/html; charset=utf-8");

            Transport transport = mailSession.getTransport("smtp");
            transport.connect(host, user, pass);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();

            JOptionPane.showMessageDialog(null, "E-mail enviado a partir da solicitação de recuperação de senha.", "Sucesso", 1);
        } catch (Exception ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao enviar o e-mail de recuperação de senha.", "Erro", 0);
        }
    }

    public void enviarEmailBemVindo(String emailUsuario, String nomeUsuario) {
        try {
            String host = "smtp.gmail.com";
            String user = "theplaceenglishschoolrh@gmail.com";
            String pass = "The@place";
            String to = emailUsuario; //para quem será enviado
            String from = "theplaceenglishschoolrh@gmail.com";
            String subject = "The Place English School"; //assunto

            DataSource caminhoArquivo;
            URL urlDiretorioLocal = this.getClass().getResource("/br/com/theplace/imagens/");

            String messageText = "<html>\n"
                    + "    <head>\n"
                    + "        <style>\n"
                    + "            body { margin: 0; padding: 0; min-width: 100%; width: 100% !important; height: 100% !important;}\n"
                    + "        </style>\n"
                    + "    </head>\n"
                    + "    <body>\n"
                    + "        <table width=\"100%\" align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%;\" class=\"background\"><tr><td align=\"center\" valign=\"top\" style=\"border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;\"\n"
                    + "                                                                                                                                                                                                        bgcolor=\"#2D3445\">\n"
                    + "                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\"\n"
                    + "                           width=\"600\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;\n"
                    + "                           max-width: 600px;\" class=\"wrapper\">\n"
                    + "                        <tr>\n"
                    + "                            <td align=\"center\" valign=\"top\" style=\"border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 22px; font-weight: bold; line-height: 130%;\n"
                    + "                                padding-top: 22px;\n"
                    + "                                color: #FFFFFF;\n"
                    + "                                font-family: sans-serif;\" class=\"header\">\n"
                    + "                                The Place English School\n"
                    + "                            </td>\n"
                    + "                        </tr>\n"
                    + "                        <tr>\n"
                    + "                            <td align=\"center\" valign=\"top\" style=\"border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;\n"
                    + "                                padding-top: 20px;\" class=\"hero\"><a target=\"_blank\" style=\"text-decoration: none;\"><img border=\"0\" vspace=\"0\" hspace=\"0\"\n"
                    + "                                                                                                  src=\"cid:logotipo\"\n"
                    + "                                                                                                  alt=\"Logotipo The Place English School\"\n"
                    + "                                                                                                  width=\"230\" style=\"\n"
                    + "                                                                                                  width: 88.33%;\n"
                    + "                                                                                                  max-width: 230px;\n"
                    + "                                                                                                  color: #FFFFFF; font-size: 13px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;\"/></a></td>\n"
                    + "                        </tr>     \n"
                    + "                        <tr>\n"
                    + "                            <td align=\"center\" valign=\"top\" style=\"border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 15px; font-weight: 400; line-height: 160%;\n"
                    + "                                padding-top: 25px; \n"
                    + "                                color: #FFFFFF;\n"
                    + "                                font-family: sans-serif;\" class=\"paragraph\">\n"
                    + "                                Olá " + nomeUsuario + ", sua solicitação de acesso foi confirmada. <br>\n"
                    + "                                Você tem permição para acessar ao sistema The Place English School, e utilizar de suas diversas funcionalidades.\n"
                    + "                            </td>\n"
                    + "                        </tr>\n"
                    + "                        <tr>\n"
                    + "                            <td style=\"padding-top: 10px; padding-bottom: 15px;\"></td>\n"
                    + "                        </tr>\n"
                    + "                    </table>\n"
                    + "                </td></tr><tr><td align=\"center\" valign=\"top\" style=\"border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;\n"
                    + "                              padding-top: 5px;\"\n"
                    + "                              bgcolor=\"#FFFFFF\">\n"
                    + "                    <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"center\"\n"
                    + "                           width=\"600\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;\n"
                    + "                           max-width: 600px;\">\n"
                    + "                        <tr>\n"
                    + "                            <td align=\"center\" valign=\"top\" style=\"border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 10px; padding-right: 10px;\" class=\"floaters\"><table width=\"280\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\" valign=\"top\" style=\"border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0; margin: 0; padding: 0; display: inline-table; float: none;\" class=\"floater\"><tr><td align=\"center\" valign=\"top\" style=\"border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 15px; padding-right: 15px; font-size: 17px; font-weight: 400; line-height: 160%;\n"
                    + "                                                                                                                                                                                                        padding-top: 30px; \n"
                    + "                                                                                                                                                                                                        font-family: sans-serif;\n"
                    + "                                                                                                                                                                                                        color: #000000;\"><a target=\"_blank\" style=\"text-decoration: none;\n"
                    + "                                             font-size: 17px; line-height: 160%;\"><img border=\"0\" vspace=\"0\" hspace=\"0\"\n"
                    + "                                                   src=\"cid:matriculas\"\n"
                    + "                                                   width=\"250\" height=\"142\"\n"
                    + "                                                   alt=\"Grid Item\" title=\"Matriculas\" style=\"\n"
                    + "                                                   color: #000000; font-size: 10px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block; margin-bottom: 8px;\" />\n"
                    + "                                                <b style=\"color:#0B5073; text-decoration: underline;\">Matriculas</b></a><br/>\n"
                    + "                                            Realizar matricula de alunos com agilidade e eficiência.\n"
                    + "                                        </td></tr></table><table width=\"280\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"right\" valign=\"top\" style=\"border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0; margin: 0; padding: 0; display: inline-table; float: none;\" class=\"floater\"><tr><td align=\"center\" valign=\"top\" style=\"border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 15px; padding-right: 15px; font-size: 17px; font-weight: 400; line-height: 160%;\n"
                    + "                                        padding-top: 30px; \n"
                    + "                                        font-family: sans-serif;\n"
                    + "                                                                                                                                                                                                        color: #000000;\"><a target=\"_blank\" style=\"text-decoration: none;\n"
                    + "                                             font-size: 17px; line-height: 160%;\"><img border=\"0\" vspace=\"0\" hspace=\"0\"\n"
                    + "                                                   src=\"cid:relatorios\"\n"
                    + "                                                   width=\"250\" height=\"142\"\n"
                    + "                                                   alt=\"Grid Item\" title=\"Relatórios\" style=\"\n"
                    + "                                                   color: #000000; font-size: 10px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block; margin-bottom: 8px;\" />\n"
                    + "                                                <b style=\"color:#0B5073; text-decoration: underline;\">Relatórios</b></a><br/>\n"
                    + "                                            Emitir diversos relatórios.\n"
                    + "                                        </td></tr></table></td>\n"
                    + "                        </tr>\n"
                    + "                        <tr>\n"
                    + "                            <td align=\"center\" valign=\"top\" style=\"border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 10px; padding-right: 10px;\" class=\"floaters\"><table width=\"280\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"right\" valign=\"top\" style=\"border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0; margin: 0; padding: 0; display: inline-table; float: none;\" class=\"floater\"><tr><td align=\"center\" valign=\"top\" style=\"border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 15px; padding-right: 15px; font-size: 17px; font-weight: 400; line-height: 160%;\n"
                    + "                                                                                                                                                                                                        padding-top: 30px;\n"
                    + "                                                                                                                                                                                                        font-family: sans-serif;\n"
                    + "                                                                                                                                                                                                        color: #000000;\"><a target=\"_blank\" style=\"text-decoration: none;\n"
                    + "                                             font-size: 17px; line-height: 160%;\"><img border=\"0\" vspace=\"0\" hspace=\"0\"\n"
                    + "                                                   src=\"cid:pagamento\"\n"
                    + "                                                   width=\"250\" height=\"142\"\n"
                    + "                                                   alt=\"Grid Item\" title=\"Formas de Pagamento\" style=\"\n"
                    + "                                                   color: #000000; font-size: 10px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block; margin-bottom: 8px;\" />\n"
                    + "                                                <b style=\"color:#0B5073; text-decoration: underline;\">Formas de Pagamento</b></a><br/>\n"
                    + "                                            Dispõe de diversas formas de pagamento.\n"
                    + "                                        </td></tr></table><table width=\"280\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" align=\"left\" valign=\"top\" style=\"border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-spacing: 0; margin: 0; padding: 0; display: inline-table; float: none;\" class=\"floater\"><tr><td align=\"center\" valign=\"top\" style=\"border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 15px; padding-right: 15px; font-size: 17px; font-weight: 400; line-height: 160%;\n"
                    + "                                        padding-top: 30px;\n"
                    + "                                        font-family: sans-serif;\n"
                    + "                                                                                                                                                                                                        color: #000000;\"><a target=\"_blank\" style=\"text-decoration: none;\n"
                    + "                                             font-size: 17px; line-height: 160%;\"><img border=\"0\" vspace=\"0\" hspace=\"0\"\n"
                    + "                                                   src=\"cid:recuperarSenha\"\n"
                    + "                                                   width=\"250\" height=\"142\"\n"
                    + "                                                   alt=\"Grid Item\" title=\"Recuperar sua Senha\" style=\"\n"
                    + "                                                   color: #000000; font-size: 10px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block; margin-bottom: 8px;\" />\n"
                    + "                                                <b style=\"color:#0B5073; text-decoration: underline;\">Recuperar sua Senha</b></a><br/>\n"
                    + "                                            Esqueceu a senha, a mesma pode ser recuperada.\n"
                    + "                                        </td></tr></table></td>\n"
                    + "                        </tr>\n"
                    + "                        <tr>\n"
                    + "                            <td style=\"padding-top: 30px; padding-bottom: 35px;\"></td>\n"
                    + "                        </tr>\n"
                    + "                    </table>\n"
                    + "        </table>\n"
                    + "    </body>\n"
                    + "</html>";
            boolean sessionDebug = false;

            Properties props = System.getProperties();

            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.required", "true");

            java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            // we need security so java already provide security
            Session mailSession = Session.getDefaultInstance(props, null);
            mailSession.setDebug(sessionDebug);

            MimeMessage msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(from, "The Place English School"));
            InternetAddress[] address = {new InternetAddress(to)}; //address of sender
            msg.setRecipients(Message.RecipientType.TO, address); //receiver e-mail
            msg.setSubject(subject);
            msg.setSentDate(new Date());

            //
            // Este E-mail é dividido em partes, sendo a HTML e as imagens...
            //
            MimeMultipart multipart = new MimeMultipart("related");

            // primeira parte
            BodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(messageText, "text/html");

            //adicinando o html
            multipart.addBodyPart(messageBodyPart);

            // segunda parte
            messageBodyPart = new MimeBodyPart();
            caminhoArquivo = new FileDataSource(urlDiretorioLocal.getPath() + "logotipo.png");
            messageBodyPart.setDataHandler(new DataHandler(caminhoArquivo));
            messageBodyPart.setHeader("Content-ID", "<logotipo>");
            // adicionando a imagem
            multipart.addBodyPart(messageBodyPart);

            // segunda parte
            messageBodyPart = new MimeBodyPart();
            caminhoArquivo = new FileDataSource(urlDiretorioLocal.getPath() + "matriculas.png");
            messageBodyPart.setDataHandler(new DataHandler(caminhoArquivo));
            messageBodyPart.setHeader("Content-ID", "<matriculas>");
            // adicionando a imagem
            multipart.addBodyPart(messageBodyPart);

            // terceira parte
            messageBodyPart = new MimeBodyPart();
            caminhoArquivo = new FileDataSource(urlDiretorioLocal.getPath() + "relatorios.png");
            messageBodyPart.setDataHandler(new DataHandler(caminhoArquivo));
            messageBodyPart.setHeader("Content-ID", "<relatorios>");
            // adicionando a imagem
            multipart.addBodyPart(messageBodyPart);

            // quarta parte
            messageBodyPart = new MimeBodyPart();
            caminhoArquivo = new FileDataSource(urlDiretorioLocal.getPath() + "pagamento.png");
            messageBodyPart.setDataHandler(new DataHandler(caminhoArquivo));
            messageBodyPart.setHeader("Content-ID", "<pagamento>");
            // adicionando a imagem
            multipart.addBodyPart(messageBodyPart);

            // quinta parte
            messageBodyPart = new MimeBodyPart();
            caminhoArquivo = new FileDataSource(urlDiretorioLocal.getPath() + "recuperarSenha.png");
            messageBodyPart.setDataHandler(new DataHandler(caminhoArquivo));
            messageBodyPart.setHeader("Content-ID", "<recuperarSenha>");
            // adicionando a imagem
            multipart.addBodyPart(messageBodyPart);

            msg.setContent(multipart, "text/html; charset=utf-8");

            Transport transport = mailSession.getTransport("smtp");
            transport.connect(host, user, pass);
            transport.sendMessage(msg, msg.getAllRecipients());
            transport.close();

            JOptionPane.showMessageDialog(null, "Usuário ATIVADO com sucesso!", "Sucesso", 1);

        } catch (Exception ex) {
            //aviso caso tenha erro no try
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Falha ao enviar o e-mail de Bem Vindo.", "Erro", 0);
        }
    }
    
}

# The Place English School

Essa ferramenta servirá para o administrador gerenciar o seu negócio de forma clara e eficaz, 
tendo como meta melhorar a qualidade do serviço e, de certa forma, agilizar o processo. 
E assim, atingir uma prestação de serviços de qualidade e com aproveitamento significativo.


<br>
## Tecnologias

![Tecnologias](/uploads/46df6fb77d21b43d9ddea51809ef82c2/Tecnologias.png)

<br>
* [Java 8](https://www.java.com/pt_BR/about/whatis_java.jsp?bucket_value=desktop-chrome57-windows10-wow64&in_query=no "O que é Java?")
* [API do PagSeguro 2.0](https://pagseguro.uol.com.br/sobre_o_pagseguro.jhtml "O que é PagSeguro?")
* [API do iReport 5.5.0](http://community.jaspersoft.com/project/ireport-designer "O que é iReport?")
* [MySQL 5.7](https://www.mysql.com/why-mysql/white-papers/whats-new-mysql-5-7/ "O que é MySQL?")

<br>
## Efetuando o Download do Software

Efetue o download do software de acordo com o seu sistema operacional.

[http://www.mediafire.com/file/z2vpmb4fvtpup1f/The+Place+English+School.exe](http://www.mediafire.com/file/z2vpmb4fvtpup1f/The+Place+English+School.exe "Link para download do sistema.")

## Manual de Instalação

**Windows: Iniciando a Instalação** 

Após executar o arquivo baixado, será exibida a seguinte tela:

![selecionarIdioma](/uploads/b25f7c8fd077725b585015157b1fddb4/selecionarIdioma.png)

Selecione o idioma ```“Português (Brasil)”``` para iniciar a instalação.

![avancar](/uploads/477092dea696fa7f485411917a314fb7/avancar.png)

Clique no botão ```“Avançar >”```.

**Selecionando a pasta de instalação**

A tela a seguir será exibida solicitando a pasta em que o software será instalado:

![localDestino](/uploads/04d0a40313074b2e77582a55d8dbc35f/localDestino.png)

É recomendado manter a pasta padrão de instalação. Caso deseje modificar, selecione a nova pasta e clique em ```“Avançar >”```.

![menuIniciar](/uploads/10204898f6deb780573633165d782c12/menuIniciar.png)

A pasta padrão onde é criado os atalhos do sistema no menu inicar. Caso deseje modificar, selecione a nova pasta e clique em ```“Avançar >”```.

![criarIcone](/uploads/3129cac16ec7a5d348ad1ec34ceb0edd/criarIcone.png)

É recomendado manter o padrão onde é criado o ícone na Área de Trabalho. Caso deseje modificar, desmarque a opção e clique em ```“Avançar >”```.

**Confirmando a Instalação** 

![instalar](/uploads/407da8e2aa2642ea13a4b93e46b941b8/instalar.png)

Clique em ```“Instalar”```. A instalação será efetuada e a barra de progressão é preenchida conforme a tela abaixo:

![instalando](/uploads/c1ea05e5d788303e8d844073446ec079/instalando.png)

Instalando em progresso. Aguarde sua Finalização.

**Finalizando a Instalação** 

A tela abaixo é exibida ao fim do processo. Basta clicar no botão ```“Concluir”``` para finalizar a instalação.

![concluido](/uploads/fdb5669603e5c800f6af4b690dd09243/concluido.png)

**Pronto**, o gerenciador de cursos The Place English School está instalado.